# Serapis

## About

Serapis (version 3) is a book management software developed for and financed by the Auroville Library.\
It is released open source under the MIT license. See [License.txt](https://gitlab.com/miastudio/serapis/-/blob/master/License.txt) for further details.

## Installation

### Requirements

This installation was tested on Ubuntu 20. Installations requirements may vary slightly on different platforms.

- System with `docker` and `docker compose` > 1.6. [Recommended installation method](https://docs.docker.com/engine/install/ubuntu/) 
- docker image is provided as amd64 (x86) and arm64
- (optional - recommended) automysqlbackup

### Configure for `docker compose`

#### compose.yml

In a blank folder,  use the default `compose.yaml.sample` file provided in the [repository](https://gitlab.com/miastudio/serapis/-/blob/master/compose.yaml.sample) and rename to `compose.yaml` - or create your own:

```yaml
services:
  serapis_app:
    image: registry.gitlab.com/miastudio/serapis
    container_name: serapis_app
    depends_on:
      - serapis_db
    restart: always
    ports:
      - 80:80
    environment:
      - MIA_DB_HOST=host.docker.internal
      - MIA_DB_PASS=set_same_top_secret_password_here
  #    volumes:
  #      - ${PWD}/_backup:/app/_backup
  serapis_db:
    image: mysql:5.7
    container_name: serapis_db
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=set_same_top_secret_password_here
    ports:
      - 3306:3306
    command: --sql_mode=ONLY_FULL_GROUP_BY,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION --innodb_ft_min_token_size=1
#  serapis_backup:
#    image: databack/mysql-backup
#    container_name: serapis_backup
#    restart: always
#    depends_on:
#      - serapis_db
#    environment:
#      - DB_NAMES=serapis
#      - SINGLE_DATABASE=true
#      - DB_DUMP_FREQ=60
#      - DB_DUMP_TARGET=/db
#      - DB_SERVER=172.17.0.1
#      - DB_USER=root
#      - DB_PASS=set_same_top_secret_password_here
#    volumes:
#      - ${PWD}/_backup:/db
#      - ${PWD}/backup-scripts:/scripts.d/post-backup
```

Notes
- **Replace all 3 instances of `set_same_top_secret_password_here` with a secure password** 
- In docker versions older than 18.03, `host.docker.internal` might not work and may need to be replaced with the IP of the host in the container, typically 172.17.0.1, or 172.18.0.1. See this page for finding your IP: https://kodekloud.com/blog/get-docker-container-ip/
- The commented sections are optional and disabled by default. See `backup` section below for further details
- Port `80:80`: if you already have a web service running on port 80, this can be changed any available port - for example, 8080 (then set the value to `8080:80`). But port 80 is free, it is recommended you do not change this setting
- The image can be downloaded from docker hub or compiled using the code in this repository

### Starting

- In the folder, you should have this file: `compose.yaml`
- Start Serapis by running `docker compose up`\
- This will show a log of events which should stop after a few seconds once the containers are started.

### Accessing

You can now access Serapis on `http://127.0.0.1`.\
If you wish to access Serapis from another computer, enter `http://IP_OF_SERVER` into your browser, where IP_OF_SERVER is the IP address of the computer on which you are running Serapis.

### Database

You will probably be greeted by this error because you do not have a database:

```text
CDbException
CDbConnection failed to open the DB connection: SQLSTATE[HY000] [1049] Unknown database 'serapis'
```

#### Installing the blank database
[Download](https://gitlab.com/miastudio/serapis/-/blob/master/sql/docker_restore.sql) the `docker_restore.sql` script and run it:
```shell
docker exec -i serapis_db mysql -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD -e "create database serapis"

docker exec -i serapis_db mysql -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD serapis < ./docker_restore.sql
```

If at any point you wish to remove the database to start over, run:

```shell
docker exec -i serapis_db mysql -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD -e "drop database serapis"
```

#### Restoring a backup

If you have a previous Serapis backup, you can restore it running:
```shell
gunzip < /Path/to/serapis_backup.sql.gz | docker exec -i serapis_db serapis -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD serapis
```

### Logging in

If you refresh the Serapis page in your browser, you should now see the default home screen. You can click on the login button on the top right to access the backend. Use the following login:
- default username: `masteradmin@serapis`
- default password: `serapismasteradmin`

**Make sure to change the default password and create dedicated accounts for each user !**

### Configuring emails

Serapis uses Sparkpost to send reminders. When first installing Serapis, make sure to configure these settings under `settings` > `lookups`, `section` = `sparkpost Email`:
- `code` = 'API Key' - Your sparkpost API key
- `code` = 'from_email' - The sender email which can be replied to (for example: `mylibrary@example.com`)
- `code` = 'from_name' - The sender name displayed next to the sender's email address  (for example `My Library`)

For each of these settings, press the pencil to update the value and press `save` once done.

### Running on boot
Running `docker compose up` will run the program while the computer is up, and will stop serapis when the computer is shut down.\
To make Serapis persistant across reboots, use `docker compose up -d`

If you wish to stop Serapis in the future, browser into the folder and run `docker compose down`

## Backups

### Manual Backup

To quickly create a backup file manually, run:

```shell
docker exec -i serapis_db mysqldump -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD serapis | gzip > /path/to/v2/backup.sql.gz
```

This assumes your system has the gzip program.

### GUI backup system

- Serapis includes a backup system which is accessible within Serapis to the administrator in the `settings` > `backup` menu.
- This backup works well for small databases, but **is known to fail with large databases**.
- Display and download works regardless of the database size. 

### Backup container

We have successfully used: https://hub.docker.com/r/databack/mysql-backup

#### Configuration

Uncomment/edit this in your compose.yaml file:

```yaml
services:
  (...)
  serapis_backup:
    image: databack/mysql-backup
    container_name: serapis_backup
    restart: always
    depends_on:
      - serapis_db
    environment:
      - DB_NAMES=serapis
      - SINGLE_DATABASE=true
      - DB_DUMP_FREQ=60
      - DB_DUMP_TARGET=/db
      - DB_SERVER=172.17.0.1
      - DB_USER=root
      - DB_PASS=set_same_top_secret_password_here
    volumes:
      - ${PWD}/_backup:/db
      - ${PWD}/backup-scripts:/scripts.d/post-backup
```

Configuration:
- ensure that your `DB_PASS` is set to your actual password
- create folders:
    - `mkdir _backup`
    - `mkdir backup-scripts`
- set permission to allow backup:
  - `chmod 777 _backup`
- `${PWD}/backup-scripts`: path to a script folder to rename the backup files. `backup-rename.sh` [included in the source](https://gitlab.com/miastudio/serapis/-/blob/master/backup-scripts/backup-rename.sh) - sample below
- `DB_DUMP_FREQ`: how often the backup is done (minutes - default 60 minutes / 1h)


##### Sample `backup-rename.sh` file

Inspired from [gitlab](https://github.com/databacker/mysql-backup/#backup-pre-and-post-processing):

```shell
#!/bin/bash
# Rename backup file.
DESTDIR=/db
if [[ -n "$DB_DUMP_DEBUG" ]]; then
  set -x
fi

if [ -e ${DUMPFILE} ];
then
  now=$(date -u --iso-8601=seconds) # does not seem to support TZ
  new_name=serapis_${now}.sql.gz
  old_name=$(basename ${DUMPFILE})
  echo "Renaming backup file from ${old_name} to ${new_name}"
  mv ${DUMPFILE} ${DESTDIR}/${new_name}
else
  echo "ERROR: Backup file ${DUMPFILE} does not exist!"
fi
```

- **Save the above content into the file `backup-scripts/backup-rename.sh`**
- make it executable: `chmod +x backup-scripts/backup-rename.sh`
- Filename uses UTC time
### Map backup container folder into Serapis

If this is done, files backed up through the container can be seen, downloaded and deleted in the Serapis GUI. **Restoring will not work** !\
Use the method described in the [Restoring a backup](#restoring-a-backup) section.

```yaml
services:
  frontend:
    image: miastudio/serapis
    container_name: serapis_app
    volumes:
      - ${PWD}/_backup:/app/_backup
  (...)
```


## Advanced

### Upgrading from Serapis v2

If you have a Serapis v2 backup and wish to import/upgrade/restore it, install and start as described above and then:

#### Import v2 backup:

```shell
docker exec -i serapis_db mysql -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD -e "create database avlib"
docker exec -i serapis_db mysql -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD avlib < /path/to/v2/backup/AVLibBackup_20200202_1020.sql
```

#### Migrate data

There should now be 2 databases in MySQL:
- `avlib` (Serapis v2) - containing old data
- `serapis` (Serapis v3) - blank, freshly created from `docker_restore.sql`

To migrate the data, make sure to get the `migrate_from_v2.sql` and run it:
```shell
docker exec -i serapis_db mysql -uroot -pREPLACE_WITH_MYSQL_ROOT_PASSWORD mysql < /path/to/migrate_from_v2.sql
```

Depending on the number of books and hardware resources, this may take a considerable time. On the Auroville Library server this took Around 15 min for approximately 50,000 books.

### Advanced commands

- Stop running container: `docker compose down`
- Remove running container (**will delete ALL data**): `docker compose rm`

### Standalone database

These instructions focus on installing MySQL 5.7 as a docker image. If you need higher performance, you may want to install MySql as a standalone version.\
Such an installation goes beyond the scope of this file. You can find some (dated) examples for how to do this in the `install.sh` file in the repository.

## Develoopment

### Building container

We recommend you use the latest version on docker hub. However, if you wish to build the container locally, you do so by using the [MDS](https://gitlab.com/miastudio/mds) script:

Download:
```shell
git clone ssh://git@gitlab.com/miastudio.in/serapis.git
cd serapis
git submodule update --init --force --remote
cp compose.yaml.sample compose.yaml
nano .env
```

Make sure to create/configure the `.env` file. Then run:
```shell
./deploy.sh build
./deploy.sh run
```

#### Running MySQL container

Serapis will require MySQL which you can run like this:

```shell
docker run --name serapis_db -e MYSQL_ROOT_PASSWORD=REPLACE_WITH_MYSQL_ROOT_PASSWORD -p 3306:3306 mysql:5.7 --sql_mode=ONLY_FULL_GROUP_BY,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION --innodb_ft_min_token_size=1
```

You can restore a database to this container as described above.

#### Running Container with local storage in `compose.yaml`

If you map your local folder to `/app`, it allows you to run local code from within `docker compose` (without / instead of using MDS):

```yaml
services:
  frontend:
    image: miastudio/serapis
    container_name: serapis_app
    volumes:
      - .:/app
```

### env files

These files are relevant during development, or when running without the above `compose.yml` file

#### .env-default

Use the default `.env-default` file provided in this [repository](https://gitlab.com/miastudio/serapis/-/blob/master/.env-default)  (recommended), or create your own:

```ini
# copy this file to .env to override settings
# .env-default will be used for builds
# .env will be used for run_local

# Enable additional messages
MDS_DEBUG=true

# Image settings
MIA_IMAGE_NAME=registry.gitlab.com/miastudio/serapis
MIA_IMAGE_TAG=3.1.8

# Container Settings
MIA_CONTAINER_DAEMONIZE=true
#MIA_CONTAINER_VOLUME=local
MIA_CONTAINER_VOLUME=container
MIA_CONTAINER_NAME=serapis_app
MIA_CONTAINER_PORT=8080
MIA_CONTAINER_DOCUMENTROOT=/app/www

# App settings
MIA_APP_NAME=serapis
MIA_APP_VERSION=3.1.8

# DB Settings
MIA_DB_HOST=172.17.0.1
MIA_DB_PORT=3306
MIA_DB_NAME=serapis
MIA_DB_USER=root
MIA_DB_PASS=**SET_PASSWORD_IN_.env_FILE**

MYSQL_ROOT_PASSWORD=$MIA_DB_PASS

MIA_RUN_ADDITIONALPARAMETERS="-e MIA_APP_SHOWERRORS=$MIA_APP_SHOWERRORS -e MIA_APP_DEBUGLEVEL=$MIA_APP_DEBUGLEVEL -e MIA_APP_ENVIRONMENT=$MIA_APP_ENVIRONMENT"
#MIA_RUN_OVERRIDE="docker run....

```

This file should not be changed.

#### .env

Create a .env file in the same folder with at least the following:

```ini
# Container Settings
MIA_DB_HOST=host.docker.internal
MIA_DB_PASS=REPLACE_WITH_MYSQL_ROOT_PASSWORD

MYSQL_ROOT_PASSWORD=REPLACE_WITH_MYSQL_ROOT_PASSWORD

DB_PASS=REPLACE_WITH_MYSQL_ROOT_PASSWORD

MIA_RUN_ADDITIONALPARAMETERS="-e MIA_APP_SHOWERRORS=$MIA_APP_SHOWERRORS -e MIA_APP_DEBUGLEVEL=$MIA_APP_DEBUGLEVEL -e MIA_APP_ENVIRONMENT=$MIA_APP_ENVIRONMENT"
#MIA_RUN_OVERRIDE="docker run....
```
Configure:
- `MIA_DB_HOST`: hostname or IP under which to access the database. Typically `host.docker.internal` or `172.17.0.1`
- `MIA_DB_PASS`: chose and write your MySQL root password here
- `MYSQL_ROOT_PASSWORD`: write your MySQL root password here once more. Note: on some distributions, `MYSQL_ROOT_PASSWORD=$MIA_DB_PASS` works instead of setting the password a second time. YMMV
- `DB_PASS`: (optional) write your MySQL root password here if you are using the backup container

You can copy any `key=value` from `.env-default` into `.env` to customise. For example use `MIA_DB_PORT` to use MySQL on another port than `3306`