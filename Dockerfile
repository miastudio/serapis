# Latest compatible version
FROM php:7.1-apache

# Install imagemagik
RUN apt-get update && apt-get install -y \
    imagemagick libmagickwand-dev \
    --no-install-recommends && apt-get clean -y \
    && pecl install imagick \
    && docker-php-ext-enable imagick

# Install mysql
RUN docker-php-ext-install mysqli

# Install curl
RUN apt-get update -y && apt-get install -y \
    curl libcurl3-dev \
    --no-install-recommends && apt-get clean -y \
    && docker-php-ext-install curl

# Install mcrypt
RUN apt-get update -y && apt-get install -y \
    libmcrypt-dev \
    --no-install-recommends && apt-get clean -y \
    && docker-php-ext-install mcrypt

# RUN docker-php-ext-install memcached
RUN apt-get update -y && apt-get install -y \
    libmemcached-dev \
    && pecl install memcached \
    && docker-php-ext-enable memcached

# Install apc
RUN pecl install apcu \
    && pecl install apcu_bc-1.0.3 \
    && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini \
    && docker-php-ext-enable apc --ini-name 20-docker-php-ext-apc.ini

RUN docker-php-ext-install pdo_mysql

ARG A_MIA_CONTAINER_NAME
ENV MIA_CONTAINER_NAME=$A_MIA_CONTAINER_NAME
ARG A_MIA_CONTAINER_DOCUMENTROOT
ENV APACHE_DOCUMENT_ROOT=$A_MIA_CONTAINER_DOCUMENTROOT

ARG A_MIA_APP_NAME
ENV MIA_APP_NAME=$A_MIA_APP_NAME
ARG A_MIA_APP_VERSION
ENV MIA_APP_VERSION=$A_MIA_APP_VERSION

ARG A_MIA_APP_SHOWERRORS
ENV MIA_APP_SHOWERRORS=$A_MIA_APP_SHOWERRORS
ARG A_MIA_APP_DEBUGLEVEL
ENV MIA_APP_DEBUGLEVEL=$A_MIA_APP_DEBUGLEVEL

ARG A_MIA_DB_HOST
ENV MIA_DB_HOST=$A_MIA_DB_HOST
ARG A_MIA_DB_PORT
ENV MIA_DB_PORT=$A_MIA_DB_PORT
ARG A_MIA_DB_NAME
ENV MIA_DB_NAME=$A_MIA_DB_NAME
ARG A_MIA_DB_USER
ENV MIA_DB_USER=$A_MIA_DB_USER
ARG A_MIA_DB_PASS
ENV MIA_DB_PASS=$A_MIA_DB_PASS

ARG A_MIA_APP_ENVIRONMENT
ENV MIA_APP_ENVIRONMENT=$A_MIA_APP_ENVIRONMENT

# Use /app/WebApp/www as root (using non /var/www/html structure)
COPY . /app/

# Create yii required folders
RUN mkdir -p /app/www/protected/runtime && chown www-data:www-data /app/www/protected/runtime && chmod 775 /app/www/protected/runtime
RUN mkdir -p /app/www/assets && chown www-data:www-data /app/www/assets && chmod 775 /app/www/assets
RUN mkdir -p /app/_backup && chown www-data:www-data /app/_backup && chmod 775 /app/_backup

# from https://hub.docker.com/_/php
# This seems to fail on live
# /etc/apache2/conf-available/docker-php.conf | grep DOCUMENT_ROOT
# file does not get created on live ?!
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && \
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN chown -R www-data:www-data /app

# Remove 'ServerName not set' error
RUN echo "ServerName serapis" >> /etc/apache2/apache2.conf

# Enable RewriteModule
# using 'apachectl restart' insted of 'service apache2 restart' to avoid errors due to error 'AH00111: Config variable ${APACHE_DOCUMENT_ROOT} is not defined'
RUN a2enmod rewrite && \
    apachectl restart

