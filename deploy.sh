#!/bin/bash

# Load defaults
source ./.env-default
# Override with local settings
ENV_FILE=./.env
if [ -f "$ENV_FILE" ]; then
    source $ENV_FILE
fi

case "$1" in
  "run")
    /bin/bash ./mds/.deploytools.sh run
  ;;
  "start")
    /bin/bash ./mds/.deploytools.sh start
  ;;
  "stop")
    /bin/bash ./mds/.deploytools.sh stop
  ;;
  "restart")
    /bin/bash ./mds/.deploytools.sh restart
  ;;
  "remove")
    /bin/bash ./mds/.deploytools.sh remove
  ;;
  "build")
    /bin/bash ./mds/.deploytools.sh build
  ;;
  "rebuild")
    /bin/bash ./mds/.deploytools.sh rebuild
  ;;
  "publish")
    /bin/bash ./mds/.deploytools.sh publish
  ;;
  *)
    echo "Usage: $0 [command] [parameters]"
    echo "Commands:"
    echo " - start | run        start $MIA_APP_NAME container"
    echo " - stop               stop $MIA_APP_NAME container"
    echo " - restart            restart $MIA_APP_NAME container"
    echo " - remove             remove $MIA_APP_NAME container"
    echo " - build              build $MIA_APP_NAME image"
    echo " - rebuild            rebuild $MIA_APP_NAME image"
    echo " - publish            publish $MIA_APP_NAME image to repository"
    exit 1
  ;;
esac
