### Manual installation instructions for Serapis
# Deprecated method !
# See Readme.md file instead !



# install mysql 5.7
# https://dev.mysql.com/downloads/repo/apt/
sudo apt-get install ca-certificates lsb-release
wget https://dev.mysql.com/get/mysql-apt-config_0.8.7-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.7-1_all.deb
# use 5.7
sudo apt-get update
sudo apt-get install mysql-server
# allow 0000-00-00 dates
# https://stackoverflow.com/questions/36374335/error-in-mysql-when-setting-default-value-for-date-or-datetime/36374690#36374690
sudo su -
cat << EOF > /etc/mysql/my.cnf
[mysqld]
sql_mode=ONLY_FULL_GROUP_BY,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
innodb_ft_min_token_size=1
EOF
service mysql restart


# install system prerequisites
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install apache2 php php-mysql php-gd php-curl php-mcrypt php-apcu php-memcache imagemagick phpmyadmin git

# Install (modified) yii
sudo mkdir /opt/yii
sudo chown ${USER:=$(/usr/bin/id -run)}:www-data /opt/yii
cd /opt/yii
git init
git remote add origin ssh://git@altssh.bitbucket.org:443/coriolanweihrauch/yii.git
git config branch.master.remote origin
git config branch.master.merge refs/heads/master
git pull

# install serapis
sudo mkdir /var/www/serapis
sudo chown ${USER:=$(/usr/bin/id -run)}:www-data /var/www/serapis
cd /var/www/serapis
git init
git remote add origin ssh://git@altssh.bitbucket.org:443/coriolanweihrauch/serapis.git
git config branch.master.remote origin
git config branch.master.merge refs/heads/master
git pull

# create empty folder + fix permissions
mkdir /var/www/serapis/www/protected/runtime
mkdir /var/www/serapis/www/assets
mkdir /var/www/serapis/_backup
sudo chown ${USER:=$(/usr/bin/id -run)}:www-data /var/www/serapis/www/protected/runtime
sudo chown ${USER:=$(/usr/bin/id -run)}:www-data /var/www/serapis/www/assets
sudo chown ${USER:=$(/usr/bin/id -run)}:www-data /var/www/serapis/_backup
sudo chmod 774 /var/www/serapis/www/protected/runtime
sudo chmod 774 /var/www/serapis/www/assets
sudo chmod 774 /var/www/serapis/_backup

# edit local config files
cp /var/www/serapis/www/protected/config/_db.php.sample /var/www/serapis/www/protected/config/_db.php
cp /var/www/serapis/www/protected/config/_params.php.sample /var/www/serapis/www/protected/config/_params.php
nano /var/www/serapis/www/protected/config/_db.php
nano /var/www/serapis/www/protected/config/_params.php


# Install barebone database
mysql -u root -p -e "CREATE DATABASE serapis; grant all privileges on serapis.* to serapis@localhost identified by '__REPLACE_WITH_PASSWORD__'; FLUSH PRIVILEGES;"
mysql -u root -p serapis < /var/www/serapis/sql/serapis_bare.sql
# or restore mysql database
# mysql -u root -p serapis < serapis.sql

# Optional apache config
# servername
echo "ServerName serapis" >> /etc/apache2/apache2.conf
# disable default host
rm /etc/apache2/sites-enabled/000-default.conf

# change '001-serapis' as required
sudo bash -c 'cat <<EOF > /etc/apache2/sites-available/001-serapis.conf
<VirtualHost *:80>
	RewriteEngine on
        DocumentRoot "/var/www/serapis/www"
<Directory "/var/www/serapis/www">
	AllowOverride all
        allow from all
        Options +Indexes
</Directory>
</VirtualHost>
EOF'
# enable mod rewrite
sudo a2enmod rewrite
# enable site
sudo a2ensite 001-serapis.local
# check config
sudo apachectl configtest
# restart
sudo apachectl graceful


## cron jobs ##
sudo mkdir /var/log/serapis
sudo chown ${USER:=$(/usr/bin/id -run)}:www-data /var/log/serapis
sudo chmod 774 /var/log/serapis
# backup
sudo crontab -e -u www-data
# insert:
# 1 * * * * /var/www/serapis/www/protected/yiic serapis backup --bVerbose=true >> /var/log/serapis/backup.log 2>&1
# email
# 

touch serapis.log
chown git:www-data serapis.log
chmod 664 serapis.log
sudo crontab -e -u www-data
# * * * * * /var/www/serapis/www/protected/yiic serapis sendreminder --bReallySend=true --bVerbose=true >> /var/log/serapis.log 2>&1
* * * * * /var/www/serapis.miastudio.in/www/protected/yiic serapis sendreminder --bReallySend=true --bVerbose=true >> /var/log/serapis.log 2>&1




# access at http://serapis.local
# default username: masteradmin@serapis
# default password: serapismasteradmin


