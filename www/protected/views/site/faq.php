<div id="picstrip">
<?php
for($i=0; $i<6; $i++)
	print('<div style="background-image:url('.Yii::app()->createAbsoluteUrl('/images/t/'.$arrImages[$i]).')" ></div>');
?>
</div>
<h1>Frequently Asked Questions</h1>
<dl>
	<dt>How can we contact you ?</dt>
	<dd>Please write us at <?php echo MiACActiveRecord::displayContact("youthactivities@auroville.org.in"); ?></dd>

	<dt>Why this website?</dt>
	<dd>Auroville offers more than 30 <a href="<?php echo Yii::app()->createAbsoluteUrl('/activity'); ?>">activities</a> to children.<br/>We wish to provide the community (especially children/parents) with an organized and simple way to become aware of what is available.</dd>

	<dt>Who is the Auroville Youth Activities platform team?</dt>
	<dd>Aurosylle, Elisa, Marta, Megan, Pushkar, Satyavan, Shakti</dd>

	<dt>Which activities can be presented on the website?</dt>
	<dd>Any Youth Activity from Auroville running on a yearly / school year basis. However, new yearly activities will be added to the website after running independently for a few months.</dd>

	<dt>Is there a contribution required to be presented on the website?</dt>
	<dd>No, there is no contribution required.</dd>

	<dt>Why is the website so rich in photos?</dt>
	<dd>The website’s aim is to talk to children, hence the visual impact is emphasized.</dd>

	<dt>Can I present my activity without photos?</dt>
	<dd>Photos are required to present your activity at the same level of others.</dd>

	<dt>How can I enrol my child?</dt>
	<dd>Contact the coach of the sport that you are interested in</dd>

	<dt>Are guests welcome to join?</dt>
	<dd>Kindly check with the coach of the chosen activity</dd>

	<dt>How do I become one of the activity presented in the website?</dt>
	<dd>If you run an activity for kids year-round (excluding summer break) you write to <?php echo MiACActiveRecord::displayContact("youthactivities@auroville.org.in"); ?> – explaining in a few words your activity and providing your name / phone number and mail id. You'll then be provided with a username and password to access the backend of the website and create your own page</dd>

</dl>