<h1>Check-in</h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'checkin-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'method' => 'get',
	'focus' => array($book,'sn'),
)); ?>
<fieldset>

<?php
if($cc)
{
?>
<h2>Last book checked in</h2>
<?php
  echo $form->textFieldRow(
    $book,
    'barcode',
    array(
	    'disabled' => 'disabled',
	    'id' => 'sndontsave',
	    'class' => 'span4',
    ),
    array('append'=>CHtml::link('<i class="fa fa-eye"></i>',array('book/view','id'=>$cc->book_id)))
  );
  $book->sn = null;
	echo $form->textFieldRow(
		$cc->book,
		'title',
    array(
	    'disabled' => 'disabled',
	    'class' => 'span4',
    ),
    array('append'=>CHtml::link('<i class="fa fa-eye"></i>',array('book/view','id'=>$cc->book_id)))
	);

	echo $form->textFieldRow(
		$cc->person,
		'summary',
    array(
	    'disabled' => 'disabled',
	    'class' => 'span4',
        'append'=>'m&sup2;'
    ),
    array('append'=>CHtml::link('<i class="fa fa-eye"></i>',array('person/view','id'=>$cc->person_id)))
	);

	echo $form->textFieldRow(
		$cc,
		'borrowDate',
	    array(
		    'disabled' => 'disabled',
		    'class' => 'span4',
	    )
	);

    echo $form->hiddenField(
        $cc->person,
        'id'
    );

/*
	echo $form->textAreaRow(
    $cc->person,
    'notes',
    array(
	    'rows' => 5,
	    'class' => 'span4',
// 	    'disabled' => 'disabled',
    )
  );
*/
?>
<?php
}
?>
<h2>Next book to check-in</h2>
<?php
// format barcode
if($book['sn'])
    $book['sn'] = $book->barcode;
echo $form->textFieldRow(
  $book,
  'sn',
  array(
    'class' => 'span4',
  )
);
?>
</fieldset>

<div class="form-actions">
<?php $this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => "Check-in",
    'icon' => 'fa fa-check fa-fw',
  )
); ?>

<?php
if($book->id)
{
    $url = Yii::app()->createAbsoluteUrl('checkout/repair', array('id'=>$book->id));
$this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'button',
    'type' => 'warning',
    'label' => "Repair",
    'icon' => 'fa fa-refresh fa-fw',
	'htmlOptions' => array(
		'onclick' => 'if($("#Book_sn").val() != "") bootbox.confirm("Mark book as \'repair\' ?",
										function(confirmed){
			                if(confirmed) {
			                   window.location = "'.$url.'";
			                }
					}); else window.location = "'.$url.'"',
	),
  )
);
?>

<?php
    $url = Yii::app()->createAbsoluteUrl('checkout/missing', array('id'=>$book->id));
$this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'button',
    'type' => 'inverse',
    'label' => "Missing",
    'icon' => 'fa fa-question fa-fw',
	'htmlOptions' => array(
		'onclick' => 'if($("#Book_sn").val() != "") bootbox.confirm("Mark book as \'missing\' ?",
										function(confirmed){
			                if(confirmed) {
			                   window.location = "'.$url.'";
			                }
					}); else window.location = "'.$url.'"',
	),
  )
);
}
?>

</div>

<?php $this->endWidget(); ?>