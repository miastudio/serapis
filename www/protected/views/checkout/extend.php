<h1>Extend</h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'checkin-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'type' => 'horizontal',
	'focus' => array($model,'sn'),
)); ?>
<fieldset>
<?php echo $form->errorSummary($model) ?>
<?php
	echo $form->textFieldRow(
    $model->book,
    'summary',
    array(
	    'disabled' => 'disabled',
	    'class' => 'span4',
    )
  );

	echo $form->textFieldRow(
    $model->person,
    'summary',
    array(
	    'disabled' => 'disabled',
	    'class' => 'span4',
    )
  );

	echo $form->textFieldRow(
    $model,
    'borrowDate',
    array(
	    'disabled' => 'disabled',
	    'class' => 'span4',
    )
  );

  echo $form->datepickerRow(
    $model,
    'dueDate',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
      'htmlOptions' => array(
          'class' => 'span4',
      )
    ),
    array(
    	'append' => '<i class="icon-calendar" onClick="$(\'#Checkout_dueDate\').focus();"></i>',
        'hint'=>$model->extensionMessageRich,
    )
  );
?>

</fieldset>

<div class="form-actions">
<?php
$this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => "Extend".($model->cannotExtend ? " anyway":""),
  )
);
?>
</div>

<?php $this->endWidget(); ?>