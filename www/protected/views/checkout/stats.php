<h1>Checkouts</h1>
<?php
$gridColumns = array(
	array('name'=>'year', 'header'=>'Year'),
	array('name'=>'checkouts', 'header'=>'Checkouts'),
);


if(is_array($stats) && count($stats))
{
	$dataProvider = new CArrayDataProvider($stats, array(
    'sort'=>array(
      'attributes'=>array('year','checkouts'),
      'defaultOrder'=>'year',
    ),
    'pagination'=>array(
			'pageSize'=>20,
    ),
  ));

	$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
			'type' => Lookup::item('user_settings', 'ui_gridview_type'),
			'template'=>"{items}\n{pager}",
	    'dataProvider' => $dataProvider,
	    'template' => "{items}",
	    'columns' => $gridColumns,
'enableSorting'=>true,
    )
	);
}

/*
  	'id'=>'author_Book-grid',
    'dataProvider' => $gridDataProvider,
    'columns' => $gridColumns,
		'pager' => array(
		  'class' => 'bootstrap.widgets.TbPager',
		  'displayFirstAndLast' => true,
		),
*/

?>