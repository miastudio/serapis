<?php
$model = new Book_Topicalterm();
$model->unsetAttributes();
$model->book_id = $book_id;

// print_r($model);
// die();

$gridDataProvider=$model->search();

$buttonPlus = CHtml::ajaxLink(
	CHtml::tag('i', array('class'=>'icon-plus'), ""),
	$this->createUrl('book_Topicalterm/ajaxCreate'),
	array(
		'type' => 'POST',
		'data' => array(
			'Book_Topicalterm[book_id]'    		   => $model->book_id,
			'Book_Topicalterm[topicalterm_id]'     => 'js:$("#topicalterm_id").val()',
		),
		'success' => 'function(html){ $.fn.yiiGridView.update("book_topicalterm-grid"); }',
		'error' => 'function(html){ console.log(html.responseText); alert(html.responseText); }',
	),
	array(
		'data-original-title' => 'Assign to book',
		'data-toggle' => "tooltip",
	),
	true
);

$buttonCreate = CHtml::link(
    CHtml::tag('i', array('class'=>'fa fa-list'), ""),
    Yii::app()->createUrl('topicalterm/create'),
    array(
        'target'=>'_blank',
        'data-original-title' => 'Create',
		'data-toggle' => "tooltip",
	),
	true
);

$gridColumns = array(
		array(
			'header' => $model->getAttributeLabel('topicalterm_id'),
			'name' => 'topicalterm_id',
			'value' => 'CHtml::link($data->topicalterm->name, array("topicalterm/update", "id"=>$data->topicalterm_id), array("target"=>"_blank"), true)',
			'type' => 'raw',
			'footer' => CHtml::hiddenField('topicalterm_id' , 'value', array('id' => 'topicalterm_id')).$this->widget(
				'zii.widgets.jui.CJuiAutoComplete',
				array(
			    'name'=>'book_topicalterm_name',
					'sourceUrl'=>$this->createUrl('topicalterm/ajaxItem'),
			    'options'=>array(
			        'minLength'=>'2',
			        'select'=>"js: function(event, ui) {
							 		$('#topicalterm_id').val(ui.item['id']);
								}",
			    ),
			    'htmlOptions'=>array(
						'placeholder' => "New ".$model->getAttributeLabel('topicalterm_id'),
						'class' => 'span-12'
					),
				),
				true),
		),
		array(
			'name' => 'alternateTerm',
			'value' => '$data->topicalterm->alternateTerm',
		),
		array(
			'name' => 'notes',
			'value' => '$data->topicalterm->notes',
		),
		array(
			'class'=> 'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'remove',
					'options'=>array('title'=>'Permanently erase'),
					'url' => 'Yii::app()->createUrl("book_Topicalterm/delete", array("id"=>$data->id))',
				),
				'update' => array(
// 					'icon' => 'eye-open',
					'options' => array('title'=>'Update'),
					'url' => 'Yii::app()->createUrl("topicalterm/update", array("id"=>$data->topicalterm_id))',
				),
			),
			'footer' => $buttonPlus." ".$buttonCreate,
		),
);

$this->widget(
  'bootstrap.widgets.TbGridView',
  array(
  	'id'=>'book_topicalterm-grid',
		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'dataProvider' => $gridDataProvider,
		'template'=>"{items}\n{pager}",
    'columns' => $gridColumns,
		'pager' => array(
		  'class' => 'bootstrap.widgets.TbPager',
		  'displayFirstAndLast' => true,
		),
		'afterAjaxUpdate'=>"cgridReloadHandlerTopicaltermBook", // Crucial: no () after function. Calls on Load !!
  )
);


Yii::app()->clientScript->registerScript('cgridReloadHandlerTopicaltermBook', '
function cgridReloadHandlerTopicaltermBook(id,data){
    jQuery("#book_topicalterm_name").autocomplete({"minLength":"2","select": function(event, ui) { $("#topicalterm_id").val(ui.item["id"]);},"source":"/topicalterm/ajaxItem"});
}');