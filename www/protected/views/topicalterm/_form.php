<?php
/* @var $this meterController */
/* @var $model f */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'topicalterm-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'name'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

<?php
  echo $form->textFieldRow(
    $model,
    'name',
    array(
        'class' => 'span8',
    )
  );

  echo $form->textFieldRow(
    $model,
    'alternateTerm',
    array(
        'class' => 'span8',
    )
  );

  echo $form->textFieldRow(
    $model,
    'notes',
    array(
        'class' => 'span8',
    )
  );
?>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
        <?php
if(count($model->books))
{
            $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'type' => 'inverse',
                'label' => 'Delete all instances of this TT in to delete',
                'disabled' => 'disabled',
            )
        );
}
else
{
        $url = Yii::app()->createUrl('topicalterm/delete', array('id'=>$model->id));
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Delete',
                'type' => 'danger',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Really delete this Topical Term ?",
									function(confirmed){
						                if(confirmed) {
						                   window.location = "'.$url.'";
						                }
                                })',
				),
            )
        );
}
        ?>

    </div>
<?php $this->endWidget(); ?>