<?php
$this->breadcrumbs=array(
	'Topical Term'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'View', 'url'=>array('view','id'=>$model->id)),
// 	array('label'=>'Report', 'url'=>array('report', 'id'=>$model->id)),
);
?>
<h1>Update Topical Term '<?php echo $model->name; ?>'</h1>
<div id="Checkout">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>

<h2>Linked books</h2>
<div id="checkout">
<?php $this->renderPartial('//book/_list', array('books'=>$model->books, 'arrHide'=>array('authorRole'))); ?>
</div>