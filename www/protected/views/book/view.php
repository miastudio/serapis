<?php
/* @var $this HousingassetController */
/* @var $model Housingasset */

$this->breadcrumbs=array(
	'Books'=>array('index'),
	$model->summary,
	'view'
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'List', 'url'=>array('admin')),
);
?>

<br/>
<div class="badgelist"><?php
// echo $model->showBadges();
?></div>

<?php
if($model->PreviousId)
{
	print("<div class=\"previousid\">");
	echo CHtml::link('<i class="fa fa-arrow-left fa-lg"></i>',array('view', 'id'=>$model->PreviousId));
	print("</div>");
}
if($model->NextId)
{
	print("<div class=\"nextid\">");
	echo CHtml::link('<i class="fa fa-arrow-right fa-lg"></i>',array('view', 'id'=>$model->NextId));
	print("</div>");
}
?>

<?php
/*
 $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'sn',
		'title',
		'titleRemainder',
		'titleParalle',
		'titleOriginal',
		'partNumber',
		'partTotal',
		'statementOfResponsibility',
		'languages',
    'pubPublisher',
    'pubPlace',
    'pubYear',
    'editionStatement',
    'collation',
    'size',
    'pubPublisher',
    'bindingName',
    'ISBN',
    'ISSN',
    'seriesName',
    'seriesPartNumber',
    'seriesPartName',
    'notes',
    'creationDate',
    'deletionDate',
    'ddc',
    'number',
    'age',
	),
));
*/
?>

<div id="titlerow">
<div id="booklocation">Your book's location:</div>
<div id="booknumber"><?php print($model->ddc."<br/>".$model->number."<br/>".$model->barcode); ?></div>
<div id="bookstatus">Your book is: <?php print($model->statusBadge); ?></div>
</div>

<div id="bookauthors"><?php
if(is_array($model->authors) && count($model->authors) > 0)
foreach($model->link_author as $i => $v)
{
    echo CHtml::link('<b>'.$v->author->name.'</b> ('.$v->roleName.')<br/>',array('author/view', 'id'=>$v->author_id));
}
?></div>


<div id="booktitle"><?php
    print("<b>".$model->title."</b>");
    if($model->partNumber)
      print(" - <b>".$model->partNumber."</b><br/>");
    else
      print("<br/>\n");
    if($model->titleRemainder && $model->statementOfResponsibility)
      print("<i>".$model->titleRemainder . "</i> / " . $model->statementOfResponsibility);
    else
      print("<i>".$model->titleRemainder . "</i>". $model->statementOfResponsibility);
    ?>
</div>


<div id="bookpublication"><?php
    print($model->pubPlace
    .": ".$model->pubPublisher
    .", ".$model->pubYear
    .". - ".$model->editionStatement);
    ?><br/>
    <?php print($model->collation); ?>
<?php
if(count($model->features))
{
    print(" - ");
    foreach($model->features as $f)
    {
        $features[] = $f->name;
    }
    print(implode(", ", $features));
}
  if($model->bindingName)
	print(" - ".$model->bindingName);
?>
</div>

<div id="bookdetails">
<?php if($model->seriesName)
      {
        echo "(".$model->seriesName;
        if($model->seriesPartNumber)
            echo "; ".$model->seriesPartNumber;
        if($model->seriesPartName)
            echo ", ".$model->seriesPartName;
        echo ")<br/>";
      }
?>
<?php if($model->titleParallel)
    echo "    <u>Varying form of title</u>: ".$model->titleParallel."<br/>";
?>
<?php if($model->titleOriginal)
    echo "    <u>Original title</u>: ".$model->titleOriginal."<br/>";
?>

<?php if($model->ISBN)
    echo "    <u>ISBN</u>: ".$model->ISBN."<br/>";
?>

<?php if($model->languages)
    echo "    <u>Language</u>: ".$model->languages."<br/>";
?>
<?php if(count($model->topicalterms))
{
    print("<u>Subjects</u> / <u>Topical terms</u>:<br/><ul>");
    foreach($model->topicalterms as $t)
        print("<li>".CHtml::link($t->name, array('topicalterm/view', 'id'=>$t->id))."</li>");
    print("</ul>");
}
?>
<u>Date added</u>: <?php print(date("d/m/Y",strtotime($model->creationDate))); ?><br/>
</div>

<?php if($model->notes)
    echo "    <div id='booknotes'><i>".nl2br($model->notes)."</i></div>";
?>

<?php if(!Yii::app()->user->isGuest)
{
?>
<h2>Lending History</h2>
<div id="Checkout">
<?php $this->renderPartial('//checkout/_list', array('book_id'=>$model->id, 'arrHide'=>array('ddc','number','extend','sn','title','languages','author'))); ?>
</div>
<?php
}