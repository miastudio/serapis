<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Book'=>array('admin'),
	$model->summary=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'View', 'url'=>array('view','id'=>$model->id)),
// 	array('label'=>'Report', 'url'=>array('report', 'id'=>$model->id)),
);
?>

<h1>Book '<?php echo $model->summary; ?>'</h1>
<div class="badgelist"><?php
// echo $model->showBadges();
?></div>

<?php
if($model->PreviousId)
{
	print("<div class=\"previousid\">");
	echo CHtml::link('<i class="fa fa-arrow-left fa-lg"></i>',array('update', 'id'=>$model->PreviousId));
	print("</div>");
}
if($model->NextId)
{
	print("<div class=\"nextid\">");
	echo CHtml::link('<i class="fa fa-arrow-right fa-lg"></i>',array('update', 'id'=>$model->NextId));
	print("</div>");
}
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<h2>Lending History</h2>
<div id="Checkout">
<?php $this->renderPartial('//checkout/_list', array('book_id'=>$model->id, 'arrHide'=>array('sn','title','languages','author'))); ?>
</div>
