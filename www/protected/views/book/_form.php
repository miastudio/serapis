<?php
/* @var $this meterController */
/* @var $model f */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'book-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'title'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
		<?php
		$url = Yii::app()->createAbsoluteUrl('book/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
		<?php
if($model->deletionDate)
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Re-enter',
                'type' => 'inverse',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Re-enter book in catalogue (un-withdraw) ?",
									function(confirmed){
						                if(confirmed) {
						                   $("#Book_deletionDate").val("");
						                   $("#Book_status_id").val("'.Book::STATUS_IN.'");
						                   $("#book-form").submit();
						                }
                                })',
				),
            )
        );
else
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Withdraw',
                'type' => 'inverse',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Mark book as withdrawn (removed) ?",
									function(confirmed){
						                if(confirmed) {
						                   $("#Book_deletionDate").val(new Date().toISOString().slice(0,10));
						                   $("#Book_status_id").val("'.Book::STATUS_WITHDRAWN.'");
						                   $("#book-form").submit();
						                }
                                })',
				),
            )
        ); ?>
		<?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Duplicate',
                'type' => 'info',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Duplicate book ?",
									function(confirmed){
						                if(confirmed) {
						                   $("#Book_form_duplicate").val(1);
						                   $("#book-form").submit();
						                }
                                })',
				),
            )
        ); ?>

    </div>

    <fieldset>
	<div id="metadata_cover">
	</div>

<?php
	echo $form->dropDownListRow(
    $model,
    'status_id',
    Lookup::items('book_status'),
    array(
    	'empty'=>'--Select--',
        'class' => 'span8',
		)
  );

  echo $form->textFieldRow(
    $model,
    'sn',
    array(
        'class' => 'span8',
        'value' => $model->barcode,
    )
  );

  echo $form->hiddenField(
    $model,
    'form_duplicate'
  );

	echo $form->textFieldRow(
    $model,
    'title',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'titleRemainder',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'titleParallel',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'titleOriginal',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'partNumber',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'partTotal',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'statementOfResponsibility',
    array(
        'class' => 'span8',
    )
  );
?>

<div class="control-group">
	<label class="control-label">Author</label>
	<div class="controls" id="book_author">
		<?php $this->renderPartial('//author/_gridedit', array('book_id'=>$model->id)); ?>
	</div>
</div>

<?php
	// http://select2.github.io/select2/
	// http://yiibooster-3.clevertech.biz/widgets/forms_inputs/view/select2.html
	echo $form->select2Row(
	  $model,
	  'languages',
	  array(
      'asDropDownList' => false,
      'options' => array(
    	'tags' => Book::model()->listLanguages(),
        'tokenSeparators' => array(',', ' '),
        'class' => 's2_autocomplete',
        )
	  )
	);

/*
	echo $form->textFieldRow(
    $model,
    'pubPublisher',
    array(
        'class' => 'span8',
    )
  );
*/

?>
<div class="control-group">
	<label class="control-label" for="<?php echo "pubPublisher"; ?>"><?php echo $model->getAttributeLabel('pubPublisher'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#book_pubPublisher').focus();">
<?php
	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Book[pubPublisher]',
		'sourceUrl'=>$this->createUrl('book/ajaxPubPublisher'),
		'value'=>$model->pubPublisher,

    'options'=>array(
	    'minLength'=>'3',
    ),
    'htmlOptions'=>array(
        'class' => 'span8',
        'rows' => '5',
    )
	));
?>
		<span class="add-on"><i class="fa fa-ellipsis-h"></i></span>
		</div>
	</div>
</div>
<?php

/*
	echo $form->textFieldRow(
    $model,
    'pubPlace',
    array(
        'class' => 'span8',
    )
  );
*/

?>
<div class="control-group">
	<label class="control-label" for="<?php echo "pubPlace"; ?>"><?php echo $model->getAttributeLabel('pubPlace'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#book_pubPlace').focus();">
<?php
	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Book[pubPlace]',
		'sourceUrl'=>$this->createUrl('book/ajaxPubPlace'),
		'value'=>$model->pubPlace,

    'options'=>array(
	    'minLength'=>'3',
    ),
    'htmlOptions'=>array(
        'class' => 'span8',
        'rows' => '5',
    )
	));
?>
		<span class="add-on"><i class="fa fa-ellipsis-h"></i></span>
		</div>
	</div>
</div>
<?php

	echo $form->textFieldRow(
    $model,
    'pubYear',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'editionStatement',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'collation',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'size',
    array(
        'class' => 'span8',
    )
  );

	echo $form->dropDownListRow(
    $model,
    'binding_id',
    Lookup::items('physicaldetail'),
    array(
    	'empty'=>'--Select--',
        'class' => 'span8',
		)
  );

?>
<div class="control-group">
	<label class="control-label"><?php echo $model->getAttributeLabel('feature'); ?></label>
	<div class="controls" id="book_feature">
		<?php $this->renderPartial('//feature/_gridedit', array('book_id'=>$model->id)); ?>
	</div>
</div>
<?php

	echo $form->textFieldRow(
    $model,
    'ISBN',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'ISSN',
    array(
        'class' => 'span8',
    )
  );

/*
	echo $form->textFieldRow(
    $model,
    'seriesName',
    array(
        'class' => 'span8',
    )
  );
*/

?>
<div class="control-group">
	<label class="control-label" for="<?php echo "seriesName"; ?>"><?php echo $model->getAttributeLabel('seriesName'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#book_seriesName').focus();">
<?php
	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Book[seriesName]',
		'sourceUrl'=>$this->createUrl('book/ajaxSeriesName'),
		'value'=>$model->seriesName,

    'options'=>array(
	    'minLength'=>'3',
    ),
    'htmlOptions'=>array(
        'class' => 'span8',
        'rows' => '5',
    )
	));
?>
		<span class="add-on"><i class="fa fa-ellipsis-h"></i></span>
		</div>
	</div>
</div>
<?php

	echo $form->textFieldRow(
    $model,
    'seriesPartNumber',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'seriesPartName',
    array(
        'class' => 'span8',
    )
  );
?>
<div class="control-group">
	<label class="control-label"><?php echo $model->getAttributeLabel('topicalterm'); ?></label>
	<div class="controls" id="book_topicalterm">
		<?php $this->renderPartial('//topicalterm/_gridedit', array('book_id'=>$model->id)); ?>
	</div>
</div>
<div class="control-group">
	<label class="control-label"><?php echo $model->getAttributeLabel('keyword'); ?></label>
	<div class="controls" id="book_keyword">
		<?php $this->renderPartial('//keyword/_gridedit', array('book_id'=>$model->id)); ?>
	</div>
</div>
<?php


/*
	echo $form->textFieldRow(
    $model,
    'ageGroup',
    array(
        'class' => 'span8',
    )
  );
*/

?>
<div class="control-group">
	<label class="control-label" for="<?php echo "ageGroup"; ?>"><?php echo $model->getAttributeLabel('ageGroup'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#book_ageGroup').focus();">
<?php
	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
        'name'=>'Book[ageGroup]',
		'sourceUrl'=>$this->createUrl('book/ajaxAgeGroup'),
		'value'=>$model->ageGroup,

    'options'=>array(
	    'minLength'=>'1',
    ),
    'htmlOptions'=>array(
        'class' => 'span8',
        'rows' => '5',
    )
	));
?>
		<span class="add-on"><i class="fa fa-ellipsis-h"></i></span>
		</div>
	</div>
</div>
<?php

	echo $form->textAreaRow(
    $model,
    'notes',
    array(
        'class' => 'span8',
        'rows'=> 8,
    )
  );

  echo $form->datepickerRow(
    $model,
    'creationDate',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
      'htmlOptions' => array(
          'class' => 'span8',
      )
    ),
    array(
    	'append' => '<i class="icon-calendar" onClick="$(\'#Book_creationDate\').focus();"></i>',
    )
  );

  echo $form->datepickerRow(
    $model,
    'deletionDate',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
      'htmlOptions' => array(
          'class' => 'span8',
      )
    ),
    array(
    	'append' => '<i class="icon-calendar" onClick="$(\'#Book_deletionDate\').focus();"></i>',
    )
  );

	echo $form->textFieldRow(
    $model,
    'ddc',
    array(
        'class' => 'span8',
    )
  );

	echo $form->textFieldRow(
    $model,
    'number',
    array(
        'class' => 'span8',
    )
  );

?>

<div class="control-group">
	<label class="control-label"><?php echo $model->getAttributeLabel('sn'); ?></label>
	<div class="controls" id="book_barcode">
        <?php echo $model->barcode; ?>
	</div>
</div>

    </fieldset>
    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
		<?php
		$url = Yii::app()->createAbsoluteUrl('book/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
		<?php
if($model->deletionDate)
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Re-enter',
                'type' => 'inverse',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Re-enter book in catalogue (un-withdraw) ?",
									function(confirmed){
						                if(confirmed) {
						                   $("#Book_deletionDate").val("");
						                   $("#Book_status_id").val("'.Book::STATUS_IN.'");
						                   $("#book-form").submit();
						                }
                                })',
				),
            )
        );
else
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Withdraw',
                'type' => 'inverse',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Mark book as withdrawn (removed) ?",
									function(confirmed){
						                if(confirmed) {
						                   $("#Book_deletionDate").val(new Date().toISOString().slice(0,10));
						                   $("#Book_status_id").val("'.Book::STATUS_WITHDRAWN.'");
						                   $("#book-form").submit();
						                }
                                })',
				),
            )
        ); ?>
		<?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'button',
            	'label' => 'Duplicate',
                'type' => 'info',
            	'htmlOptions' => array(
                    'onclick' => 'bootbox.confirm("Duplicate book ?",
									function(confirmed){
						                if(confirmed) {
						                   $("#Book_form_duplicate").val(1);
						                   $("#book-form").submit();
						                }
                                })',
				),
            )
        ); ?>

    </div>
<?php $this->endWidget(); ?>