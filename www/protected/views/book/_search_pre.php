<div class="well">Search for <strong>Author</strong>, <strong>Keywords</strong> and <strong>Topical Terms</strong> first and then filter results in the table below</div>
<?php
echo $form->textFieldRow(
  $model,
  'search_advanced_pre',
  array('onChange'=>'$("#search_advanced_pre_model_category").val("");$("#search_advanced_pre_model_id").val("");')
);

if($model->search_advanced_pre_model)
  echo $form->textFieldRow(
    $model->search_advanced_pre_model,
    'summary',
    array('disabled' => 'disabled',),
    array('label'=>'Selected '.get_class($model->search_advanced_pre_model))
  );

else
    echo $form->checkBoxListRow(
      $model,
      'search_advanced_pre_paginate',
      array('Show all results (may take time to load)'),
      array()
    );