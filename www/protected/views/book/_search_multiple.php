<div class="well">Search for the same text on multiple fields</div>
<?php
	echo $form->textFieldRow(
    $model,
    'search_advanced_multiple'
  );

$arrAttr = array();
foreach($model->advancedSearchMultipleAttributes as $id => $attr)
  $arrAttr[$id] = $model->getAttributeLabel($attr);

echo $form->checkBoxListRow(
  $model,
  'search_advanced_multiple_fields',
  $arrAttr
);
