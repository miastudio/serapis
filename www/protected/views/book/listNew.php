<?php
/* @var $this clientController */
/* @var $model client */

$this->breadcrumbs=array(
	'Book'=>array('admin'),
	'Search',
);

$this->menu=array(
	array('label'=>'Advanced Search', 'url'=>array('search')),
	array('label'=>'Create Book', 'url'=>array('create')),
);
?>

<h1>New books</h1>
<?php // update this to dynamic dates ?? ?>
<?php
$month_start = new DateTime("today -1 months");
$month_end = new DateTime("today");

$week_start = new DateTime("today -1 weeks");
$week_end = new DateTime("today");

$today = new DateTime();

?>
<p>
<a href="javascript:updateDateRange('<?php echo $month_start->format('Y-m-d'); ?>','<?php echo $month_end->format('Y-m-d'); ?>');">Last month</a> -
<a href="javascript:updateDateRange('<?php echo $week_start->format('Y-m-d'); ?>','<?php echo $week_end->format('Y-m-d'); ?>');">Last Week</a> -
<a href="javascript:updateDateRange('<?php echo $today->format('Y-m-d'); ?>','<?php echo $today->format('Y-m-d'); ?>');">Today</a>
</p>
<?php
#http://www.daterangepicker.com/#options
$creationDateFilter = $this->widget('bootstrap.widgets.TbDateRangePicker', array(
						'model' => $model,
						'attribute' => 'search_creationDate',
	          'htmlOptions' => array(
	            'hint' => 'Click inside! An even a date range field!.',
							'value' => $model->search_creationDate,
							'size' => '15',
							'id' => 'Book_search_creationDate',
						),
	          'options' => array(
    	                    'format' => 'YYYY-MM-DD',
    	                    'locale' => array(
                                'firstDay' => 1,
    	                    ),
	          ),
						'callback' => 'js:function(start, end){ myUpdateGrid(start,end); }',
					), true);
?>
<script language="javascript">
function updateDateRange(strStartDate, strEndDate)
{
	// http://www.daterangepicker.com/#options
	$('#Book_search_creationDate').val(strStartDate+" - "+strEndDate);
	myUpdateGrid(strStartDate, strEndDate);
}
function myUpdateGrid(start, end)
{
	console.log('callback');

	// https://github.com/yiisoft/yii/blob/master/framework/zii/widgets/assets/gridview/jquery.yiigridview.js
	// http://queirozf.com/entries/yii-cgridview-javascript-functions-builtin-jquery-plugin
	href = $('#book-grid').yiiGridView('getUrl')
	if(href.indexOf("?") > 0)
	{
		var url = href.split('?');
		var params = $.deparam(url[1]);
	}
	else
	{
		var url = new Array(href);
		var params = new Object();
		params.Book = new Object();
	}
	params.Book.search_creationDate = $("#Book_search_creationDate").val();
	var updateUrl = $.param.querystring(url[0], params);

	console.log(url[1]);
	console.log(params);
	console.log(updateUrl);

// 	console.log($('#book-grid').yiiGridView);

	window.location = updateUrl;
// try to set url and use this:
// 	$('#book-grid').yiiGridView('update')
// $.fn.yiiGridView.update('book-grid')
}
</script>


<?php
$author = new Author();
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'book-grid',
// 	'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',
    'enableHistory' => 'true',
	'selectableRows'=>1,
	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),

	'columns'=>array(
		array(
			'name' => 'creationDate',
			'filter' => $creationDateFilter,
		),
		array(
			'name' => 'status_id',
			'value' => '$data->statusBadge',
			'type' => 'raw',
			'filter' => CHtml::listData(Lookup::lookupList('book_status'), 'id', 'title'),
		),
		array(
			'name' => 'sn',
// 			'value' => '$data->statusBadge',
// 			'type' => 'raw',
// 			'filter' => CHtml::listData(Lookup::lookupList('activity_status'), 'id', 'title'),
		),
		array(
			'name' => 'title',
			'type' => 'raw',
			'value' => Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" ?
			                'CHtml::link($data->title, array("book/update","id"=>$data->id))' :
			                'CHtml::link($data->title, array("book/view","id"=>$data->id))',
		),
		'author' => array(
// 			'header'=>$author->getAttributeLabel('authorName'),
      'name'=>'search_author',

			'type' => 'raw',
			'value' => '$data->authorNamesLinks',
		),
		array(
			'name' => 'languages',
// 			'type' => 'raw',
// 			'filter' => CHtml::listData(Lookup::lookupList('language'), 'id', 'title'),
			'filter' => CHtml::listData(Lookup::lookupList('language'), 'title', 'title'),

		),
		array(
			'name' => 'ddc',
		),
		array(
			'name' => 'number',
		),
		array(
			'name' => 'ageGroup',
		),

		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update}',

			'buttons' => array(
				'update' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'pencil',
				),
			),
		),
	),
));

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>