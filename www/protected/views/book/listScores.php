<?php
/* @var $this clientController */
/* @var $model client */

$this->breadcrumbs=array(
	'Book'=>array('admin'),
	'Search',
);

$this->menu=array(
	array('label'=>'Advanced Search', 'url'=>array('search')),
	array('label'=>'Create Book', 'url'=>array('create')),
);
?>

<h1>Scores</h1>
<?php
$author = new Author();
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'book-grid',
// 	'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',
    'enableHistory' => 'true',
	'selectableRows'=>1,
	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),

	'columns'=>array(

		'checkoutCount',
		'creationDate',
		array(
			'name' => 'dateLastBorrowed',
			'filter' => false,
		),
		array(
			'name' => 'status_id',
			'value' => '$data->statusBadge',
			'type' => 'raw',
			'filter' => CHtml::listData(Lookup::lookupList('book_status'), 'id', 'title'),
		),
		array(
			'name' => 'sn',
// 			'value' => '$data->statusBadge',
// 			'type' => 'raw',
// 			'filter' => CHtml::listData(Lookup::lookupList('activity_status'), 'id', 'title'),
		),
		array(
			'name' => 'title',
			'type' => 'raw',
			'value' => Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" ?
			                'CHtml::link($data->title, array("book/update","id"=>$data->id))' :
			                'CHtml::link($data->title, array("book/view","id"=>$data->id))',
		),
		'author' => array(
// 			'header'=>$author->getAttributeLabel('authorName'),
      'name'=>'search_author',

			'type' => 'raw',
			'value' => '$data->authorNamesLinks',
		),
		array(
			'name' => 'languages',
// 			'type' => 'raw',
// 			'filter' => CHtml::listData(Lookup::lookupList('language'), 'id', 'title'),
			'filter' => CHtml::listData(Lookup::lookupList('language'), 'title', 'title'),
		),
		array(
			'name' => 'ddc',
		),
		array(
			'name' => 'number',
		),
		array(
			'name' => 'ageGroup',
		),

		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update} {trash} {delete}',

			'buttons' => array(
				'update' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'pencil',
				),
				'trash' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'trash',
				),
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'remove',
				),
			),
		),
	),
));

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>