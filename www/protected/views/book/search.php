<?php
/* @var $this clientController */
/* @var $model client */

$this->breadcrumbs=array(
	'Book'=>array('admin'),
	'Advanced Search',
);

$this->menu=array(
	array('label'=>'Basic Search', 'url'=>array('search')),
	array('label'=>'Create Book', 'url'=>array('create')),
);
?>

<h1>Advanced search</h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'adv-search-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'action' => $_SERVER['REDIRECT_URL'], // to remove remnant Book[search_advanced_pre_model][category] and [id] from URL
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'search_advanced'),
  'method' => 'get',
)); ?>
<?php echo $form->errorSummary($model); ?>
<?php
/*
if($model->search_advanced_pre_model && $model->search_advanced_pre_model != 'cbasecontroller')
{
*/
    echo CHtml::hiddenField('Book[search_advanced_pre_model][category]', strtolower(get_class($model->search_advanced_pre_model)), array('id' => 'search_advanced_pre_model_category'));
    echo CHtml::hiddenField('Book[search_advanced_pre_model][id]', $model->search_advanced_pre_model->id, array('id' => 'search_advanced_pre_model_id'));
// }

echo CHtml::hiddenField('activeTab' , $activeTab, array('id' => 'activeTab'));
$this->widget(
  'bootstrap.widgets.TbTabs',
  array(
    'type' => 'tabs', // 'tabs' or 'pills'
		'id'=>'searchtabs',
    'tabs' => array(
      array(
        'label' => 'Pre-search',
        'id' => 'tab_pre',
        'linkOptions' => array('id'=>'tab_id_pre'),
        'content' => $this->renderPartial('_search_pre', array('model'=>$model, 'form'=>$form), true),
				'icon' => 'fa fa-share-square-o fa-fw',
				'active' => $activeTab == 'tab_pre' || ! $activeTab ? true : false,
      ),
      array(
      	'label' => 'Multiple fields',
        'id' => 'tab_multiple',
        'linkOptions' => array('id'=>'tab_id_multiple'),
        'content' => $this->renderPartial('_search_multiple', array('model'=>$model, 'form'=>$form), true),
				'icon' => 'fa fa-barcode fa-fw',
				'active' => $activeTab == 'tab_multiple' ? true : false,
      ),
      array(
      	'label' => 'Custom Columns',
        'id' => 'tab_custom',
        'linkOptions' => array('id'=>'tab_id_custom'),
        'content' => $this->renderPartial('_search_custom', array('model'=>$model, 'form'=>$form), true),
				'icon' => 'fa fa-th-list fa-fw',
				'active' => $activeTab == 'tab_custom' ? true : false,
      ),
    ),
  )
);

Yii::app()->clientScript->registerScript("searchtabsclick1", "$('#tab_id_pre').click(function(){\$('#advancedSubmit').text('Search'); return true;})");
Yii::app()->clientScript->registerScript("searchtabsclick2", "$('#tab_id_multiple').click(function(){\$('#advancedSubmit').text('Search'); return true;})");
Yii::app()->clientScript->registerScript("searchtabsclick3", "$('#tab_id_custom').click(function(){\$('#advancedSubmit').text('Apply'); return true;})");

?>
<div class="form-actions">
<?php $this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $activeTab == 'tab_custom' ? 'Apply' : 'Search',
  	'htmlOptions' => array(
			'onclick' => "$('#activeTab').val( $('#searchtabs ul li.active a').attr('href').replace(/^.*#/, '') );",
			'id' => 'advancedSubmit',
		),
  )
); ?>

<?php $this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'button',
    'label' => 'Reset',
    'htmlOptions' => array(
      'onClick' => "$(':input','#adv-search-form')
         .not(':button, :submit, :reset')
         .val('')
         .removeAttr('checked')
         .removeAttr('selected');
         $('#search_advanced_pre_model_category').val('');
         $('#search_advanced_pre_model_id').val('');
         $('#Author_summary').parent().parent().remove();
         $('#Keyword_summary').parent().parent().remove();
         $('#Topicalterm_summary').parent().parent().remove();
         $('#Book_search_advanced_pre').focus();
         $('#Search-grid').html('');"
    )
  )
); ?>
</div>
<?php $this->endWidget(); ?>

<?php
if($author)
{
  $this->renderPartial('//author/_list', array('model'=>$author));
}
if($keyword)
{
  $this->renderPartial('//keyword/_list', array('model'=>$keyword));
}
if($topicalterm)
{
  $this->renderPartial('//topicalterm/_list', array('model'=>$topicalterm));
}
?>


<?php
$author = new Author();
$arrColumnsAll = array(
  'id' => array(
  	'name' => 'id',
  ),
  'status_id' =>array(
  	'name' => 'status_id',
  	'value' => '$data->statusBadge',
  	'type' => 'raw',
  	'filter' => CHtml::listData(Lookup::lookupList('book_status'), 'id', 'title'),
  ),
  'sn' => array(
  	'name' => 'sn',
  ),
  'title' => array(
  	'name' => 'title',
  	'type' => 'raw',
  	'value' => Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" ?
			                'CHtml::link($data->title, array("book/update","id"=>$data->id))' :
			                'CHtml::link($data->title, array("book/view","id"=>$data->id))',
  ),
  'titleOriginal' => array(
  	'name' => 'titleOriginal',
  ),
  'titleRemainder' => array(
  	'name' => 'titleRemainder',
  ),
  'titleParallel' => array(
  	'name' => 'titleParallel',
  ),
  'author' => array(
    'name'=>'search_author',
  	'type' => 'raw',
  	'value' => '$data->authorNamesLinks',
  ),
  'partNumber' => array(
  	'name' => 'partNumber',
  ),
  'partTotal' => array(
  	'name' => 'partTotal',
  ),
  'statementOfResponsibility' => array(
  	'name' => 'statementOfResponsibility',
  ),
  'languages' => array(
  	'name' => 'languages',
  	'filter' => CHtml::listData(Lookup::lookupList('language'), 'title', 'title'),
  ),
  'ddc' => array(
  	'name' => 'ddc',
  ),
  'number' => array(
  	'name' => 'number',
  ),
  'ageGroup' => array(
  	'name' => 'ageGroup',
  ),
  'pubPublisher' => array(
  	'name' => 'pubPublisher',
  ),
  'pubPlace' => array(
  	'name' => 'pubPlace',
  ),
  'ageGroup' => array(
  	'name' => 'ageGroup',
  ),
  'editionStatement' => array(
  	'name' => 'editionStatement',
  ),
  'collation' => array(
  	'name' => 'collation',
  ),
  'size' => array(
  	'name' => 'size',
  ),
  'ISBN' => array(
  	'name' => 'ISBN',
  ),
  'ISSN' => array(
  	'name' => 'ISSN',
  ),
  'seriesName' => array(
  	'name' => 'seriesName',
  ),
  'seriesPartNumber' => array(
  	'name' => 'seriesPartNumber',
  ),
  'seriesPartName' => array(
  	'name' => 'seriesPartName',
  ),
  'notes' => array(
  	'name' => 'notes',
  ),
  'checkoutCount' => array(
  	'name' => 'checkoutCount',
  ),
  'creationDate' => array(
  	'name' => 'creationDate',
  ),
  'deletionDate' => array(
  	'name' => 'deletionDate',
  ),
);

$arrColumns = array();
if($model->advancedSearchCustomSelectedAttributes && count($model->advancedSearchCustomSelectedAttributes ))
{
  foreach($model->advancedSearchCustomSelectedAttributes as $attr)
  {
    if(array_key_exists($attr, $arrColumnsAll))
      $arrColumns[$attr] = $arrColumnsAll[$attr];
  }
}
else
{
  foreach($model->defaultSearchAttributes as $attr)
  {
    if(array_key_exists($attr, $arrColumnsAll))
      $arrColumns[$attr] = $arrColumnsAll[$attr];
  }
}

$arrColumns[] = array(
	'htmlOptions' => array('nowrap'=>'nowrap'),
	'class'=>'bootstrap.widgets.TbButtonColumn',
	'template'=>'{view} {update} {trash} {delete}',
	'filter' => 'bla',

/*
                        'header'=>$clearButton,
                        'filterHtmlOptions'=>array(
                              'onclick'=>$jsClear,
                              'class'=>'clearFilter'
                         ),
*/
	'buttons' => array(
		'update' => array(
			'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
			'icon' => 'pencil',
		),
		'trash' => array(
			'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
			'icon' => 'trash',
		),
		'delete' => array(
			'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
			'icon' => 'remove',
		),
	),
);



if( ! $bHideResults)
{
  $this->widget('bootstrap.widgets.TbGridView', array(
  	'id'=>'Search-grid',
  	'dataProvider'=>$model->search(),

  	'filter'=>$model,
  	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',

  	'selectableRows'=>1,
  	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
  	'pager' => array(
  	  'class' => 'bootstrap.widgets.TbPager',
  	  'displayFirstAndLast' => true,
  	),

  	'columns'=>$arrColumns,
  ));
}

// print("bla");
// print($clearButton);

?>
