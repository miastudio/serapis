<?php
/* @var $this clientController */
/* @var $model client */

$this->breadcrumbs=array(
	'Book'=>array('admin'),
	'Search',
);

$this->menu=array(
	array('label'=>'Advanced Search', 'url'=>array('search')),
// 	array('label'=>'Create Book', 'url'=>array('create')),
	array('label'=>'Clear', 'url'=>array('admin')),
);
?>

<h1>Search book</h1>
<?php
if(Yii::app()->user->isGuest)
{
	print("To get your book, note both shelf number and specifier");
}

$dataProvider=$model->search();

// Determine the JavaScript to reset the filters.
// 'window.location.href' should be improved upon but is ok in most cases.
/*
$clearFilterData=CJavaScript::encode(
        array(
                'data' => array(
                        get_class($model) . '[]' => '',
                        $dataProvider->sort->sortVar => ' ',
                        $dataProvider->pagination->pageVar => 1
                ),
                'url' => new CJavaScriptExpression('window.location.href')
        ));
$jsClear=new CJavaScriptExpression("jQuery('#Book-grid').yiiGridView('update',$clearFilterData);");
$clearButton=CHtml::link(Yii::t('app','Clear'),'',array('onclick'=>$jsClear));
*/

$columns = array(
		"sn" => array(
			'name' => 'sn',
			'value' => '$data->barcode',
// 			'type' => 'raw',
// 			'filter' => CHtml::listData(Lookup::lookupList('activity_status'), 'id', 'title'),
		),
		"title" => array(
			'name' => 'title',
			'type' => 'raw',
			'value' => Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" ?
			                'CHtml::link($data->title, array("book/update","id"=>$data->id)).$data->extraTitlesRich' :
			                'CHtml::link($data->title, array("book/view","id"=>$data->id)).$data->extraTitlesRich',
		),
		"authorNameCache" => array(
// 			'header'=>$author->getAttributeLabel('authorName'),
            'name'=>'authorNameCache',
			'type' => 'raw',
			'value' => '$data->authorNamesLinks',
// 			'value' => $model->search_author ? '$data->authorNamesLinks' : '$data->firstAuthorOnlyNameLink',
		),
		"languages" => array(
			'name' => 'languages',
// 			'type' => 'raw',
			'filter' => CHtml::listData(Lookup::lookupList('language'), 'title', 'title'),
		),
		"ddc" => array(
			'name' => 'ddc',
		),
		"number" => array(
			'name' => 'number',
		),
		"ageGroup" => array(
			'name' => 'ageGroup',
		),
		"status_id" => array(
			'name' => 'status_id',
			'value' => '$data->statusBadge',
			'type' => 'raw',
			'filter' => CHtml::listData(Lookup::lookupList('book_status'), 'id', 'title'),
		),
		'dueDate' => array(
    		'name' => 'dueDate',
            'filter' => false,
		),
		'borrowerName' => array(
    		'name' => 'borrowerName',
    		'value' => '$data->borrowerNameLink',
    		'type' => 'raw',
            'filter' => false,
		),
		"buttons" => array(
            'header'=>CHtml::link('Clear', 'admin', array('class'=>'clearbutton')),
/*
            'filterHtmlOptions'=>array(
              'onclick'=>$jsClear,
              'class'=>'clearFilter'
            ),
*/

			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update}',

			'buttons' => array(
				'update' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'pencil',
				),
				'trash' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'trash',
				),
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'remove',
				),
			),
		),
	);

if(Yii::app()->user->isGuest)
{
    unset($columns['sn']);
    unset($columns['borrowerName']);
}
if(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")
{
    unset($columns['dueDate']);
    unset($columns['borrowerName']);
}

$author = new Author();
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Book-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',

	'selectableRows'=>1,
// 	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),
    'enableHistory' => 'true',
	'columns'=>$columns,
    'afterAjaxUpdate'=>'afterAjaxUpdate',
));

Yii::app()->clientScript->registerScriptFile(Yii::app()->getBaseUrl(true) . '/js/focuslastfield.js', CClientScript::POS_END);

if(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")
    $fieldname = "Book_sn";
else
    $fieldname = "Book_title";

Yii::app()->clientScript->registerScript(
    'focus_field',
    '$("#'.$fieldname.'").focus();',
    CClientScript::POS_READY
);

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>