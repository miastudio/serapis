<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'My Account',
);
?>

<h1>My Account</h1>
<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
  'type' => 'horizontal',
)); ?>


<?php echo $form->errorSummary($model); ?>

<fieldset>
<?php
	echo $form->textFieldRow(
            $model,
            'type_id',
            array('disabled' => true, 'value'=>Lookup::item('user_type',$model['type_id']))
        );

	echo $form->textFieldRow(
            $model,
            'name'
        );

	echo $form->textFieldRow(
            $model,
            'email'
        );

	echo $form->textFieldRow(
            $model,
            'newPassword',
            array('placeholder'=>'******')
        );

	echo $form->dropDownListRow(
    $model,
    'background_id',
    Lookup::items('user_background'),
    array('empty'=>'--Select--')
  );
?>
</fieldset>

    <div class="form-actions">
        <?php
				$this->widget(
				    'bootstrap.widgets.TbButton',
				    array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Save')
				);?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('user/dashboard');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->