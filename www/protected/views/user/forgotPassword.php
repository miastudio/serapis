<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */


$this->pageTitle=Yii::app()->name . ' - Forgot password';
$this->breadcrumbs=array(
	'Forgot password',
);
?>

<div class="sbox login">
<h1>Forgot Password</h1>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>


		<?php echo $form->labelEx($model,'verifyCode'); ?>
<?php
echo '<div>'; $this->widget('CCaptcha'); echo '</div>';
echo CHtml::activeTextField($model,'verifyCode');
/* $this->widget("CCaptcha", array('buttonLabel'=>'change one for me', 'buttonOptions'=>array('style'=>'margin-bottom:20px;'))); */
?>
		<?php echo $form->error($model,'verifyCode'); ?>
		<br/>

		<?php
		//echo CHtml::submitButton('Get new Password');
$this->widget(
    'bootstrap.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Reset password')
);
		?>

<?php $this->endWidget(); ?>

</div><!-- form -->