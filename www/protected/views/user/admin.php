<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Manage Staff',
);

$this->menu=array(
	array('label'=>'Create Staff', 'url'=>array('create')),
	array('label'=>'Export', 'url'=>array('create')),

);
?>

<h1>Manage Staff</h1>
<div class="sportsmenuupper">
<?php
$this->widget(
  'bootstrap.widgets.TbButtonGroup',
  array(
    'size' => 'small',
    'buttons' => $this->menu,
  )
);
?>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),

	'selectableRows'=>1,
	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'/id/"+$.fn.yiiGridView.getSelection(id);}',

	'columns'=>array(
		array(
			'name' => 'type_id',
			'type' => 'raw',
			'value' => '$data->typeBadge',
			'filter'=> CHtml::listData($model->lookupList('user_type'), 'id', 'title'),
		),
		'email',
		'name',
		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}',
		),
	),
)); ?>
