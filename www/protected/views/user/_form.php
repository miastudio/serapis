<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'type_id'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

<fieldset>

<?php

	echo $form->dropDownListRow(
    $model,
    'type_id',
    Lookup::items('user_type'),
    array('empty'=>'--Select--')
  );

	echo $form->textFieldRow(
	  $model,
	  'name'
  );

	echo $form->textFieldRow(
    $model,
    'email'
  );

	echo $form->textFieldRow(
    $model,
    'password'
  );

	echo $form->textFieldRow(
    $model,
    'newPassword'
/*
    array(
      'htmlOptions' => array(
        'placeholder' => 'Leave blank to leave unchanged',
      ),
    )
*/
  );

  echo $form->dropDownListRow(
    $model,
    'background_id',
    Lookup::items('user_background'),
    array('empty'=>'--Select--')
  );
?>
</fieldset>

    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('user/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>

<?php $this->endWidget(); ?>