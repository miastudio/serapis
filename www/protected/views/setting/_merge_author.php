<div class="control-group">
	<label class="control-label" for="<?php echo "merge_author_to"; ?>"><?php echo $model->getAttributeLabel('merge_author_from'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#merge_author_from').focus();">
<?php
	echo $form->hiddenField(
    $model,
    'merge_author_from_id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Merge_author_from',
		'sourceUrl'=>$this->createUrl('author/ajaxItem'),
		'value'=>	Author::model()->findByPk($model->merge_author_from_id)->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Setting_merge_author_from_id').val(ui.item['id']);
			}",
    ),
	));
?>
		<span class="add-on"><i class="fa fa-times"></i></span>
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="<?php echo "merge_author_to"; ?>"><?php echo $model->getAttributeLabel('merge_author_to'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#merge_author_to').focus();">
<?php
	echo $form->hiddenField(
    $model,
    'merge_author_to_id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Merge_author_to',
		'sourceUrl'=>$this->createUrl('author/ajaxItem'),
		'value'=>Author::model()->findByPk($model->merge_author_to_id)->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Setting_merge_author_to_id').val(ui.item['id']);
			}",
    ),
	));
?>
		<span class="add-on"><i class="fa fa-check"></i></span>
		</div>
	</div>
</div>