<?php
// print_r($book	);

$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Book-grid',
	'dataProvider'=>$book->search(),
	'filter'=>$book,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
  'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',

	'selectableRows'=>1,
	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),
    'enableSorting' => false,
    'filterPosition'=>'',

	'columns'=>array(
		'sn',
		'ddc',
		'number',
		'creationDate',
	),
));
?>