<div class="control-group">
	<label class="control-label" for="<?php echo "merge_topicalterm_to"; ?>"><?php echo $model->getAttributeLabel('merge_topicalterm_from'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#merge_topicalterm_from').focus();">
<?php
	echo $form->hiddenField(
    $model,
    'merge_topicalterm_from_id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Merge_topicalterm_from',
		'sourceUrl'=>$this->createUrl('topicalterm/ajaxItem'),
		'value'=>	Topicalterm::model()->findByPk($model->merge_topicalterm_from_id)->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Setting_merge_topicalterm_from_id').val(ui.item['id']);
			}",
    ),
	));
?>
		<span class="add-on"><i class="fa fa-times"></i></span>
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="<?php echo "merge_topicalterm_to"; ?>"><?php echo $model->getAttributeLabel('merge_topicalterm_to'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#merge_topicalterm_to').focus();">
<?php
	echo $form->hiddenField(
    $model,
    'merge_topicalterm_to_id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Merge_topicalterm_to',
		'sourceUrl'=>$this->createUrl('topicalterm/ajaxItem'),
		'value'=>Topicalterm::model()->findByPk($model->merge_topicalterm_to_id)->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Setting_merge_topicalterm_to_id').val(ui.item['id']);
			}",
    ),
	));
?>
		<span class="add-on"><i class="fa fa-check"></i></span>
		</div>
	</div>
</div>