<h1>Print labels</h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'book-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'sn'),
)); ?>
<?php
echo CHtml::hiddenField('activeTab' , $activeTab, array('id' => 'activeTab'));
echo CHtml::hiddenField('bExport' , '', array('id' => 'bExport'));

$printDate = $form->datepickerRow(
    $model,
    'date_from',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
    ),
    array(
    )
  );

$printDate .= $form->datepickerRow(
    $model,
    'date_to',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
    ),
    array(
    )
  );

$printNumber = $form->numberFieldRow(
    $model,
    'barcode_from'
  );

$printNumber .= $form->numberFieldRow(
    $model,
    'barcode_to'
  );

$printIndividual = $form->textAreaRow(
    $model,
    'barcodes',
    array(
	    'rows'=>10,
    )
  );

$this->widget(
  'bootstrap.widgets.TbTabs',
  array(
    'type' => 'tabs', // 'tabs' or 'pills'
		'id'=>'printtabs',
    'tabs' => array(
      array(
        'label' => 'Date range',
        'id' => 'tab_date',
        'content' => $printDate,
    	'icon' => 'fa fa-calendar fa-fw',
    	'active' => $activeTab == 'tab_date' || ! $activeTab ? true : false,
      ),
      array(
      	'label' => 'Number range',
        'id' => 'tab_number',
        'content' => $printNumber,
		'icon' => 'fa fa-list-ol fa-fw',
		'active' => $activeTab == 'tab_number' ? true : false,
      ),
      array(
      	'label' => 'Individual Numbers',
        'id' => 'tab_individual',
        'content' => $printIndividual,
    	'icon' => 'fa fa-ellipsis-h fa-fw',
    	'active' => $activeTab == 'tab_individual' ? true : false,
      ),
    ),
  )
);
?>
<?php
	echo $form->dropDownListRow(
    $model,
    'format_id',
    Lookup::items('labelformat')
  );
?>
<div class="form-actions">
<?php $this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => 'View',
  	'htmlOptions' => array(
			'onclick' => "updateForm('0');",
		),
  )
); ?>

<?php
$this->widget(
  'bootstrap.widgets.TbButton',
  array(
  	'buttonType' => 'submit',
  	'type' => 'warning',
  	'label' => 'Export',
  	'htmlOptions' => array(
			'onclick' => "updateForm('1')",
		),
  )
); ?>
</div>
<script language="javascript">
function updateForm(bExport)
{
    $('#bExport').val(bExport);
    $('#activeTab').val( $('#printtabs ul li.active a').attr('href').replace(/^.*#/, '') );
    return false;
}
</script>
<?php $this->endWidget(); ?>
<?php
if($activeTab)
{
?>
<h2>Preview</h2>
<div>
<?php echo $this->renderPartial('_print_preview', array('book'=>$book), true); ?>
</div>
<?php
}