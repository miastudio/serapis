<h1>Batch Update</h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'batch-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'barcodes'),
)); ?>
<?php
echo CHtml::hiddenField('activeTab' , $activeTab, array('id' => 'activeTab'));
echo CHtml::hiddenField('bExport' , '', array('id' => 'bExport'));

echo $form->textAreaRow(
    $model,
    'barcodes',
    array(
	    'rows'=>10,
    )
  );
?>
<?php
echo $form->dropDownListRow(
    $model,
    'batch_status',
    Lookup::items('book_status')
  );
?>
<div class="form-actions">
<?php $this->widget(
  'bootstrap.widgets.TbButton',
  array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => 'Update Status',
  )
); ?>
</div>
<?php $this->endWidget(); ?>