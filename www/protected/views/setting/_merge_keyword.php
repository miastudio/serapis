<div class="control-group">
	<label class="control-label" for="<?php echo "merge_keyword_to"; ?>"><?php echo $model->getAttributeLabel('merge_keyword_from'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#merge_keyword_from').focus();">
<?php
	echo $form->hiddenField(
    $model,
    'merge_keyword_from_id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Merge_keyword_from',
		'sourceUrl'=>$this->createUrl('keyword/ajaxItem'),
		'value'=>	Keyword::model()->findByPk($model->merge_keyword_from_id)->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Setting_merge_keyword_from_id').val(ui.item['id']);
			}",
    ),
	));
?>
		<span class="add-on"><i class="fa fa-times"></i></span>
		</div>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="<?php echo "merge_keyword_to"; ?>"><?php echo $model->getAttributeLabel('merge_keyword_to'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#merge_keyword_to').focus();">
<?php
	echo $form->hiddenField(
    $model,
    'merge_keyword_to_id'
  );

	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Merge_keyword_to',
		'sourceUrl'=>$this->createUrl('keyword/ajaxItem'),
		'value'=>Keyword::model()->findByPk($model->merge_keyword_to_id)->summary,

    'options'=>array(
	    'minLength'=>'2',
	    'select'=>"js: function(event, ui) {
		 		$('#Setting_merge_keyword_to_id').val(ui.item['id']);
			}",
    ),
	));
?>
		<span class="add-on"><i class="fa fa-check"></i></span>
		</div>
	</div>
</div>