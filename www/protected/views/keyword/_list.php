<h2>Keyword</h2>

<?php
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Keyword-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),

// 	'selectableRows'=>1,
// 	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),

	'columns'=>array(
		array(
			'name' => 'name',
			'type' => 'raw',
			'value' => 'CHtml::link($data->name, array("keyword/view","id"=>$data->id))',
		),
		array(
			'name' => 'notes',
		),

		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{use}',

			'buttons' => array(
				'use' => array(
					'icon' => 'fa fa-check',
					'url' => 'Yii::app()->createUrl("book/search", array("Book[search_advanced_pre_model][category]"=>$data->tableName(),"Book[search_advanced_pre_model][id]"=>$data->id))',
					'label' => 'Use for pre-search',
				),
			),
		),
	),
));
?>