<?php
$this->breadcrumbs=array(
	'Keyword'=>array('admin'),
	'View',
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update','id'=>$model->id)),
);
?>
<h1>View Keyword '<?php echo $model->name; ?>'</h1>

<h2>Linked books</h2>
<div id="checkout">
<?php $this->renderPartial('//book/_list', array('books'=>$model->books, 'arrHide'=>array('authorRole'))); ?>
</div>