<?php
$this->breadcrumbs=array(
	'Keyword'=>array('admin'),
	'Create',
);
?>

<h1>Create Keyword</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>