<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/serapis.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/end_user.css" />
	<link rel="icon" type="image/png" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png" />
	<style>
    body{
<?php
if(Yii::app()->user && Yii::app()->user->background == '1')
    echo '';
else
    echo '    	background-image: url("../images/background01.jpg");'
?>
    }
    </style>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<?php
$this->widget(
  'bootstrap.widgets.TbNavbar',
  array(
	  'type' => null, // null or 'inverse'
	  'brand' => "<img src=\"".Yii::app()->request->baseUrl."/images/logo_navbar.png\"> ".Yii::app()->name,
	  'brandUrl' => array('/book/admin'),
	  'collapse' => true, // requires bootstrap-responsive.css
	  'fixed' => 'top',
	  'items' => array(
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'items' => array(
          array(
          	'label' => 'Check-in',
          	'icon' => 'fa fa-sign-in fa-fw',
          	'visible'=> ! Yii::app()->user->isGuest,
          	'active' => Yii::app()->controller->action->id == "checkin",
          	'url' => '/checkout/checkin'
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'items' => array(
          array(
          	'label' => 'Check-out',
          	'icon' => 'fa fa-sign-out fa-fw',
          	'visible'=> ! Yii::app()->user->isGuest,
          	'active' => Yii::app()->controller->action->id == "checkout",
          	'url' => '/checkout/checkout'
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'items' => array(
          array(
          	'label' => 'Books',
          	'icon' => 'fa fa-book fa-fw',
//           	'visible'=>Yii::app()->user->isGuest,
          	'active' => (Yii::app()->controller->id == "book" && Yii::app()->controller->action->id != "listNew" && Yii::app()->controller->action->id != "listScores" && Yii::app()->controller->action->id != "print"),
            'items' => array(
              array('label' => 'Search', 'url' => '/book/admin', 'icon' => 'fa fa-search fa-fw',),
              array('label' => 'Advanced Search', 'url' => '/book/search', 'icon' => 'fa fa-search-plus fa-fw',),
              '---',
              array('label' => 'List New', 'url' => array('/book/listNew'), 'icon' => 'fa fa-star fa-fw', 'visible'=> Yii::app()->user->isGuest),
//               array('label' => 'My History', 'url' => array('/user/myHistory'), 'icon' => 'fa fa-list-alt fa-fw', 'visible'=> Yii::app()->user->isGuest),
              array('label' => 'Create book', 'url' => array('/book/create'), 'icon' => 'fa fa-plus fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
              '---',
              array('label' => 'Authors List', 'url' => array('/author/admin'), 'icon' => 'fa fa-pencil fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
              array('label' => 'Create Author', 'url' => array('/author/create'), 'icon' => 'fa fa-plus fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
              array('label' => 'Create Feature', 'url' => array('/feature/create'), 'icon' => 'fa fa-plus fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
              array('label' => 'Create Keyword', 'url' => array('/keyword/create'), 'icon' => 'fa fa-plus fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
              array('label' => 'Create Topical Term', 'url' => array('/topicalterm/create'), 'icon' => 'fa fa-plus fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
              '---',
            array('label' => 'Print', 'url' => array('/setting/print'), 'icon' => 'fa fa-print fa-fw', 'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
            array('label' => 'Merge', 'url' => array('/setting/merge'), 'icon' => 'fa fa-compress fa-fw', 'visible'=>Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
            array('label' => 'Status (batch)', 'url' => array('/setting/batch'), 'icon' => 'fa fa-list fa-fw', 'visible'=>!Yii::app()->user->isGuest),

            ),
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'items' => array(
          array(
          	'label' => 'Users',
          	'icon' => 'fa fa-users fa-fw',
          	'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin" || Yii::app()->user->roles == "staff"),
          	'active' => (Yii::app()->controller->id == "person"),
          	'items' => array(
              array('label' => 'List', 'url' => array('/person/admin'), 'icon' => 'fa fa-list-alt fa-fw',),
              array('label' => 'Create', 'url' => array('/person/create'), 'icon' => 'fa fa-plus fa-fw',),

          	),
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'items' => array(
          array(
          	'label' => 'Lists',
          	'icon' => 'fa fa-list-alt fa-fw',
//           	'visible'=> ! Yii::app()->user->isGuest,
          	'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
          	'active' => Yii::app()->controller->action->id == "overdue" || Yii::app()->controller->action->id == "listNew" || Yii::app()->controller->action->id == "listScores" || Yii::app()->controller->action->id == "stats",
            'items' => array(
              array('label' => 'Overdue', 'url' => '/checkout/overdue', 'icon' => 'fa fa-clock-o fa-fw',),
              array('label' => 'New', 'url' => '/book/listNew', 'icon' => 'fa fa-star fa-fw',),
              array('label' => 'Scores', 'url' => '/book/listScores', 'icon' => 'fa fa-music fa-fw',),
              array('label' => 'Checkouts', 'url' => '/checkout/stats', 'icon' => 'fa fa-exchange fa-fw',),
            ),
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'htmlOptions' => array('class' => 'pull-right role-'.Yii::app()->user->roles),
        'items' => array(
          array(
            'label' => Yii::app()->user->name,
            'url' => '#',
            'icon'=>'fa fa-user fa-fw',
          	'visible'=>!Yii::app()->user->isGuest,
          	'active'=>Yii::app()->controller->action->id == "myProfile",
            'items' => array(
              array('label' => 'My Account', 'url' => array('/user/myProfile'), 'icon' => 'fa fa-cog fa-fw',),
              '---',
              array('label' => 'Logout', 'url' => array('/user/logout'), 'icon' => 'fa fa-sign-out fa-fw',),
            ),
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'htmlOptions' => array('class' => 'pull-right'),
        'items' => array(
          array(
          	'label' => 'Settings',
          	'url' => '#',
          	'icon' => 'fa fa-gear fa-fw',
          	'visible'=>!Yii::app()->user->isGuest && (Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
          	'active'=> Yii::app()->controller->id == "lookup" || Yii::app()->controller->id == "default" || Yii::app()->controller->id == "attachment" || Yii::app()->controller->id == "captchahistory" || Yii::app()->controller->id == "user" && Yii::app()->controller->action->id != "myProfile" || Yii::app()->controller->action->id == "print" ,
          	'items' => array(
              array('label' => 'Staff', 'url' => array('user/admin'), 'icon' => 'fa fa-users fa-fw',),
              '---',
              array('label' => 'Lookups', 'url' => array('/lookup/admin'), 'icon' => 'fa fa-external-link fa-fw'),
              array('label' => 'Backup', 'url' => array('/backup/default'), 'icon' => 'fa fa-save fa-fw',),
              array('label' => 'Caches', 'url' => array('setting/cache'), 'icon' => 'fa fa-inbox fa-fw',),
              '---',
			  array('label' => 'Mail Queue', 'url' => array('/mailqueue/admin'), 'icon' => 'fa fa-envelope-o fa-fw'),
          	),
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'htmlOptions' => array('class' => 'pull-right'),
        'items' => array(
          array(
						'label' => 'Login', 'url' => array('/user/login'), 'icon' => 'fa fa-sign-in fa-fw',
						'visible'=>Yii::app()->user->isGuest,
					),
				),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'htmlOptions' => array('class' => 'pull-right'),
        'items' => array(
          array(
          	'label' => 'Help',
          	'icon' => 'fa fa-question-circle fa-fw',
          	'visible'=> Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin",
          	'active' => (Yii::app()->controller->id == "help"),
            'items' => array(
              array('label' => 'View', 'url' => '/help/view', 'icon' => 'fa fa-eye fa-fw',),
              array('label' => 'Update', 'url' => '/help/admin', 'icon' => 'fa fa-list fa-fw',),
            ),
          ),
        ),
      ),
      array(
        'class' => 'bootstrap.widgets.TbMenu',
        'htmlOptions' => array('class' => 'pull-right'),
        'items' => array(
          array(
          	'label' => 'Help',
          	'icon' => 'fa fa-question-circle fa-fw',
          	'visible'=> ! (Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin"),
          	'active' => (Yii::app()->controller->id == "help"),
          	'url' => '/help/view',
          ),
        ),
      ),
	  ),
  )
);
// Icons
// http://fortawesome.github.io/Font-Awesome/icons/
// http://getbootstrap.com/components/
?>
<?php
// Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/addjquery.js',CClientScript::POS_HEAD);


$this->widget('bootstrap.widgets.TbAlert', array(
    'block' => true,
    'fade' => true,
    'closeText' => '&times;', // false equals no close link
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
    'alerts' => array( // configurations per alert type
        // success, info, warning, error or danger
        'success' => array('closeText' => '&times;'),
        'info', // you don't need to specify full config
        'warning' => array('block' => false, 'closeText' => false),
        'error' => array('block' => false, 'closeText' => false)
    ),
));

// Hide success flashes
/*
Yii::app()->clientScript->registerScript(
   'myHideEffect',
   '$(".alert-success").animate({opacity: 1.0}, 2000).fadeOut("slow");',
   CClientScript::POS_READY
);
*/

// Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-ui-no-conflict.min.js',CClientScript::POS_HEAD);
?>

<?php echo $content; ?>
<div class="sbreadcrumbbar">
<?php
$this->widget(
    'bootstrap.widgets.TbBreadcrumbs',
		array(
		  'links' => $this->breadcrumbs,
		  'separator' => ' //',
		)
);
?>
</div>
<div class="smenu">
<?php
$this->widget(
    'bootstrap.widgets.TbButtonGroup',
    array(
        'size' => 'small',
        'buttons' => $this->menu,
    )
);
?>
</div>
<div class="spacer">
</div>

</body>
</html>