<?php
/* @var $this HousingassetController */
/* @var $model Housingasset */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->summary,
	'view'
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'List', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->summary; ?></h1>
<div class="badgelist"><?php
echo $model->showBadges();
?></div>

<?php
 $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'asynctoId',
		'masterlistId',
		'aurovillename',
		'name',
		'surname',
		'category',
		'community',
	),
));
?>

<h2>Contact</h2>
<?php $this->renderPartial('//contactdetail/_list', array('models'=>$model->contactdetails)); ?>

<h2>Lending History</h2>
<div id="Checkout">
<?php $this->renderPartial('//checkout/_list', array('person_id'=>$model->id, 'arrHide'=>array('person','extend'))); ?>
</div>