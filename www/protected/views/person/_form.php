<?php
/* @var $this cersonController */
/* @var $model person */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'person-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
)); ?>



    <fieldset>
	<?php echo $form->errorSummary($model); ?>
<?php
	echo $form->textFieldRow(
    $model,
    'aurovillename',
    array(
			'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );

	echo $form->textFieldRow(
    $model,
    'name',
    array(
			'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );

	echo $form->textFieldRow(
    $model,
    'surname',
    array(
			'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );

	echo $form->dropDownListRow(
    $model,
    'community_id',
    Lookup::items('community'),
    array(
    	'empty'=>'--Select--',
	    'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );

  if($model->address)
	echo $form->textFieldRow(
        $model,
        'address',
        array('disabled' => true)
    );

	echo $form->dropDownListRow(
    $model,
    'category_id',
    Lookup::items('person_category'),
    array(
	    'empty'=>'--Select--',
		)
  );

/*
	echo $form->dropDownListRow(
    $model,
    'status_id',
    Lookup::items('person_status'),
    array(
	    'empty'=>'--Select--',
	    'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );
*/

	echo $form->textFieldRow(
    $model,
    'workplace'
  );

	echo $form->numberFieldRow(
    $model,
    'limitBooks'
  );

	echo $form->numberFieldRow(
    $model,
    'limitDays'
  );

	echo $form->numberFieldRow(
    $model,
    'limitExtensions'
  );

	echo $form->dropDownListRow(
    $model,
    'overdue_id',
    Lookup::items('overdue'),
    array(
    	'empty'=>'--Select--',
		)
  );

	echo $form->textFieldRow(
    $model,
    'asynctoId'
  );

/*
	echo $form->textFieldRow(
    $model,
    'masterlistId',
    array(
			'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );
*/

	echo $form->dropDownListRow(
    $model,
    'presence_id',
    Lookup::items('person_presence'),
    array(
    	'empty'=>'--Select--',
	    'disabled' => $model->asynctoId ? true : false,
		),
    array(
	   	'append' => $model->asynctoId ? '<i class="fa fa-refresh"></i>' : '',
	  )
  );

  echo $form->datepickerRow(
    $model,
    'creationDate',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
      'htmlOptions' => array(
          'class' => 'span2',
      )
    ),
    array(
    	'append' => '<i class="icon-calendar" onClick="$(\'#Person_creationDate\').focus();"></i>',
    )
  );

  echo $form->datepickerRow(
    $model,
    'deletionDate',
    array(
      'options' => unserialize(Lookup::item('user_settings', 'ui_calendar_format')),
      'htmlOptions' => array(
          'class' => 'span2',
      )
    ),
    array(
    	'append' => '<i class="icon-calendar" onClick="$(\'#Person_deletionDate\').focus();"></i>',
    )
  );

  echo $form->textAreaRow(
    $model,
    'notes',
    array('rows'=>10)
  );

?>

    </fieldset>

    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'button',
                'type' => 'primary',
                'id' => 'usersave',
                'label' => $model->isNewRecord ? 'Create' : 'Save',
                'htmlOptions' => array(
                    'onclick' => '$("#usersave").attr("disabled", true);$( "#person-form" ).submit();',
                ),
            )
        ); ?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('person/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>

<?php $this->endWidget(); ?>