<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Authors'=>array('admin'),
	$model->summary=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'View', 'url'=>array('view','id'=>$model->id)),
	array('label'=>'List', 'url'=>array('admin')),
);
?>

<h1>Update Author '<?php echo $model->summary; ?>'</h1>
<div class="badgelist"><?php
echo $model->showBadges();
?></div>

<?php
if($model->PreviousId)
{
	print("<div class=\"previousid\">");
	echo CHtml::link('<i class="fa fa-arrow-left fa-lg"></i>',array('update', 'id'=>$model->PreviousId));
	print("</div>");
}
if($model->NextId)
{
	print("<div class=\"nextid\">");
	echo CHtml::link('<i class="fa fa-arrow-right fa-lg"></i>',array('update', 'id'=>$model->NextId));
	print("</div>");
}
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<h2>Publications</h2>
<div id="checkout">
<?php $this->renderPartial('//book/_list', array('books'=>$model->books, 'arrHide'=>array('languages'), 'author'=>$model)); ?>
</div>
