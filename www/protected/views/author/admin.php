<?php
/* @var $this clientController */
/* @var $model client */

$this->breadcrumbs=array(
	'Author'=>array('admin'),
	'Search',
);

$this->menu=array(
	array('label'=>'Create Author', 'url'=>array('create')),
);
?>

<h1>Search Author</h1>

<?php
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Author-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',
    'enableHistory' => 'true',

// 	'selectableRows'=>1,
// 	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),
	'columns'=>array(
		array(
			'name' => 'title',
		),
		array(
			'name' => 'name',
			'type' => 'raw',
			'value' => 'CHtml::link($data->name, array("author/view","id"=>$data->id))',
		),
		array(
			'name' => 'nameFullerForm',
		),
		array(
			'name' => 'lifetime',
		),
		array(
			'name' => 'notes',
		),

		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update} {trash} {delete}',

			'buttons' => array(
				'update' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'pencil',
				),
				'trash' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'trash',
				),
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin")',
					'icon' => 'delete',
				),
			),
		),
	),
));

 Yii::app()->clientScript->registerScript(
    'focus_name',
    '$("#Author_name").focus();',
   CClientScript::POS_READY
);

if( $_GET['showall'] != 1)
    echo CHtml::link( "Show all", array_merge(array(Yii::app()->request->getPathInfo()), $_GET, array('showall'=>true)));
?>