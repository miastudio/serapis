<?php
/* @var $this meterController */
/* @var $model f */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'author-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
  'type' => 'horizontal',
  'focus' => array($model, 'name'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
    <fieldset>
	<div id="metadata_cover">
	</div>

<?php
	echo $form->textFieldRow(
    $model,
    'title'
  );

	echo $form->textFieldRow(
    $model,
    'name'
  );

	echo $form->textFieldRow(
    $model,
    'nameFullerForm'
  );

	echo $form->textFieldRow(
    $model,
    'lifetime'
  );

/*
	echo $form->textAreaRow(
    $model,
    'notes'
  );
*/
?>
<div class="control-group">
	<label class="control-label" for="<?php echo "notes"; ?>"><?php echo $model->getAttributeLabel('notes'); ?></label>
	<div class="controls">
		<div class="input-append" onClick="$('#author_name').focus();">
<?php
	$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'Author[notes]',
		'sourceUrl'=>$this->createUrl('author/ajaxNotes'),
		'value'=>$model->notes,

    'options'=>array(
	    'minLength'=>'3',
    ),
    'htmlOptions'=>array(
//         'class' => 'span4',
        'rows' => '5',
    )
	));

?>
		<span class="add-on"><i class="fa fa-ellipsis-h"></i></span>
		</div>
	</div>
</div>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('author/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>
<?php $this->endWidget(); ?>