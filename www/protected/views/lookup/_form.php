<?php
/* @var $this LookupController */
/* @var $model Lookup */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
)); ?>

	<?php echo $form->errorSummary($model); ?>

<fieldset>
<?php

if(isset($model->section))
	echo $form->textFieldRow(
      $model,
      'section',
      array('disabled' => true, 'value'=>Lookup::item('lookup_type', $model->section))
  );
else
  	echo $form->dropDownListRow(
      $model,
      'section',
			Lookup::items('lookup_type')
    );

	echo $form->textFieldRow(
      $model,
      'code',
      array('placeholder'=>'auto-generated')
  );

	echo $form->textFieldRow(
      $model,
      'description'
  );

?>
</fieldset>

    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => $model->isNewRecord ? 'Create' : 'Save'
            )
        ); ?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('lookup/admin');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset',
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>


<?php $this->endWidget(); ?>