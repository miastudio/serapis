<?php
/* @var $this LookupController */
/* @var $model Lookup */

//print(Lookup::item("lookup_type",$data->section));

$this->breadcrumbs=array(
	'Manage Lookups'=>array('lookup/admin'),
	Lookup::item('lookup_type',$model->section),
	$model->description,
);
?>

<h1>Update <?php 
//print(Lookup::item('lookup_type',$model->section)."/".$model->description.""); 
print(Lookup::item('lookup_type',$model->section)." '".$model->description."'"); 
?> </h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>