<?php
/* @var $this LookupController */
/* @var $model Lookup */

$this->breadcrumbs=array(
	'Manage Lookups',
);

$this->menu=array(
	array('label'=>'Create Lookup', 'url'=>array('create')),
);
?>

<h1>Manage Lookups</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'lookup-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
	'selectableRows'=>1,
	'columns'=>array(
		array(
			'name' => 'section',
			'type'=>'raw',
			'value'=>'Lookup::item("lookup_type",$data->section)',
      'filter'=> Lookup::items('lookup_type'),
		),
		array(
			'name' => 'code',
			'filter' => false,
		),
		'description',
		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin")',
				),
            ),
		),
	),
)); ?>
