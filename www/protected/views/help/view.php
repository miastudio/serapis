<?php
/* @var $this HousingassetController */
/* @var $model Housingasset */

$this->breadcrumbs=array(
	'Authors'=>array('index'),
	$model->summary,
	'view'
);

$this->menu=array(
	array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'List', 'url'=>array('admin')),
);
?>

<h1><?php echo ucfirst($model->summary); ?> Help</h1>
<?php
echo $model->content;
?>