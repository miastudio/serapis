<?php
/* @var $this clientController */
/* @var $model client */

$this->breadcrumbs=array(
	'Help'=>array('admin'),
	'Update',
);

$this->menu=array(
// 	array('label'=>'Create Author', 'url'=>array('create')),
);
?>

<h1>Update Help</h1>

<?php
	$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'Author-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
  'rowCssClassExpression' => '( $row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0] ) . ( $data->deletionDate ? " deleted" : null )',

	'selectableRows'=>1,
	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'?id="+$.fn.yiiGridView.getSelection(id);}',
	'pager' => array(
	  'class' => 'bootstrap.widgets.TbPager',
	  'displayFirstAndLast' => true,
	),

	'columns'=>array(
		array(
			'name' => 'id',
			'value' => '$data->levelBadge',
			'type' => 'raw',
		),
		array(
			'name' => 'content',
			'value' => '$data->contentSummary',
		),

		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update}',

			'buttons' => array(
				'update' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'pencil',
				),
			),
		),
	),
));
?>