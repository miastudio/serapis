<?php
$model = new Book_Feature();
$model->unsetAttributes();
$model->book_id = $book_id;

// print_r($model);
// die();

$gridDataProvider=$model->search();

$buttonPlus = CHtml::ajaxLink(
	CHtml::tag('i', array('class'=>'icon-plus'), ""),
	$this->createUrl('book_Feature/ajaxCreate'),
	array(
		'type' => 'POST',
		'data' => array(
			'Book_Feature[book_id]'      => $model->book_id,
			'Book_Feature[feature_id]'   => 'js:$("#feature_id").val()',
		),
		'success' => 'function(html){ $.fn.yiiGridView.update("book_feature-grid"); }',
		'error' => 'function(html){ console.log(html.responseText); alert(html.responseText); }',
	),
	array(
		'data-original-title' => 'Assign to book',
		'data-toggle' => "tooltip",
	)
);

$buttonCreate = CHtml::link(
    CHtml::tag('i', array('class'=>'fa fa-list'), ""),
    Yii::app()->createUrl('feature/create'),
    array(
        'target'=>'_blank',
        'data-original-title' => 'Create',
		'data-toggle' => "tooltip",
	),
	true
);
$gridColumns = array(
		array(
			'header' => $model->getAttributeLabel('feature_id'),
			'name' => 'feature_id',
			'value' => 'CHtml::link($data->feature->summary, array("feature/update", "id"=>$data->feature_id), array("target"=>"_blank"), true)',
			'type' => 'raw',
			'footer' => CHtml::hiddenField('feature_id' , 'value', array('id' => 'feature_id')).$this->widget(
				'zii.widgets.jui.CJuiAutoComplete',
				array(
			    'name'=>'book_feature_name',
					'sourceUrl'=>$this->createUrl('feature/ajaxItem'),
			    'options'=>array(
			        'minLength'=>'1',
			        'select'=>"js: function(event, ui) {
							 		$('#feature_id').val(ui.item['id']);
								}",
			    ),
			    'htmlOptions'=>array(
						'placeholder' => "New ".$model->getAttributeLabel('feature_id'),
						'class' => 'span-14'
					),
				),
				true),
		),
		array(
			'name' => 'notes',
			'value' => '$data->feature->notes',
		),
		array(
			'class'=> 'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => '(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")',
					'icon' => 'remove',
					'options'=>array('title'=>'Permanently erase'),
					'url' => 'Yii::app()->createUrl("book_Feature/delete", array("id"=>$data->id))',
				),
				'update' => array(
// 					'icon' => 'eye-open',
					'options' => array('title'=>'Update'),
					'url' => 'Yii::app()->createUrl("feature/update", array("id"=>$data->feature_id))',
				),
			),
			'footer' => $buttonPlus." ".$buttonCreate,
		),
);

$this->widget(
  'bootstrap.widgets.TbGridView',
  array(
  	'id'=>'book_feature-grid',
		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
    'dataProvider' => $gridDataProvider,
		'template'=>"{items}\n{pager}",
    'columns' => $gridColumns,
		'pager' => array(
		  'class' => 'bootstrap.widgets.TbPager',
		  'displayFirstAndLast' => true,
		),
		'afterAjaxUpdate'=>"cgridReloadHandlerFeatureBook", // Crucial: no () after function. Calls on Load !!
  )
);

Yii::app()->clientScript->registerScript('cgridReloadHandlerFeatureBook', '
function cgridReloadHandlerFeatureBook(id,data){
    jQuery("#book_feature_name").autocomplete({"minLength":"2","select": function(event, ui) { $("#feature_id").val(ui.item["id"]);},"source":"/feature/ajaxItem"});
}');