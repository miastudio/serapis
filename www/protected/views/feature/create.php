<?php
$this->breadcrumbs=array(
	'Feature'=>array('admin'),
	'Create',
);
?>

<h1>Create Feature</h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>