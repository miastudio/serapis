<?php
$this->breadcrumbs=array(
	'Feature'=>array('admin'),
	'Update',
);
?>
<h1>Update Feature '<?php echo $model->name; ?>'</h1>
<div id="Checkout">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>

<h2>Linked books</h2>
<div id="checkout">
<?php $this->renderPartial('//book/_list', array('books'=>$model->books, 'arrHide'=>array('authorRole'))); ?>
</div>