<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Mail Queue',
);
?>

<h1>Mail Queue</h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type' => Lookup::item('user_settings', 'ui_gridview_type'),
	'columns'=>array(
		array(
			'name'=>'template_id',
			'value'=>'$data->templateBadge',
			'type'=>'raw',
			'filter'=>Lookup::items('mailqueue_template'),
		),
		'email',
		array(
			'name'=>'mergevariablesRich',
			'type'=>'raw',
			'filter' => false,
		),
		'creationDate',
		array(
			'name'=>'sendingresult_id',
			'value'=>'$data->sendingresultBadge',
			'type'=>'raw',
			'filter'=>Lookup::items('mailqueue_sendingresult'),
		),
		'sendingDate',
		array(
			'htmlOptions' => array('nowrap'=>'nowrap'),
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{send}',
			'buttons'=>array(
				'send'=>array(
					'icon' => 'fa fa-envelope-o fa-fw',
					'label' => 'Send',
					'url' => 'Yii::app()->createUrl("mailqueue/send", array("id"=>$data->id))',
					'visible' => ' ! $data->sendingDate',
				),
			),
		),
	),
)); ?>
