<?php
if($edit)
{
?>
<div class="addlink"><i class="fa fa-plus"></i> <?php echo CHtml::link('Add Contact detail', "",  // the link for open the dialog
    array(
        'style'=>'cursor: pointer;',
        'onclick'=>"{addContactdetail(); $('#dialogContactdetail').dialog('open');}",
	       'success'=>"function(data){ console.log('successfully opened create window');}",
        ));
?></div>
<?php
}
?>
<?php
$gridDataProvider = new CArrayDataProvider($models);

$model = new Contactdetail();
$gridColumns = array(
	array(
		'name'=>'contactType',
		'header'=>$model->getAttributeLabel('contactType_id'),
	),
	array(
		'name'=>'contact',
		'header'=>$model->getAttributeLabel('contact'),
	),
	array(
		'htmlOptions' => array('nowrap'=>'nowrap'),
		'class'=>'bootstrap.widgets.TbButtonColumn',
		'deleteButtonUrl'=>'Yii::app()->createUrl("contactdetail/ajaxDelete", array("id"=>$data->id))',
		'template'=> $edit ? '{asyncto} {edit} {delete}' : '{asyncto}',
		'buttons'=>array(
			'asyncto' => array(
				'visible' => '($data->bAsynctoDelete == 1)',
				'label' => '<span class="fa fa-refresh"></span>',
				'options'=>array('title'=>'Managed by ASyncTo'),
//				'url' => 'Yii::app()->createUrl("contactdetail/ajaxEdit", array("id"=>$data->id))',
				'url' => 'Yii::app()->createUrl("#")',
			),
			'edit' => array(
				'visible' => '$data->bAsynctoDelete != 1',
				'label' => '<span class="icon-pencil"></span>',
				'options'=>array('title'=>'Edit'),
				'url' => 'Yii::app()->createUrl("contactdetail/ajaxEdit", array("id"=>$data->id))',
//				'url' => 'Yii::app()->createUrl("#")',
				'click' => 'function(){ editContactdetail($(this).attr("href").match(/\d+/)[0]); return false; }',
			),
			'delete' => array(
				'visible' => '$data->bAsynctoDelete != 1',
				'label' => '<span class="icon-remove"></span>',
				'options'=>array('title'=>'Permanently erase'),
				'url' => 'Yii::app()->createUrl("contactdetail/erase", array("id"=>$data->id))',
// 				'url' => 'Yii::app()->createUrl("#")',
				'click' => <<<EOD
					function() {
						deleteContactdetail(this);
						return false;
					}
EOD
,
			),
		),
	)
);
$this->widget(
    'bootstrap.widgets.TbGridView',
    array(
    		'type' => Lookup::item('user_settings', 'ui_gridview_type'),
        'dataProvider' => $gridDataProvider,
        'template' => "{items}",
				'rowHtmlOptionsExpression' => 'array("id"=>$data->id)',
//				'filter' => $model->search(),
        'columns' => $gridColumns,
    )
);
?>