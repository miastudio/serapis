<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'contactdetail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
  'type' => 'horizontal',
  'htmlOptions'=>array(
     'onsubmit'=>"return false;",/* Disable normal form submit */
     'onkeypress'=>" if(event.keyCode == 13){ send(); } " /* Do ajax call when user presses enter key */
   ),
)); ?>

	<?php echo $form->errorSummary($model); ?>

<fieldset>
<?php
	echo $form->hiddenField(
    $model,
    'id'
  );

	echo $form->hiddenField(
    $model,
    'person_id'
  );

	echo $form->dropDownListRow(
    $model,
    'contactType_id',
    Lookup::items('contact_type'),
    array('empty'=>'--Select--')
  );

 	echo $form->textFieldRow(
    $model,
    'contact'
  );

?>
</fieldset>

<?php
if($saved)
{
	echo "<div class=\"alert in alert-block fade alert-success\">".$saved."</div>";
}
else
{
?>
    <div class="form-actions">

    <?php
			$this->widget(
		    'bootstrap.widgets.TbButton',
		    array(
	        'buttonType' => 'ajax-submit',
	        'type' => 'primary',
	        'label' => $model->isNewRecord ? 'Create' : 'Save',
					'htmlOptions' => array(
						'onclick' => 'js:send(); return false;',
					),
				)
      );?>
				<?php
      $this->widget(
        'bootstrap.widgets.TbButton',
        array(
        	'buttonType' => 'reset',
        	'label' => 'Cancel',
        	'htmlOptions' => array(
						'onclick' => 'bootbox.confirm("Discard changes ?",
														function(confirmed){
							                if(confirmed) {
																$(".ui-dialog-content:first").dialog("close");
							                }
														})',
					),
        )
      );
    ?>
    </div>
<?php
}
?>
<?php $this->endWidget(); ?>

<script language="javascript">

function send()
{
	var data=$("#contactdetail-form").serialize();

  $.ajax({
   type: 'POST',
   url: '<?php echo Yii::app()->createAbsoluteUrl("contactdetail/ajaxEdit"); ?>',
   data:data,
	 success:function(data){
                $('#dialogContactdetail div.divForForm').html(data.div);
              },
   error: function(data) { // if error occured
	   alert("Error occured.please try again");
	   alert(data);
	 },

  dataType:'json'
  });
}
<?php
if($saved)
	echo 'setTimeout(function(){ closeContactdetail('.$person_id.'); }, 500);';
?>
</script>