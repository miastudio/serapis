<?php
function getMDSEnv($environment_var, $default)
{
    if(getenv($environment_var))
    {
        return getenv($environment_var);
    }
    return $default;
}
$mds_app_name = getMDSEnv('MIA_APP_NAME', 'Serapis');
$mds_app_version = getMDSEnv('MIA_APP_VERSION', '1.0.0');

$mds_db_host = getMDSEnv('MIA_DB_HOST', '127.0.0.1');
$mds_db_port = getMDSEnv('MIA_DB_PORT', '3306');
$mds_db_name = getMDSEnv('MIA_DB_NAME', 'serapis');
$mds_db_user = getMDSEnv('MIA_DB_USER', 'serapis');
$mds_db_pass = getMDSEnv('MIA_DB_PASS', '**SET_PASSWORD_IN_.env_FILE**');