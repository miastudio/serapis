<?php
if (file_exists(dirname(__FILE__) . '/_db.local.php')) {
    require_once(dirname(__FILE__) . '/_db.local.php');
}
require('mds.php');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'homeUrl'  => '/book/admin',
    'name'     => ucfirst($mds_app_name).' '.$mds_app_version,


    // preloading 'log' component
    'preload'  => array(
        'log',
        'bootstrap',
    ),

    // autoloading model and component classes
    'import'   => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
//  		'application.extensions.formatCurrency.*',
    ),

    'modules'    => array(
        // uncomment the following to enable the Gii tool
// 		'gii'=>array(
// 			'class'=>'system.gii.GiiModule',
// 			'password'=>'__CHANGE_THIS_PASSWORD__',
// 			// If removed, Gii defaults to localhost only. Edit carefully to taste.
// 			'ipFilters'=>array('127.0.0.1','::1','10.*.*.*','192.168.37.*'),
// 		),
        'backup' => array(),
    ),

    // application components
    'components' => array(
        'user'       => array(
            'class'          => 'MiAWebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            // Requires additional inputs ?
            // https://stackoverflow.com/questions/20116229/session-timeout-not-working-for-chttpsession-in-yii-framework
            // 'class' => 'CDbHttpSession',
            'authTimeout'    => 3600,
        ),
        'bootstrap'  => array(
            'class'          => 'ext.yiibooster.components.Bootstrap',
//        'responsiveCss' => FALSE,
            'fontAwesomeCss' => TRUE,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'rules'          => array(
                'gii'                                  => 'gii',
                'gii/<controller:\w+>'                 => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'    => 'gii/<controller>/<action>',
                'backup'                               => 'backup',
                'backup/<controller:\w+>'              => 'backup/<controller>',
                'backup/<controller:\w+>/<action:\w+>' => 'backup/<controller>/<action>',

                '<controller:\w+>'                  => '<controller>/index',
                '<controller:\w+>/<action:\w+>'     => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>/<title>' => '<controller>/view',
                '<controller:\w+>/<id:\d+>'         => '<controller>/view',
                ''                                  => 'book/admin',
            ),
        ),
        // uncomment the following to use a MySQL database
        'db'         => array(
            'connectionString' => 'mysql:host=' . $mds_db_host . ';port=' . $mds_db_port . ';dbname=' . $mds_db_name,
            'emulatePrepare'   => true,
            'username'         => $mds_db_user,
            'password'         => $mds_db_pass,
            'charset'          => 'utf8',
        ),

        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                                array(
                                    'class'=>'CWebLogRoute',
                                    'enabled' => YII_DEBUG,
                                ),
                */
            ),
        ),
        // http://www.yiiframework.com/forum/index.php/topic/29225-tip-using-phpexcel-with-yii/
        'excel'        => array(
            'class' => 'ext.excel.PHPExcel',
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'     => array(
        // this is used in contact page
        'adminEmail' => '',
        'PHPMail'    => array(
            'host'     => '',
            'username' => '',
            'password' => '',
        ),
        'backup'     => array(
            'directory'   => Yii::app()->basePath . '/../../_backup/',
            'filename'    => 'serapis_backup_',
            'deleteafter' => 30,
        ),
    ),
);