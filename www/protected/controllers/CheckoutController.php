<?php

class CheckoutController extends MiAController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('',),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('checkin','checkout','extend','admin','stats','repair','missing'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('overdue','enqueue'),
				'roles'=>array('admin', 'masteradmin',),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('',),
//				'users'=>array('admin'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Checkout('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Checkout']))
			$model->attributes=$_GET['Checkout'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function actionCheckout()
	{
    	$this->pageTitle = "Check-out";
		if(isset($_GET['Person']) && isset($_GET['Person']['id']))
		{
			$person = Person::model()->findByPk($_GET['Person']['id']);
			if(array_key_exists('notes', $_GET['Person']) && $person->notes != $_GET['Person']['notes'])
			{
    			$person->notes = $_GET['Person']['notes'];
    			$person->save();
                Yii::app()->user->setFlash('success', "Successfully updated notes");
			}

			if(isset($_GET['Book']) && isset($_GET['Book']['sn']) && $_GET['Book']['sn'])
			{
    			if(strlen($_GET['Book']['sn']) != Book::SN_LENGTH)
    			{
                    $book = new Book();
                    $message = "Barcode must be ".Book::SN_LENGTH." digits";
                    $book->addError('sn', $message);
					Yii::app()->user->setFlash('error', $message);
    			}
    			else
    			{
    				$books = Book::model()->findAll(array('condition'=>'sn='.$_GET['Book']['sn']));
    				if( ! count($books))
    				{

    					$message = "Book #".$_GET['Book']['sn']." does not exist!";
    					$book->addError('sn', $message);
    					Yii::app()->user->setFlash('error', $message);
    				}
    				else
    				{
    					$book = $books[0];

    					if($book->status_id == Book::STATUS_OUT)
    					{
    //     					add number, <title> by <name> due by <date>
    						$message = "Book '".$book->summary."' is already checked out by ".$book->currentcheckout->person->name." due by ".$book->currentcheckout->dueDate;
    						$book->addError('sn', $message);
    						Yii::app()->user->setFlash('error', $message);
    					}
    					else
    					{
    						$c = new Checkout();
    						$c->status_id = Checkout::STATUS_OUT;
    						$c->book_id = $book->id;
    						$c->person_id = $person->id;
    						$c->borrowDate = date("Y-m-d");
    						$c->dueDate = date('Y-m-d',strtotime('+'.$person->limitDays.' days',strtotime($c->borrowDate)));

    // move to checkout controller ??
    // add checkout count
    // add last checkout date

    						$c->save(false);

    						$book->status_id = Book::STATUS_OUT;
    						$book->save(false);
    // 						unset($book);

    						Yii::app()->user->setFlash('success', "Successfully checked out '".$book->summary."'");
    					}
    				}
    			}
			}
		}

		if( ! $person)
			$person = new Person();
		if( ! $book)
			$book = new Book();

		$this->render('checkout',array(
			'person'=>$person,
			'book'=>$book,
		));
	}

	public function actionCheckin()
	{
    	$this->pageTitle = "Check-in";
        if(isset($_GET['Person']))
        {
			$person = Person::model()->findByPk($_GET['Person']['id']);
			if($person->notes != $_GET['Person']['notes'])
			{
    			$person->notes = $_GET['Person']['notes'];
    			$person->save();
                Yii::app()->user->setFlash('success', "Successfully updated notes");
//                 print('updated note');
			}
        }


		if(isset($_GET['Book']) && isset($_GET['Book']['sn']) && $_GET['Book']['sn'])
		{

			if(strlen($_GET['Book']['sn']) != Book::SN_LENGTH)
			{
                $book = new Book();
    			$message = "Barcode must be ".Book::SN_LENGTH." digits";
                $book->addError('sn', $message);
				Yii::app()->user->setFlash('error', $message);
			}
			else
			{
    			$books = Book::model()->findAll(array('condition'=>'sn='.$_GET['Book']['sn']));
    			$cc = null;
    			if( ! count($books))
    			{
    				$book = new Book();
    				$message = "Book #".$_GET['Book']['sn']." does not exist!";
    				$book->addError('sn', $message);
    				Yii::app()->user->setFlash('error', $message);
    			}
    			else
    			{
    				$book = $books[0];

    				if($book->status_id == Book::STATUS_IN)
    				{
    					$message = "Book '".$book->summary."' is already checked in";
    					$book->addError('sn', $message);
    					Yii::app()->user->setFlash('error', $message);
    				}
    				elseif($book->status_id == Book::STATUS_REPAIR)
    				{
        				$message = "Book '".$book->summary."' is on repair";
    					$book->addError('sn', $message);
    					Yii::app()->user->setFlash('error', $message);
    				}
    				elseif($book->status_id == Book::STATUS_INCOMING)
    				{
        				$message = "Book '".$book->summary."' is still incoming";
    					$book->addError('sn', $message);
    					Yii::app()->user->setFlash('error', $message);
    				}
    				elseif($book->status_id == Book::STATUS_MISSING)
    				{
        				$message = "Book '".$book->summary."' is missing";
    					$book->addError('sn', $message);
    					Yii::app()->user->setFlash('error', $message);
    				}
    				else
    				{
    					if($book->currentcheckout){
    						$cc = $book->currentcheckout;
    						$cc->returnDate = date("Y-m-d");
    						$cc->status_id = Checkout::STATUS_IN;
    						$cc->save(false);
    					}
    					$book->status_id = Book::STATUS_IN;
    					$book->save(false);
    					Yii::app()->user->setFlash('success', "Successfully checked in '".$book->summary."'");
    // 					$book->sn = null;
    				}
    			}
			}
		}

		if( ! $book)
			$book = new Book();

		$this->render('checkin',array(
			'book'=>$book,
			'cc' => $cc,
		));
	}

    public function actionRepair($id)
    {
        $model = $this->loadBook($id);
        if($model->setRepair())
            Yii::app()->user->setFlash('success', "Marked '".$model->summary."' for repair");
        else
            Yii::app()->user->setFlash('important', "Error marking '".$model->summary."' for repair");
        $this->redirect(array('checkin'));
    }

    public function actionMissing($id)
    {
        $model = $this->loadBook($id);
        if($model->setMissing())
            Yii::app()->user->setFlash('success', "Marked '".$model->summary."' as missing");
        else
            Yii::app()->user->setFlash('important', "Error marking '".$model->summary."' as missing");
        $this->redirect(array('checkin'));
    }

	public function actionExtend($id)
	{
    	$this->pageTitle = "Extend";
		$model = $this->loadModel($id);
        $model->dueDate = date("Y-m-d", strtotime("+".$model->person->limitDays." days", strtotime($model->dueDate)));

		if(isset($_POST['Checkout']) && isset($_POST['Checkout']['dueDate']))
		{
			$model->attributes=$_POST['Checkout'];
			if($model->save(false, array('dueDate')))
			{
				Yii::app()->user->setFlash('success', "Extended successfully till ".$model->dueDate);
				$this->redirect(array('checkout','Person[id]'=>$model->person->id));
			}
			else
				Yii::app()->user->setFlash('success', "Error saving extension");
		}

		$this->render('extend',array(
			'model'=>$model,
		));
	}

	public function actionOverdue()
	{
    	$this->pageTitle = "Overdue";
		$model=new Checkout('search');
		$model->unsetAttributes();  // clear any default values
		$model->status_id = ">=".Checkout::STATUS_OUT; // != does not work
		$model->returnDate = "0000-00-00";

// print_r($model);

        if(Lookup::item('reminder', 'last_processed') != date("Y-m-d"))
        {
          $arrOverdue = $model->search(true)->getData();
          $arrRes = array();
          foreach($arrOverdue as $checkout)
          {
// print($checkout->id."<br/>");
            $r = $checkout->calculateReminderStatus_id();
            $arrRes[$r]++;
            $checkout->save(false);
          }
// print("<br/>");
// die('done');
          Lookup::saveItem('reminder', 'last_processed', date("Y-m-d"));
          $iEmailsToSend = 0;
          foreach($arrRes as $status => $count)
          {
	          if($status == Checkout::REMINDER_STATUS_READY1 ||
		          $status == Checkout::REMINDER_STATUS_READY2 ||
		          $status == Checkout::REMINDER_STATUS_READY3 ||
		          $status == Checkout::REMINDER_STATUS_READY1)
	          $iEmailsToSend += $count;
          }
          Yii::app()->user->setFlash('success', "Updated status of ".count($arrOverdue)." reminders. ".$iEmailsToSend." new emails to send");
        }
		if(isset($_GET['Checkout']))
			$model->attributes=$_GET['Checkout'];
        if( ! $model->status_id || $model->status_id == Checkout::STATUS_IN)
    		$model->status_id = ">".Checkout::STATUS_IN;

		$this->render('overdue',array(
			'model'=>$model,
		));
	}

  public function actionEnqueue()
  {
    $model=new Checkout('search');
    $model->unsetAttributes();  // clear any default values
	$model->status_id = ">=".Checkout::STATUS_OUT;
	$model->returnDate = "0000-00-00";
    $model->search_reminderStatus_id_set = array(Checkout::REMINDER_STATUS_READY1, Checkout::REMINDER_STATUS_READY2, Checkout::REMINDER_STATUS_READY3, Checkout::REMINDER_STATUS_READY4);
    $arrOverdue = $model->search(true)->getData();

/*
print_r(count($arrOverdue));
die();
*/

    $iCountSuccess = 0;
    $iCountFailure = 0;
    foreach($arrOverdue as $i => $checkout)
    {
        if( ! $checkout->person->hasEmail)
        {
          continue;
        }
        $mq = new Mailqueue;
        $mq->user_id = $checkout->person_id;
        $mq->email = $checkout->person->emails[0]->contact;
        switch($checkout->reminderStatus_id)
        {
            case Checkout::REMINDER_STATUS_READY1:
            	$mq->template_id = Mailqueue::TEMPLATE_REMINDER_1;
            	$checkout->reminderStatus_id = Checkout::REMINDER_STATUS_WAITING2;
            break;
            case Checkout::REMINDER_STATUS_READY2:
            	$mq->template_id = Mailqueue::TEMPLATE_REMINDER_2;
            	$checkout->reminderStatus_id = Checkout::REMINDER_STATUS_WAITING3;
            break;
            case Checkout::REMINDER_STATUS_READY3:
            	$mq->template_id = Mailqueue::TEMPLATE_REMINDER_3;
            	$checkout->reminderStatus_id = Checkout::REMINDER_STATUS_WAITING4;
            break;
            case Checkout::REMINDER_STATUS_READY4:
            	$mq->template_id = Mailqueue::TEMPLATE_REMINDER_4;
            	$checkout->reminderStatus_id = Checkout::REMINDER_STATUS_DONE;
            break;
        }
        $mq->mergevariables = array(
            'aurovillename'   => $checkout->person->aurovillename,
            'name'            => $checkout->person->name,
            'surname'         => $checkout->person->surname,
            'title'           => $checkout->book->summary,
            'author'          => $checkout->book->author[0]->summary,
            'borrowDate'      => $checkout->borrowDate,
            'dueDate'         => $checkout->dueDate,
            'reminderCount'   => $checkout->reminderCount,
            'reminderDate'    => $checkout->reminderDate,
        );
        if($mq->save(false))
        {
            $checkout->reminderDate = date("Y-m-d");
            $checkout->reminderCount++;
            $checkout->save(false);
            $iCountSuccess++;
        }
        else
        {
//             print("failure");
            $iCountFailure++;
        }
    }
    if($iCountFailure > 0)
      Yii::app()->user->setFlash('error', 'Error sending '.$iCountFailure.' message'.($iCountFailure > 1 ? 's' : ''));
    else
      Yii::app()->user->setFlash('success', 'Prepared '.$iCountSuccess.' messages'.($iCountSuccess > 1 ? 's' : ''));
    $this->redirect(array('overdue'));
  }

	public function actionStats()
	{
		$raw = Yii::app()->db->createCommand('SELECT Year(`borrowDate`) AS `year`, count(`id`) AS `checkouts` FROM `checkout` GROUP BY Year(`borrowDate`)')->queryAll();

		$stats = array();
		if(is_array($raw) && count($raw))
			foreach($raw as $row)
			{
				$stat = new checkoutStat();
				$stat->year = $row['year'];
				$stat->checkouts = $row['checkouts'];
				$stats[] = $stat;
			}

		$this->render('stats',array(
			'stats'=>$stats,
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Activity the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Checkout::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadBook($id)
	{
		$model=Book::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
    }


	/**
	 * Performs the AJAX validation.
	 * @param Activity $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Checkout-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

class checkoutStat
{
	var $year;
	var $checkouts;
}