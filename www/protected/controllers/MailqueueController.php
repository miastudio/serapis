<?php

class MailqueueController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(''),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','send','sendOne'),
				'roles'=>array('masteradmin','admin','staff'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
  	$model = $this->loadModel($id);

    $this->render('view',array(
        'model'=>$model
    ));
  }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Transformer']))
		{
			$model->attributes=$_POST['Transformer'];
			if( ! $model->feeder_id)
				$model->feeder_id = null;
			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Transformer successfully saved!");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionErase($id)
	{
		$model = $this->loadModel($id);

		$error = null;
		try
		{
			foreach($model->meters as $tm)
			{
				$tm->transformer_id = null;
				$tm->save(false);
			}
			$model->delete();
		}
		catch(Exception $e) {
		  $error = $e->getMessage();
		}

		if($error && isset($_GET['ajax']))
			echo CJavaScript::jsonEncode(array('error'=>$error));

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			if($error)
				Yii::app()->user->setFlash('error', $error);
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Mailqueue('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Mailqueue']))
			$model->attributes=$_GET['Mailqueue'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionSend($id)
	{
		$model=$this->loadModel($id);
		$model->send(true);
		Yii::app()->user->setFlash(Lookup::item('mailqueue_sendingresult',$model->sendingresult_id), $model->sendingresult);
		$this->redirect(array('admin'));
	}

    public function actionSendOne()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "sendingDate IS NULL";
        $criteria->limit = 1;
        $criteria->order = "creationDate ASC";
        $models = Mailqueue::model()->findAll($criteria);
        if(is_array($models) && count($models) == 1)
        {
            $model = $models[0];
            $model->send();
            Yii::app()->user->setFlash(Lookup::item('mailqueue_sendingresult',$model->sendingresult_id), $model->sendingresult);
        }
        else
        {
            Yii::app()->user->setFlash('error', 'Nothing to send');
        }
		$this->redirect(array('admin'));
    }

	public function actionTest()
	{

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Transformer the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Mailqueue::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Transformer $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Transformer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}