<?php

class ContactdetailController extends MiAController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('ajaxItem','ajaxList'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
// 				'actions'=>array('ajaxList','ajaxEdit', 'ajaxDelete'),
				'actions'=>array('ajaxUpdate','ajaxEdit', 'ajaxDelete', 'ajaxCreate', 'delete'),
				'roles'=>array('masteradmin','admin','staff'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Stewardship the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Contactdetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Stewardship $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Contactdetail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAjaxList($person_id)
	{
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
        throw new CHttpException('422', 'Ajax Access only');
    }
		$models=Contactdetail::model()->findAll(array("condition"=>"person_id = $person_id"));

    echo CJSON::encode(array(
    	'status'=>'success',
			'div'=>$this->renderPartial('_list', array('models'=>$models, 'edit'=>true), true),
    ));
    exit;
	}

	public function actionAjaxEdit()
  {
  	if(isset($_POST['id']))
			$model=Contactdetail::model()->findByPk($_POST['id']);
		elseif(isset($_POST['Contactdetail']['id']) && $_POST['Contactdetail']['id'] != "")
			$model=Contactdetail::model()->findByPk($_POST['Contactdetail']['id']);
		else
			$model = new Contactdetail;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);


		// Form was submitted
    if(isset($_POST['Contactdetail']))
    {
      $model->attributes=$_POST['Contactdetail'];
			$bIsExisting = false;
			if($model->id)
				$bIsExisting = true;
      if($model->save())
      {
        if(Yii::app()->request->isAjaxRequest)
        {
        	if($bIsExisting)
	          echo CJSON::encode(array(
		          'status'=>'success',
			        'div'=>$this->renderPartial('_formInline', array('model'=>$model, 'saved'=>"Successfully updated", 'person_id' => $model->person_id), true),
		          ));
		      else
	          echo CJSON::encode(array(
		          'status'=>'success',
			        'div'=>$this->renderPartial('_formInline', array('model'=>$model, 'saved'=>"Successfully created", 'person_id' => $model->person_id), true),
		          ));
          exit;
        }
        else
        {
        	$this->redirect(array('view','id'=>$model->id));
        }
      }
      else
      {
//print_r($model);
//print_r($model->getErrors());
	      echo CJSON::encode(array(
	        'status'=>'failure',
//	        'div'=>"Error saving",
	        'div'=>$this->renderPartial('_formInline', array('model'=>$model), true),
	        ));
        exit;
      }
    }
    // New form from parameters
		else
		{
	    if (Yii::app()->request->isAjaxRequest)
	    {
	    	if(isset($_POST['person_id']))
	    		$model->person_id = $_POST['person_id'];

	      echo CJSON::encode(array(
	        'status'=>'failure',
	        'div'=>$this->renderPartial('_formInline', array('model'=>$model), true)));
	      exit;
	    }
	  }
  }

	public function actionAjaxDelete($id)
  {
    if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
        throw new CHttpException('422', 'Ajax Access only');
    }

		$model = $this->loadModel($id);
		$person_id = $model->person_id;
		$model->delete();

		$this->actionAjaxList($person_id);
	}

	public function actionAjaxCreate()
	{
		$model = new Contactdetail();

		if(isset($_POST['Contactdetail']))
		{
			$model->attributes=$_POST['Contactdetail'];

			if($model->person_id == 'value')
				$model->person_id = null;

			if($model->save())
			{
				echo "Created successfully";
			}
			else
			{
				$errorMessage = "";
				foreach($model->errors as $field => $errors)
				{
					foreach($errors as $e)
						$errorMessage .= "[".$field."] ".implode("; ",$errors)."\n";
				}
				$this->_sendResponse(400, $errorMessage);
			}
		}
		else
			$this->_sendResponse(400, "Invalid data submitted");
  }

	public function actionAjaxUpdate()
	{
		if($_POST['pk'] && $_POST['name'])
		{
	 		$model=$this->loadModel($_POST['pk']);
	 		$model{$_POST['name']} = $_POST['value'];

	 		if($model->save())
	 		{
				echo "Update successfully";
			}
			else
			{
				$errorMessage = "";
				foreach($model->errors as $field => $errors)
				{
					foreach($errors as $e)
						$errorMessage .= "[".$field."] ".implode("; ",$errors)."\n";
				}
				$this->_sendResponse(400, $errorMessage);
			}
		}
		else
			$this->_sendResponse(400, "Invalid data submitted");
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
}