<?php

class Book_TopicaltermController extends MiAController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','admin',),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('ajaxCreate','ajaxUpdate','delete',),
				'roles'=>array('masteradmin','admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Book_Topicalterm('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Book_Topicalterm']))
			$model->attributes=$_GET['Book_Topicalterm'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Submeterreading the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Book_Topicalterm::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionAjaxCreate()
	{
		$model = new Book_Topicalterm();

		if(isset($_POST['Book_Topicalterm']))
		{
			$model->attributes=$_POST['Book_Topicalterm'];

			if($model->save())
			{
				echo "Created successfully";
			}
			else
			{
				$errorMessage = "";
				foreach($model->errors as $field => $errors)
				{
					foreach($errors as $e)
						$errorMessage .= "[".$field."] ".implode("; ",$errors)."\n";
				}
				$this->_sendResponse(400, $errorMessage);
			}
		}
		else
			$this->_sendResponse(400, "Invalid data submitted");
  }

	public function actionAjaxUpdate()
	{
		if($_POST['pk'] && $_POST['name'])
		{
	 		$model=$this->loadModel($_POST['pk']);
	 		$model{$_POST['name']} = $_POST['value'];

	 		if($model->save())
	 		{
				echo "Update successfully";
			}
			else
			{
				$errorMessage = "";
				foreach($model->errors as $field => $errors)
				{
					foreach($errors as $e)
						$errorMessage .= "[".$field."] ".implode("; ",$errors)."\n";
				}
				$this->_sendResponse(400, $errorMessage);
			}
		}
		else
			$this->_sendResponse(400, "Invalid data submitted");
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

}