<?php
class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actions()
	{
  return array(
   // captcha action renders the CAPTCHA image displayed on the contact page
   'captcha'=>array(
    'class'=>'CCaptchaAction',
    'backColor'=>0xFFFFFF,
   ),
  );
 }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
				'actions'=>array('forgotPassword','captcha','reset','resetPassword','resetApply','login','logout','register','activate','resend'),
				'users'=>array('*'),
			),

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('myProfile','dashboard',),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','view','admin'),
				'roles'=>array('masteradmin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		return $this->actionUpdate($id);
	}

	public function actionDashboard()
	{
		$this->redirect(array('book/admin'));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$res = User::model()->findByAttributes(array('email'=>$model->email));
			if($res)
			{
				Yii::app()->user->setFlash('error', "User ".$model->email." already exists");
			}
			else
			{
// 				$model->attributes=$_POST['User'];
				$model->creationDate = date("Y-m-d");
				if($model->newPassword)
				  $model->encryptPassword();
// 				$model->activationcode = $model->randomCode;
				if($model->save())
				{
					// save before sending password (to get user ID)
// 					$model->resetPassword();
					Yii::app()->user->setFlash('success', "User successfully created!");
// 					$this->redirect(array('update','id'=>$model->id));
					$this->redirect(array('admin'));
				}
			}
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
//			if($model->password != $_POST['User']['previous_password'])

/*
print_r($_POST);
print_r($model);
die();
*/


			if($model->validate())
			{
/*
				if($model->resetPassword)
				{
					$res = $model->resetPassword();
				}
*/
				if($model->newPassword)
				  $model->encryptPassword();
				if($model->save())
				{
					if($res['status'] == "success")
						Yii::app()->user->setFlash('success', "User $model->email saved! ".$res['message']);
					else
						Yii::app()->user->setFlash('success', "User $model->email saved!");
					$this->redirect(array('user/admin'));
				}
				else
					Yii::app()->user->setFlash('error', "Error saving $model->email!");
			}
			else
				Yii::app()->user->setFlash('error', "Could not save - please check your input!");
		}


		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
//	public function actionMyProfile($id)
	public function actionMyProfile()
	{
		$model=$this->loadModel(Yii::app()->user->getId());
		$model->scenario = 'myProfile';

		// Uncomment the following line if AJAX validation is needed
//		$this->performAjaxValidation($model);

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo MiATbActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];

			if($model->newPassword != "")
				$model->encryptPassword();

/*
print_r($model);
die();
*/

/* 			$model->updatedBy_id = Yii::app()->user->id; */

			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Profile successfully saved");
// 				$this->redirect(array('myProfile','id'=>$model->id));
 				$this->redirect(array('myProfile'));
 			}
			else
				Yii::app()->user->setFlash('error', "Could not save - please check your input!");
		}

		$this->render('myProfile',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	// create user
	// create hash
	// send email
	// redirect
	public function actionRegister()
	{
		$model = new User;
		$model->scenario = 'register';

    // if it is ajax validation request
    if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
    {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
    // collect user input data
    if(isset($_POST['User']))
    {
      $model->attributes=$_POST['User'];
			if($model->validate())
			{
				// set 'newPassword' field which will be encrypted
				$model->newPassword = $model->password;
				$model->activationcode = $model->randomCode;
				$model->type_id = User::TYPE_DISABLED;

				if($model->save())
				{
					$res = $model->sendActivationmail(true);
					Yii::app()->user->setFlash($res['status'], $res['message']);
					$this->redirect(array('user/activate', 'email'=>$model->email));
				}
			}
    }

    // display the register form
		$this->layout='sbox';
    $this->render('register',array('model'=>$model));
	}

	public function actionResend($id)
	{
		$model = $this->loadModel($id);
		if( ! $model->activationcode)
		{
			Yii::app()->user->setFlash('warning', 'This account has already been activated. Please log in');
			$this->redirect(array('user/login'));
		}
		elseif( $model->activationmail_count >= User::MAX_ACTIVATIONMAIL_COUNT)
		{
			Yii::app()->user->setFlash('error', 'We have sent you '.User::MAX_ACTIVATIONMAIL_COUNT.' activation emails. Please contact us for further assistance');
			$this->redirect(array('user/activate', 'email'=>$model->email));
		}
		else
		{
			$res = $model->sendActivationmail(true);
			Yii::app()->user->setFlash($res['status'], $res['message']);
			$this->redirect(array('user/activate', 'email'=>$model->email));
		}
	}

	public function actionActivate($email=null,$activationcode=null)
	{
		// URL
		if( ! (isset($_POST['User']) && count($_POST['User'])) && $email)
		{
			$model = User::model()->findByAttributes(array('email'=>$email), array('condition'=>'activationcode is not null'));
		}

		// form (or URL error)
		if( ! $model)
			$model = new User();
		$model->scenario = 'activate';
    // if it is ajax validation request
    if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
    {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
    // collect user input data
    if( ! $model->isNewRecord && (isset($_POST['User']) || $activationcode))
    {
			// Form submitted
	    if(isset($_POST['User']))
	    {
	      $model->attributes=$_POST['User'];
	      $activationcode_temp = $model->activationcode_temp;
				$model = User::model()->findByAttributes(array('email'=>$model->email), array('condition'=>'activationcode is not null'));
				$model->activationcode_temp = $activationcode_temp;
	    }
	    // URL submitted
	    else
	    	$model->activationcode_temp = $activationcode;

			if( ! $model)
			{
				die('no model');
				$model = new User();
				$model->email=$email;
				$model->activationcode_temp=$code;
				$model->addError('email', 'Invalid email');
			}
			elseif($model->validate())
			{
				if($model->activationcode_temp != $model->activationcode)
				{
					$model->addError('activationcode_temp', 'Invalid code');
				}
				else
				{
					$model->activationcode = null;
					$model->type_id = User::TYPE_CLIENT;
					if($model->save())
					{
						$identity=new UserIdentity($model->email,'dummy');
						$identity->loginWithoutPassword();
						Yii::app()->user->login($identity,0);
						$res = $model->sendWelcomemail(true);

						Yii::app()->user->setFlash($res['status'], $res['message']);
						$this->redirect(Yii::app()->getBaseUrl().Yii::app()->homeUrl);
					}
				}
			}
    }

		$this->layout='sbox';
		$this->render('activate', array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// Already logged in
		if(!Yii::app()->user->isGuest)
		{
			$this->redirect(Yii::app()->homeUrl);
		}

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
				if(Yii::app()->user->returnUrl)
					$this->redirect(Yii::app()->user->returnUrl);
				else
					$this->redirect(array('user/dashboard'));
			}
		}
		// display the login form
		$this->layout='sbox';
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->getBaseUrl().Yii::app()->homeUrl);
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionForgotPassword()
	{
		$model = new User();
		$model->scenario = 'forgotPassword';
		$model->unsetAttributes();  // clear any default values

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$existingModel = User::model()->find("email=?",array($model->email));
/*
print_r($model);
print_r($existingModel);
die();
*/
			if($model->validate())
			{
				if($existingModel)
				{
					$existingModel->resetcode = $model->randomCode;
					$existingModel->resetmail_count = '1';
					$existingModel->resetcodeDate = date("Y-m-d");
					$existingModel->save(false);
					$res = $existingModel->sendResetmail(true);
					Yii::app()->user->setFlash($res['status'], $res['message']);
	 				$this->redirect(array('user/reset', 'id'=>$existingModel->id));
	 			}
				else
		 			die('user '.$model->email.' not found');
		 	}
		 	else
		 	{
			 	print_r($model->getErrors());
// 			 	print_r($model->errorSummary());
		 		die('invalid captcha');
		 	}
		}

		$this->layout='sbox';
		$this->render('forgotPassword',array(
			'model'=>$model,
		));
	}

	public function actionReset($id,$resend=false)
	{
		$model = $this->loadModel($id);
		// direct link => nope !
		if( ! $model->resetcode)
		{
			Yii::app()->user->setFlash('error', 'Please click "forgot password" if you need to reset your password');
			$this->redirect(array('user/login'));
		}
		// Too many resends
		elseif( $model->resetmail_count >= User::MAX_RESETMAIL_COUNT)
		{
			Yii::app()->user->setFlash('error', 'We have sent you '.User::MAX_RESETMAIL_COUNT.' activation emails. Please contact us for further assistance');
		}
		// User clicked 'resend'
		elseif($resend)
		{
			$res = $model->sendResetmail(true);
			Yii::app()->user->setFlash($res['status'], $res['message']);
		}

		$this->layout='sbox';
		$this->render('reset', array('model'=>$model));
	}

	public function actionResetpassword($email,$resetcode)
	{
		$model = User::model()->findByAttributes(array('email'=>$email, 'resetcode'=>$resetcode));

		// check age of resetcode
		$resetDate = new DateTime($model->resetcodeDate);
		// expiry date
		$currentDate = new DateTime(date("Y-m-d"));
		$diff = $currentDate->diff($resetDate);
		if($diff->format('%a') > User::MAX_RESETMAIL_LOGIN_INTERVAL_DAYS)
		{
			Yii::app()->user->setFlash('error', 'Your reset code has expired. Please generate a new one.');
			$this->redirect(array('user/forgotPassword'));
		}

		if($model===null)
		{
// 			$this->layout='public';
			$this->render('reset_invalid');
			return;
		}

		$model->resetcode=null;
		$model->resetmail_count = null;
		$model->resetcodeDate = null;
		if($model->save())
		{
// die('will now login');

			$identity=new UserIdentity($model->email,'dummy');
			$identity->loginWithoutPassword();
			Yii::app()->user->login($identity,0);

			Yii::app()->user->setFlash('success', 'You have been logged in. Please set a new password');
			$this->redirect(array('user/myProfile'));
		}
	}
}
