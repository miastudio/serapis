<?php

class LookupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
//	public $layout='//layouts/prem_base';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('ajaxItem','ajaxQuicksearch'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','admin','create','update','phpinfo'),
//				'users'=>array('admin'),
				'roles'=>array('masteradmin','admin'),
			),
			array('allow',
				'actions'=>array('delete'),
//				'users'=>array('admin'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionPhpinfo()
	{
		phpinfo();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView(array $id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Lookup;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lookup']))
		{
			$model->attributes=$_POST['Lookup'];
            if(! $model->code)
            {
                $model->code = Lookup::nextCode($model->section);
            }

			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Lookup successfully created!");
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate(array $id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Lookup']))
		{
			$model->attributes=$_POST['Lookup'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Lookup successfully saved!");
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete(array $id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Lookup('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Lookup']))
			$model->attributes=$_GET['Lookup'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Lookup the loaded model
	 * @throws CHttpException
	 */
	public function loadModel(array $id)
	{
		$model=Lookup::model()->findByAttributes(array('section'=>$id['section'], 'code'=>$id['code']));

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Lookup $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='lookup-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	// data provider for EJuiAutoCompleteFkField for aurovilleName field
	public function actionAjaxItem($section,$term)
	{
		$items=array();
		$models=Lookup::model()->findAll(array(
			'condition'=>'section=:section and description like :description',
			'params'=>array(':section'=>$section, ':description'=>$term."%"),
			'order'=>'description',
		));

		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->description,
					'value' => $m->description,
					'id' => $m->code, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
	}

	// data provider for EJuiAutoCompleteFkField for aurovilleName field
	public function actionAjaxQuicksearch($term)
	{
		$persons=Person::model()->findAll(array(
			'condition'=>'aurovillename like :term OR name like :term OR surname like :term OR masterlistid like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'aurovillename',
		));

		$housingassets=HousingAsset::model()->findAll(array(
			'condition'=>'code like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'code',
		));

		$models = array_merge($persons, $housingassets);


		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->summary,
					'value' => $m->summary,
					'id' => "/".$m->tableName()."/view?id=".$m->id,
					'type' => $m->tableName() == 'person' ? 'user' : 'home'
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
	}
}