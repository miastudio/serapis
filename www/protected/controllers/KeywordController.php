<?php

class KeywordController extends MiAController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','view',),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','ajaxItem'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete',),
				'roles'=>array('admin', 'masteradmin',),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('erase',),
//				'users'=>array('admin'),
				'roles'=>array('masteradmin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Keyword;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Keyword']))
		{
			$model->attributes=$_POST['Keyword'];

			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Keyword successfully created!");
				$this->redirect(array('update','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Keyword']))
		{
			$model->attributes=$_POST['Keyword'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success', "Keyword successfully saved!");
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionView($id)
	{
		$model=$this->loadModel($id);

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Marks a particular model as deleted
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Keyword('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Keyword']))
			$model->attributes=$_GET['Keyword'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages deleted models.
	 */
	public function actionAdminTrash()
	{
		$model=new Keyword('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Keyword']))
			$model->attributes=$_GET['Keyword'];

		$this->render('admin',array(
			'model'=>$model,
			'istrash'=>true,
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Activity the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Keyword::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Activity $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Keyword-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	// data provider for EJuiAutoCompleteFkField for aurovilleName field
	public function actionAjaxItem($term)
	{
		$models=Keyword::model()->findAll(array(
			'condition'=>'name like :term',
			'params'=>array(':term'=>$term."%"),
			'order'=>'name',
		));

		$arrIds = array();
		if(count($models))
			foreach($models as $m)
				$arrIds[] = $m->id;


		if (!empty($models)) {
			$out = array();
			foreach ($models as $m) {
				$out[] = array(
					// expression to give the string for the autoComplete drop-down
					'label' => $m->summary,
					'value' => $m->summary,
					'id' => $m->id, // return value from autocomplete
				);
			}
			echo CJSON::encode($out);
			Yii::app()->end();
		}
	}
}