<?php

/**
 * This is the model class for table "Checkout".
 *
 * The followings are the available columns in table 'checkout':
 * @property string $id
 * @property string $book_id
 * @property string $person_id
 * @property string $borrowDate
 * @property string $dueDate
 * @property string $returnDate
 * @property string $reminderDate
 * @property integer $reminderCount
 */
class Checkout extends MiACActiveRecord
{
	const STATUS_LONG_OVERDUE  = 4;
	const STATUS_OVERDUE       = 3;
	const STATUS_OUT           = 2;
	const STATUS_IN            = 1;

  const REMINDER_STATUS_NOEMAIL     = 1;
  const REMINDER_STATUS_WAITING1    = 2;
  const REMINDER_STATUS_READY1      = 3;
  const REMINDER_STATUS_WAITING2    = 4;
  const REMINDER_STATUS_READY2      = 5;
  const REMINDER_STATUS_WAITING3    = 6;
  const REMINDER_STATUS_READY3      = 7;
  const REMINDER_STATUS_WAITING4    = 8;
  const REMINDER_STATUS_READY4      = 9;
  const REMINDER_STATUS_DONE        = 10;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'checkout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('book_id, person_id, borrowDate, dueDate, returnDate, reminderDate', 'required'),
			array('reminderCount', 'numerical', 'integerOnly'=>true),
			array('status_id, book_id, person_id, reminderStatus_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status_id, book_id, search_book_sn, person_id, borrowDate, dueDate, returnDate, reminderDate, reminderCount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'book' => array(self::BELONGS_TO, 'Book', 'book_id'),
			'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status_id' => 'Status',
			'book_id' => 'Book',
			'search_book_sn' => 'Barcode',
			'person_id' => 'User',
			'borrowDate' => 'Borrowed on',
			'dueDate' => 'Due Date',
			'returnDate' => 'Return Date',
			'reminderDate' => 'Reminder Date',
			'reminderCount' => 'Reminders Sent',
			'reminderStatus_id' => 'Reminder Status',

			'action' => 'Action',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

    public $search_reminderStatus_id_set;
    public $search_book_sn;
	public function search($all=false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.status_id',$this->status_id);
		$criteria->compare('t.book_id',$this->book_id);
		if($this->search_book_sn)
		{
    	    $criteria->with = array('book');
            $criteria->compare('book.sn',$this->search_book_sn);
		}
		$criteria->compare('t.person_id',$this->person_id);
		$criteria->compare('t.borrowDate',$this->borrowDate,true);
		$criteria->compare('t.dueDate',$this->dueDate,true);
		$criteria->compare('t.returnDate',$this->returnDate,true);
		$criteria->compare('t.reminderDate',$this->reminderDate,true);
		$criteria->compare('t.reminderCount',$this->reminderCount);
		if($this->search_reminderStatus_id_set && is_array($this->search_reminderStatus_id_set))
            $criteria->addInCondition('t.reminderStatus_id',$this->search_reminderStatus_id_set);
		else
      		$criteria->compare('t.reminderStatus_id',$this->reminderStatus_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=> array(
				'defaultOrder'=>'t.dueDate ASC, t.status_id DESC',
			),
			'pagination'=>$all ? false : array(
				'pageSize'=> Lookup::Item('user_settings','ui_gridview_size'),
			),
		));
	}

	public function searchList()
	{
        $criteria = new CDbCriteria;
    	$criteria->compare('book_id',$this->book_id);
    	$criteria->compare('person_id',$this->person_id);
    	$criteria->compare('returnDate',$this->returnDate);

        return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
                'pagination'=> $_GET['showall'] == 1 ? false : array(
    				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size_checkout'),
                ),
    		  'sort'=>array(
        		  'attributes' => array('*'),
    				'defaultOrder'=>'dueDate DESC',
    			)
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Checkout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getSummary()
	{
		return $this->title;
	}

	public function showBadges()
	{
		return $this->getStatusBadge();
	}

/*
	public function getStatus_id()
	{
		$reminderCount = intval($this->reminderCount);
		if($reminderCount === null)
			return Checkout::STATUS_IN;
		if($reminderCount === 0)
			return Checkout::STATUS_OUT;
		if($reminderCount === 1 || $reminderCount == 2 )
			return Checkout::STATUS_OVERDUE;
		if($reminderCount >= 3)
			return Checkout::STATUS_LONG_OVERDUE;
	}
*/

	public function getStatusName()
	{
    return Lookup::item('checkout_status', $this->status_id);
	}

	public function getStatusBadge()
	{
		$label = $this->statusName;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('checkout_status_'.$label)));
 		return $this->makeBadge($type,$label);
	}

    public function getReminderStatusName()
    {
        return Lookup::item('reminder_status', $this->reminderStatus_id);
    }

	public function getReminderStatusBadge()
	{
		$label = $this->reminderstatusName;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('reminder_status_'.$label)));
 		return $this->makeBadge($type,$label);
	}

	public function getExtensionCount()
	{
		$borrowDate = new DateTime($this->borrowDate);
		$dueDate = new DateTime($this->dueDate);
		$extension = $dueDate->diff($borrowDate)->format("%a");
		return round(($extension/$this->person->limitDays) - 1);
	}

	public function getCannotExtend()
	{
		if($this->extensionCount > $this->person->limitExtensions)
			return true;
		return false;
	}

	public function getExtensionMessage()
	{
		if($this->cannotExtend)
			return $this->extensionCount."th Extension - limit reached";
		if($this->extensionCount == $this->person->limitExtensions)
			return "Last extension";
		if($this->extensionCount <= 1)
			return "1st Extension";
		if($this->extensionCount == 2)
			return "2nd Extension";
		if($this->extensionCount == 3)
			return "3rd Extension";
		if($this->extensionCount > 3)
			return $this->extensionCount."th Extension";
	}

	public function getExtensionMessageRich()
	{
		if($this->cannotExtend)
			$type = 'important';
		elseif($this->extensionCount == $this->person->limitExtensions)
			$type = 'warning';
		else
			$type = 'success';

		return Yii::app()->controller->widget(
	    'bootstrap.widgets.TbLabel',
	    array(
	        'type' => $type,
	        'label' => $this->extensionMessage,
	    ),
	    true
		);
	}


  public function calculateReminderStatus_id()
  {
	$dueDate = new DateTime($this->dueDate);
    $overdue = (int) $dueDate->diff(new DateTime(date("Y-m-d")))->format("%R%a");

  	if( ! $this->person->hasEmail)
  	{
  	  $this->reminderStatus_id = self::REMINDER_STATUS_NOEMAIL;
  	}
    elseif($this->reminderCount < 1)
    {
      if($overdue > Lookup::item('reminder_interval', '1'))
    	  $this->reminderStatus_id = self::REMINDER_STATUS_READY1;
      else
    	  $this->reminderStatus_id = self::REMINDER_STATUS_WAITING1;
    }
    elseif($this->reminderCount == 1)
    {
      if($overdue > Lookup::item('reminder_interval', '2'))
  	    $this->reminderStatus_id = self::REMINDER_STATUS_READY2;
      else
  	    $this->reminderStatus_id = self::REMINDER_STATUS_WAITING2;
    }
    elseif($this->reminderCount == 2)
    {
      if($overdue > Lookup::item('reminder_interval', '3'))
    	  $this->reminderStatus_id = self::REMINDER_STATUS_READY3;
      else
    	  $this->reminderStatus_id = self::REMINDER_STATUS_WAITING3;
    }
    elseif($this->reminderCount == 3)
    {
      if($overdue > Lookup::item('reminder_interval', '4'))
    	  $this->reminderStatus_id = self::REMINDER_STATUS_READY4;
      else
  	    $this->reminderStatus_id = self::REMINDER_STATUS_WAITING4;
    }
    else
    {
  	  $this->reminderStatus_id = self::REMINDER_STATUS_DONE;
    }
	return $this->reminderStatus_id;
  }

  public static function overduesWithoutEmail()
  {
    $sql = "SELECT COUNT(id) FROM `".self::tableName()."` where `status_id` >".Checkout::STATUS_IN." AND `returnDate` = '0000-00-00' and `reminderStatus_id`='".Checkout::REMINDER_STATUS_NOEMAIL."'";
    return Yii::app()->db->createCommand($sql)->queryScalar();
  }

  public static function overduesReadyToSend()
  {
    $sql = "SELECT COUNT(id) FROM `".self::tableName()."` where `status_id` >".Checkout::STATUS_IN." AND `returnDate` = '0000-00-00' AND `reminderStatus_id` IN (".implode(",",array(Checkout::REMINDER_STATUS_READY1, Checkout::REMINDER_STATUS_READY2, Checkout::REMINDER_STATUS_READY3, Checkout::REMINDER_STATUS_READY4)).")";
    return Yii::app()->db->createCommand($sql)->queryScalar();
  }
}
