<?php

/**
 * This is the model class for table "keyword".
 *
 * The followings are the available columns in table 'keyword':
 * @property string $id
 * @property string $name
 * @property string $notes
 */
class Labelprint extends CModel
{
	const FORMAT_SPINE = 1;
	const FORMAT_BARCODE = 2;
	var $format_id, $batch_status, $date_from, $date_to, $barcode_from, $barcode_to, $barcodes;

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('format_id, batch_status, barcode_from, barcode_to', 'length', 'max'=>11),
			array('date_from, date_to, barcodes', 'safe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'format_id' => 'format',
			'batch_status' => 'Status',
			'date_from' => 'Date From',
			'date_to' => 'Date To',
			'barcode_from' => 'Barcode From',
			'barcode_to' => 'Barcode To',
			'barcodes' => 'Barcodes',
		);
	}

	public function attributeNames()
	{
		return $this->attributeLabels();
	}

}