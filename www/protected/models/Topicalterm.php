<?php

/**
 * This is the model class for table "topicalterm".
 *
 * The followings are the available columns in table 'topicalterm':
 * @property integer $id
 * @property string $name
 * @property string $notes
 */
class Topicalterm extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'topicalterm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>200),
            array('alternateTerm', 'length', 'max'=>255),
			array('notes', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, alternateTerm, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'books' => array(self::MANY_MANY, 'Book', 'book_topicalterm(topicalterm_id,book_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'alternateTerm' => 'Alternate Term',
			'notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
     var $_paginationOff;
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('alternateTerm',$this->alternateTerm,true);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=> $this->_paginationOff ? false : array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Topicalterm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getSummary()
	{
    	if($this->notes)
    	    return $this->name." (".$this->notes.")";
		return $this->name;
	}

  public function mergeOnto($idTo)
  {
    $strQuery = "UPDATE ".Book_Topicalterm::tableName()." SET `topicalterm_id` = '".$idTo."' WHERE `topicalterm_id` = '".$this->id."'";
    $r = Yii::app()->db->createCommand($strQuery)->query();
  }

}
