<?php

/**
 * This is the model class for table "lookup".
 *
 * The followings are the available columns in table 'lookup':
 * @property string $section
 * @property integer $code
 * @property string $description
 */
class Lookup extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lookup';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('section, code, description', 'required'),
			array('code', 'length', 'max'=>64),
			array('section', 'length', 'max'=>32),
			array('description', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('section, code, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section' => 'Section',
			'code' => 'Code',
			'description' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section',$this->section,true);
		$criteria->compare('code',$this->code);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lookup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	private static $_items=array();

	public static function items($section, $order='description')
	{
		if(!isset(self::$_items[$section]))
			self::loadItems($section, $order);
		return self::$_items[$section];
	}

	public static function item($section,$code)
	{
		if(!isset(self::$_items[$section]))
			self::loadItems($section);
		return isset(self::$_items[$section][$code]) ? self::$_items[$section][$code] : false;
	}

	public static function saveItem($section, $code, $value)
	{
		$model=self::model()->find(array(
			'condition'=>'section=:section AND code=:code',
			'params'=>array(':section'=>$section,':code'=>$code),
		));
		$model->description = $value;
		return $model->save();
	}

	private static function loadItems($section,$order='description')
	{
		self::$_items[$section]=array();
  		$models=self::model()->findAll(array(
  			'condition'=>'section=:section',
  			'params'=>array(':section'=>$section),
  			'order'=>$order,
  		));
		foreach($models as $model)
			self::$_items[$section][$model->code]=$model->description;
	}

	public static function itemCode($section, $description)
	{
		$cmd = Yii::app()->db->createCommand();
		$item = $cmd->select("code")
					 ->from('lookup')
					 ->where('section=:section AND description like :description', array(':section'=>$section, ':description' => $description))
					 ->queryRow();

		if(is_array($item) && array_key_exists('code', $item))
		{
			return $item['code'];
		}
		else
		{
			Yii::log("Could not find description ".$description." in with lookup code ".$section, 'warning', 'mia.prem.lookup.itemCode');
			return false;
		}
	}

    public static function nextCode($section)
    {
        $existing = self::items($section, 'code');
        $label = max(array_keys($existing));
        if(is_int($label))
        {
            $label++;
        }
        else
        {
            if(is_numeric(substr($label, -1)))
                $label = preg_replace( "|(\d+)|e", "$1+1", $label);
            else
                $label = $label."_1";
        }
        return $label;
    }

	public static function lookupList($section)
	{
		$sections = Lookup::items($section);
		$cr = array();
		foreach($sections as $i => $v)
			$cr[] = array("id" => $i, "title" => $v);
		return $cr;
	}
}