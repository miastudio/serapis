<?php

/**
 * This is the model class for table "book".
 *
 * The followings are the available columns in table 'book':
 * @property string $id
 * @property string $sn
 * @property string $title
 * @property string $titleOriginal
 * @property string $titleRemainder
 * @property string $titleParallel
 * @property string $partNumber
 * @property string $partTotal
 * @property string $statementOfResponsibility
 * @property string $ddc
 * @property string $number
 * @property string $pubPublisher
 * @property string $pubPlace
 * @property string $pubYear
 * @property string $editionStatement
 * @property string $collation
 * @property string $size
 * @property string $binding_id
 * @property string $ISBN
 * @property string $ISSN
 * @property string $seriesName
 * @property string $seriesPartNumber
 * @property string $seriesPartName
 * @property string $notes
 * @property string $createdBy_id
 * @property string $creationDate
 * @property string $modifiedBy_id
 * @property string $modificationDate
 * @property string $deletedBy_id
 * @property string $deletionDate
 */
class Book extends MiACActiveRecord
{

	const STATUS_IN  = 1;
	const STATUS_OUT = 2;
	const STATUS_REPAIR = 3;
	const STATUS_INCOMING = 4;
	const STATUS_MISSING = 5;
	const STATUS_WITHDRAWN = 6;

	const SN_LENGTH = 6;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'book';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	var $form_duplicate;
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status_id, sn, createdBy_id, modifiedBy_id, deletedBy_id, checkoutCount, form_duplicate', 'length', 'max'=>11),
			array('title, titleRemainder, pubPublisher, pubPlace, editionStatement, seriesPartName', 'length', 'max'=>255),
			array('partNumber, number', 'length', 'max'=>64),
			array('ageGroup', 'length', 'max'=>5),
			array('partTotal, seriesPartNumber', 'length', 'max'=>3),
			array('ddc, pubYear', 'length', 'max'=>32),
			array('collation', 'length', 'max'=>16),
			array('size', 'length', 'max'=>4),
			array('binding_id', 'length', 'max'=>1),
			array('ISBN', 'length', 'max'=>13),
			array('ISSN', 'length', 'max'=>8),
			array('languages,seriesName', 'length', 'max'=>128),
			array('titleOriginal, titleParallel, statementOfResponsibility, notes, creationDate, modificationDate, deletionDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, status_id, sn, title, titleOriginal, titleRemainder, titleParallel, search_author, partNumber, partTotal, statementOfResponsibility, authorNameCache, languages, ddc, number, ageGroup, pubPublisher, pubPlace, pubYear, editionStatement, collation, size, binding_id, ISBN, ISSN, seriesName, seriesPartNumber, seriesPartName, notes, checkoutCount, createdBy_id, creationDate, search_creationDateFrom, search_creationDateTo, search_creationDate, modifiedBy_id, modificationDate, deletedBy_id, deletionDate, search_advanced_pre, search_advanced_pre_paginate, search_advanced_multiple, search_advanced_multiple_fields, search_advanced_custom_fields', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
// 			'languages' => array(self::MANY_MANY, 'Lookup', 'book_language(book_id,language_id)', 'condition' => 'section = "language"'),
			'authors' => array(self::MANY_MANY, 'Author', 'author_book(book_id,author_id)'),
			'checkouts' => array(self::HAS_MANY, 'Checkout', 'book_id'),
			'currentcheckout' => array(self::HAS_ONE, 'Checkout', 'book_id', 'condition'=>'returnDate = "0000-00-00"'),
			'author' => array(self::MANY_MANY, 'Author', 'author_book(book_id,author_id)', 'condition'=>'author_author.role_id=1', 'limit'=>'1'),

            'topicalterms' => array(self::MANY_MANY, 'Topicalterm', 'book_topicalterm(book_id,topicalterm_id)'),
            'features' => array(self::MANY_MANY, 'Feature', 'book_feature(book_id,feature_id)'),
            'keywords' => array(self::MANY_MANY, 'Keyword', 'book_keyword(book_id,keyword_id)'),

            'link_author' => array(self::HAS_MANY, 'Author_Book', 'book_id'),
            'link_keyword' => array(self::HAS_MANY, 'Book_Keyword', 'book_id'),
            'link_topicalterm' => array(self::HAS_MANY, 'Book_Topicalterm', 'book_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'summary' => 'Book',

			'id' => 'ID',
			'status_id' => 'Status',
			'statusName' => 'Status',
			'statusBadge' => 'Status',
			'sn' => 'Barcode',
			'title' => 'Title',
			'titleOriginal' => 'Original Title',
			'titleRemainder' => 'Remainder of Title',
			'titleParallel' => 'Parallel Title',
			'partNumber' => 'Part ...',
			'partTotal' => '... of',
			'statementOfResponsibility' => 'Statement of Responsiblity',
			'authorNameCache' => 'Author',
			'languages' => 'Lang',
			'ddc' => 'Shelf',
			'number' => 'Specifier',
			'ageGroup' => 'Age',
			'pubPublisher' => 'Publisher',
			'pubPlace' => 'Publication Place',
			'pubYear' => 'Publication Year',
			'editionStatement' => 'Edition Statement',
			'collation' => 'Collation',
			'size' => 'Size',
			'binding_id' => 'Binding',
			'ISBN' => 'ISBN',
			'ISSN' => 'ISSN',
			'seriesName' => 'Series Name',
			'seriesPartNumber' => 'Series Part Number',
			'seriesPartName' => 'Series Part Name',
			'notes' => 'Notes',
			'checkoutCount' => 'Checkouts',
			'createdBy_id' => 'Created By',
			'creationDate' => 'Date Added',
			'modifiedBy_id' => 'Modified By',
			'modificationDate' => 'Modification Date',
			'deletedBy_id' => 'Deleted By',
			'deletionDate' => 'Date Removed',

			'borrowDate' => 'Borrowed',
			'returnDate' => 'Returned',
			'dueDate' => 'Due',
			'borrowerName' => 'User',
			'status' => 'Status',
			'dateLastBorrowed' => 'Last Checkout',

			'authorName' => 'Author',
			'author' => 'Author',
			'authorRole' => 'Role',
			'search_author' => 'Author',
			'feature' => 'Features',
			'topicalterm' => 'Topical Terms',
			'keyword' => 'Keywords',

			'search_advanced_pre' => 'Pre-search for',
			'search_advanced_pre_paginate' => 'Skip pagination',
			'search_advanced_multiple' => 'Multi-field search for',
			'search_advanced_multiple_fields' => 'Fields',
			'search_advanced_custom_fields' => 'Custom Fields',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public $search_author;
	public $search_creationDate;
	public function getSearch_creationDateFrom()
	{
		if($this->search_creationDate)
		{
			$dates = explode(" - ", $this->search_creationDate);
			if(is_array($dates) && count($dates) == 2 && $dates[0] != "" && $dates[1] != "")
			{
				return $dates[0];
			}
		}
	}

	public function getSearch_creationDateTo()
	{
		if($this->search_creationDate)
		{
			$dates = explode(" - ", $this->search_creationDate);
			if(is_array($dates) && count($dates) == 2 && $dates[0] != "" && $dates[1] != "")
			{
				return $dates[1];
			}
		}
	}

    private function cleanSerial($serial)
    {
        if(is_array($serial))
        {
            foreach($serial as $i => $v)
                $serial[$i] = $this->cleanSerial($v);
            return $serial;
        }
        else
        {
            return ltrim($serial, '0');
        }
    }

	public $search_sn_from;
	public $search_sn_to;
	public $search_sn_array;
	public $search_advanced_pre;
	public $search_advanced_pre_paginate;
	public $search_advanced_pre_model;
	public $search_advanced_multiple;
	public $search_advanced_multiple_fields;
	public $search_advanced_custom_fields;

	public function search()
	{
		$criteria=new CDbCriteria;

        // Full-text search requires mysql 5.6
        if($this->title)
        {
            $arrTitle = explode(" ", $this->title);
            foreach($arrTitle as $i => $title)
                $arrTitle[$i] = "+".addslashes($title)."*";
            $criteria->addCondition("MATCH (t.title) AGAINST ('".implode(" ",$arrTitle)."' IN BOOLEAN MODE)");
            $criteria->addCondition("MATCH (t.titleRemainder) AGAINST ('".implode(" ",$arrTitle)."' IN BOOLEAN MODE)", 'OR');
/*
            $criteria->addCondition("MATCH (t.title) AGAINST ('".$this->title."' IN BOOLEAN MODE)");
            $criteria->addCondition("MATCH (t.titleRemainder) AGAINST ('".$this->title."' IN BOOLEAN MODE)", 'OR');
*/

/*
            $criteria->addCondition("MATCH (t.titleOriginal) AGAINST ('".implode(" ",$arrTitle)."' IN BOOLEAN MODE)", 'OR');
            $criteria->addCondition("MATCH (t.titleParallel) AGAINST ('".implode(" ",$arrTitle)."' IN BOOLEAN MODE)", 'OR');
*/
        }

		$arrWith = array();
		if($this->authorNameCache)
		{
			$arrWith[] = 'authors';
//             $criteria->addCondition("MATCH (t.authorNameCache) AGAINST ('".$this->authorNameCache."' IN BOOLEAN MODE)");
// 			$criteria->compare('t.authorNameCache',$this->authorNameCache,true);
            $arrAuthor = explode(" ", $this->authorNameCache);
            foreach($arrAuthor as $i => $author)
                $arrAuthor[$i] = "+".addslashes($author)."*";
            $criteria->addCondition("MATCH (t.authorNameCache) AGAINST ('".implode(" ",$arrAuthor)."' IN BOOLEAN MODE)");
		}

		if($this->search_sn_from && $this->search_sn_to)
		{
			$criteria->addCondition("t.sn >= '".$this->cleanSerial($this->search_sn_from)."'");
			$criteria->addCondition("t.sn <= '".$this->cleanSerial($this->search_sn_to)."'");
		}
		elseif($this->search_sn_array && is_array($this->search_sn_array))
		{
			$criteria->addInCondition('t.sn', $this->cleanSerial($this->search_sn_array));
			$criteria->order = "FIELD(t.sn, ".implode(",", $this->search_sn_array).")";
		}
		elseif($this->sn)
		{
            $criteria->addCondition('t.sn LIKE "'.$this->cleanSerial($this->sn).'"');
		}

		if(count($arrWith))
			$criteria->with = $arrWith;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.status_id',$this->status_id);
		$criteria->compare('t.titleOriginal',$this->titleOriginal,true);
		$criteria->compare('t.titleRemainder',$this->titleRemainder,true);
		$criteria->compare('t.titleParallel',$this->titleParallel,true);
		$criteria->compare('t.partNumber',$this->partNumber);
		$criteria->compare('t.partTotal',$this->partTotal);
		$criteria->compare('t.statementOfResponsibility',$this->statementOfResponsibility,true);
		$criteria->compare('t.languages',$this->languages,true);
		$criteria->compare('t.ddc',$this->ddc,true);
		$criteria->compare('t.number',$this->number,true);
		$criteria->compare('t.ageGroup',$this->ageGroup,true);
		$criteria->compare('t.pubPublisher',$this->pubPublisher,true);
		$criteria->compare('t.pubPlace',$this->pubPlace,true);
		$criteria->compare('t.pubYear',$this->pubYear,true);
		$criteria->compare('t.editionStatement',$this->editionStatement,true);
		$criteria->compare('t.collation',$this->collation,true);
		$criteria->compare('t.size',$this->size,true);
		$criteria->compare('t.binding_id',$this->binding_id,true);
		$criteria->compare('t.ISBN',$this->ISBN,true);
		$criteria->compare('t.ISSN',$this->ISSN,true);
		$criteria->compare('t.seriesName',$this->seriesName,true);
		$criteria->compare('t.seriesPartNumber',$this->seriesPartNumber,true);
		$criteria->compare('t.seriesPartName',$this->seriesPartName,true);
		$criteria->compare('t.notes',$this->notes,true);
// Not sure why, but $this->checkoutCount always becomes 0... ??
if($this->checkoutCount)
{
		$criteria->compare('t.checkoutCount',$this->checkoutCount);
}
		$criteria->compare('t.createdBy_id',$this->createdBy_id,true);

		if($this->search_creationDate)
		{
			if($this->search_creationDateFrom)
				$criteria->addCondition("t.creationDate >= '".$this->search_creationDateFrom."'");
			if($this->search_creationDateTo)
				$criteria->addCondition("t.creationDate <= '".$this->search_creationDateTo."'");
/*
print("condition: <br/>");
print("t.creationDate >= '".$this->search_creationDateFrom."'<br/>");
print("t.creationDate <= '".$this->search_creationDateTo."'<br/>");
die('search creation date: '.$this->search_creationDateFrom."-".$this->search_creationDateTo." / vs ".$this->search_creationDate);
*/
		}
		else
			$criteria->compare('t.creationDate',$this->creationDate,true);
		$criteria->compare('t.modifiedBy_id',$this->modifiedBy_id);
		$criteria->compare('t.modificationDate',$this->modificationDate,true);
		$criteria->compare('t.deletedBy_id',$this->deletedBy_id);
		if($this->deletionDate == "null")
			$criteria->addCondition(array('condition' => 't.deletionDate IS NULL'));
		elseif($this->deletionDate == "not null")
			$criteria->addCondition(array('condition' => 't.deletionDate IS NOT NULL'));
		else
			$criteria->compare('t.deletionDate',$this->deletionDate,true);

    // Advanced search (1)
    if($this->search_advanced_pre && $this->search_advanced_pre_model)
    {
      $linktable = "link_".strtolower(get_class($this->search_advanced_pre_model));
      $idfield = strtolower(get_class($this->search_advanced_pre_model))."_id";
      $criteria2 = new CDbCriteria;
      $criteria2->with = $linktable;
      $criteria2->compare($linktable.'.'.$idfield, $this->search_advanced_pre_model->id,true);
      $criteria->mergeWith($criteria2, 'AND');
    }

    // Advanced search (2)
  	if($this->search_advanced_multiple)
  	{
      $criteria3 = new CDbCriteria;
    	$available = $this->advancedSearchMultipleAttributes;
      $selected = $this->advancedSearchMultipleSelectedAttributes;

      foreach($available as $id => $attribute)
      {
        if(in_array($attribute, $selected))
        {
          $criteria3->compare('t.'.$attribute, $this->search_advanced_multiple, true, 'OR');
        }
      }
      $criteria->mergeWith($criteria3, 'AND');
  	}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => $sort,
			'pagination'=> $_GET['showall'] == 1 ? false : array(
				'pageSize'=>Lookup::Item('user_settings','ui_gridview_size'),
			),
		));
	}

    public function searchBarcodeRange()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.deletionDate IS NULL');
        $criteria->addBetweenCondition("t.sn", $this->cleanSerial($this->search_sn_from), $this->cleanSerial($this->search_sn_to), 'AND');
        $books = self::model()->findAll($criteria);
        $arrBarcodes = array();
        foreach($books as $book)
            $arrBarcodes[] = $book->sn;
        return $arrBarcodes;
    }

    public function searchDateRange()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.deletionDate IS NULL');
        $criteria->addBetweenCondition("t.creationDate", $this->search_creationDateFrom, $this->search_creationDateTo, 'AND');
        $books = self::model()->findAll($criteria);
        $arrBarcodes = array();
        foreach($books as $book)
            $arrBarcodes[] = $book->sn;
        return $arrBarcodes;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Book the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		$this->checkoutCount = count($this->checkouts);
        $this->updateAuthorNameCache();
		return parent::beforeSave();
	}

    public function updateAuthorNameCache()
    {
        $this->authorNameCache = implode(",",$this->authorNames);
    }

    public static function resetAuthorNameCache()
    {
        $strQuery = "
            UPDATE book
            SET authorNameCache =
            (SELECT author.name FROM
            author_book
            LEFT JOIN author ON author.id = author_book.author_id AND author_book.role_id = '1'
            WHERE author_book.book_id = book.id
            LIMIT 1);";
        $r = Yii::app()->db->createCommand($strQuery)->query();
        return $r->rowCount;
    }

	public function getSummary()
	{
		return "#".$this->barcode." - ".$this->title;
	}

    public function getExtraTitlesRich()
    {
        $strTitle = "";
        if($this->partNumber)
            $strTitle .= "<span class='title_part'>".$this->partNumber."</span>";
        if($this->titleRemainder)
            $strTitle .= "<div class='title_remainder'>".$this->titleRemainder."</div>";
/*
        if($this->titleRemainder && $this->titleParallel)
            $strTitle .= "<span class='title_separator'> </span>";
*/
        if($this->titleParallel)
            $strTitle .= "<div class='title_parallel'>".$this->titleParallel."</div>";
        if($this->titleOriginal)
            $strTitle .= "<div class='title_original'>".$this->titleOriginal."</div>";
        return $strTitle;
    }

	public function showBadges()
	{
		return $this->getBindingBadge();
	}

	public function getBindingName()
	{
    return Lookup::item('physicaldetail', $this->binding_id);
	}

	public function getBindingBadge()
	{
		$label = $this->bindingName;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('book_binding_'.$label)));
 		return $this->makeBadge($type,$label);
	}

	public function getDueDate()
	{
		if($this->currentcheckout)
			return $this->currentcheckout->dueDate;
		else
			return null;
	}

	public function getBorrowerName()
	{
		if($this->currentcheckout)
			return $this->currentcheckout->person->summary;
		else
			return null;
	}

    public function getBorrowerNameLink()
    {
		if($this->currentcheckout)
            return CHtml::link( $this->currentcheckout->person->summary, array("person/view","id"=> $this->currentcheckout->person_id));
		else
			return null;

    }

	public function getStatusBadge()
	{
		$label = $this->statusName;
		$type = Lookup::item('badge', str_replace(' ', '_', strtolower('book_status_'.$label)));
 		return $this->makeBadge($type,$label);
	}

/*
	public function getStatus_id()
	{
		if($this->currentcheckout)
			return $this->currentcheckout->status_id;
	}
*/

	public function getStatusName()
	{
    return Lookup::item('book_status', $this->status_id);
	}

	public function listLanguages()
	{
		$arrRet = array();
		foreach(Lookup::lookupList('language') as $language)
		{
			$arrRet[] = $language['title'];
		}
		return $arrRet;
	}

    public function getFirstAuthorOnlyNameLink()
    {
//         foreach($this->author as $id => $author)
		if(count($this->author))
		{
    		if(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")
    		    return CHtml::link($this->author[0]->summary, array("author/update","id"=>$this->author[0]->id));
            else
    		    return CHtml::link($this->author[0]->summary, array("author/view","id"=>$this->author[0]->id));
		}
        else
        {
            if(count($this->authors))
            {
    		if(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")
    		    return CHtml::link($this->authors[0]->summary, array("author/update","id"=>$this->authors[0]->id));
            else
    		    return CHtml::link($this->authors[0]->summary, array("author/view","id"=>$this->authors[0]->id));
            }
        }
    }

	public function getAuthorNames()
	{
		$arrAuthors = array();
		if(is_array($this->authors))
		{
			foreach($this->authors as $author)
			{
				$arrAuthors[$author->id] = $author->summary;
			}
		}
		return $arrAuthors;
	}

	public function getAuthorNamesLinks()
	{
    	$this->refresh();
		$arrRet = array();
		foreach($this->getAuthorNames() as $id => $author)
		{
    		if(Yii::app()->user->roles == "masteradmin" || Yii::app()->user->roles == "admin")
                $arrRet[] = CHtml::link($author, array("author/update","id"=>$id));
            else
    			$arrRet[] = CHtml::link($author, array("author/view","id"=>$id));
		}
		return implode("<br/>", $arrRet);
	}

    public function authorRole($author_id=null)
    {
        $a = array();
        foreach($this->link_author as $la)
        {
            if($la->author_id == $author_id)
                return $la->roleName;
        }
    }

    public function getBarcode()
    {
        return str_pad($this->sn, self::SN_LENGTH, "0", STR_PAD_LEFT);
    }

	public function getDateLastBorrowed()
	{
		if($this->checkouts)
			return $this->checkouts[count($this->checkouts)-1]->borrowDate;
	}

  // Make sure the code for advanced_search_multiple is gapless and starts at 1
  // crucial for getAdvancedSearchMultipleSelectedAttributes as checkboxes do not contain the index
  public function getAdvancedSearchMultipleAttributes()
  {
    return Lookup::items('advanced_search_multiple','cast(`t`.`code` as unsigned)');
  }

  private $_selectedMultiple = null;
  public function getAdvancedSearchMultipleSelectedAttributes()
  {
    if(is_array($this->_selectedMultiple))
      return $this->_selectedMultiple;
    $this->_selectedMultiple = array();
    if($this->search_advanced_multiple_fields && is_array($this->search_advanced_multiple_fields) && count($this->search_advanced_multiple_fields))
    {
      $available = $this->advancedSearchMultipleAttributes;
      foreach($this->search_advanced_multiple_fields as $selectedId)
      {
        $this->_selectedMultiple[] = $available[$selectedId];
      }
    }
    return $this->_selectedMultiple;
  }

  public function getAdvancedSearchCustomAttributes()
  {
    return Lookup::items('advanced_search_custom','cast(`t`.`code` as unsigned)');
  }

  private $_selectedCustom = null;
  public function getAdvancedSearchCustomSelectedAttributes()
  {
    if(is_array($this->_selectedCustom))
      return $this->_selectedCustom;
    $this->_selectedCustom = array();
    if($this->search_advanced_custom_fields && is_array($this->search_advanced_custom_fields) && count($this->search_advanced_custom_fields))
    {
      $available = $this->advancedSearchCustomAttributes;
      foreach($this->search_advanced_custom_fields as $attr)
      {
        if(in_array($attr, $available))
          $this->_selectedCustom[] = $attr;
      }
    }
    return $this->_selectedCustom;
  }

  public function getDefaultSearchAttributes()
  {
    return array(
      'sn',
      'title',
      'search_author',
      'languages',
      'ddc',
      'number',
      'ageGroup',
      'status_id',
    );
  }

  public function setRepair()
  {
      $this->status_id = self::STATUS_REPAIR;
      return $this->save(false);
  }

  public function setMissing()
  {
      $this->status_id = self::STATUS_MISSING;
//       $this->deletionDate = date("Y-m-d");
      return $this->save(false);
  }

	public function findNextSN()
	{
        $record=$this->model()->find(array(
            'select' => 'max(sn) AS sn',
        ));
        if($record!==null)
          return $record->sn + 1;
        return 1;
	}

}
