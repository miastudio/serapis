<?php
$this->breadcrumbs=array(
	'Backups'=>array('index'),
	'Upload',
);?>
<h1><?php echo ucfirst($this->action->id); ?></h1>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'install-form',
	'enableAjaxValidation' => true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'type'=>'horizontal'
));
?>
<fieldset>

<?php echo $form->fileFieldRow($model, 'fileField', array('class'=>'HouManUpload')); ?>

</fieldset>


    <div class="form-actions">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => Yii::t('app', 'Upload')
            )
        ); ?>
				<?php
				$url = Yii::app()->createAbsoluteUrl('backup/default');
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
            	'buttonType' => 'reset', 
            	'label' => 'Cancel',
            	'htmlOptions' => array(
								'onclick' => 'bootbox.confirm("Discard changes ?",
																function(confirmed){
									                if(confirmed) {
									                   window.location = "'.$url.'";
									                }
																})',
							),
            )
        ); ?>
    </div>

<?php
	$this->endWidget();
?>