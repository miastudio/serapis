<?php //$this->widget('zii.widgets.grid.CGridView', array(
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'install-grid',
	'dataProvider' => $dataProvider,
	'type' => 'striped bordered condensed',
	'columns' => array(
		array(
			'name' => 'name',
			'header' => 'Name',
		),
		array(
			'name' => 'size',
			'header' => 'Size',
		),
		array(
			'name' => 'create_time',
			'header' => 'Date Created',
		),
		array(
			'class' => 'CButtonColumn',
			'template' => '{download}',
			'header'=>"Download",
			  'buttons'=>array
			    (
			        'download' => array
			        (
			            'url'=>'Yii::app()->createUrl("backup/default/download", array("file"=>$data["name"]))',
			            'label'=>'Download this backup',
                        'imageUrl'=>Yii::app()->request->baseUrl.'/images/database_download.png',
			        ),
			        'restore' => array
			        (
			            'url'=>'Yii::app()->createUrl("backup/default/restore", array("file"=>$data["name"]))',
			            'label'=>'Restore this backup',
                        'imageUrl'=>Yii::app()->request->baseUrl.'/images/database_restore.png',
					),
			        'delete' => array
			        (
			            'url'=>'Yii::app()->createUrl("backup/default/delete", array("file"=>$data["name"]))',
			            'label'=>'Delete this 1st backup',
                        'imageUrl'=>Yii::app()->request->baseUrl.'/images/database_delete.png',
			        ),
			    ),		
		),
		array(
			'class' => 'CButtonColumn',
			'header' => 'Restore',
			'template' => '{restore}',
			  'buttons'=>array
			    (
			        'restore' => array
			        (
			            'url'=>'Yii::app()->createUrl("backup/default/restore", array("file"=>$data["name"]))',
			            'label'=>'Restore this backup',
                        'imageUrl'=>Yii::app()->request->baseUrl.'/images/database_restore.png',
                  'options' => array(
                    'confirm' => 'Do you want to replace the database with this backup ?',
                  ),
					    ),
			    ),		
		),
		array(
			'class' => 'CButtonColumn',
			'header' => 'Delete',
			'template' => '{delete}',
			  'buttons'=>array
			    (

			        'delete' => array
			        (
			            'url'=>'Yii::app()->createUrl("backup/default/delete", array("file"=>$data["name"]))',
			            'label'=>'Delete this 2nd backup',
			            'imageUrl'=>Yii::app()->request->baseUrl.'/images/database_delete.png',
			        ),
			    ),		
		),
	),
)); ?>