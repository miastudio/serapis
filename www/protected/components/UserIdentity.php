<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
  private $id;
  public function authenticate()
  {
//     $record=User::model()->findByAttributes(array('email'=>$this->username));
//     $record=User::model()->findByAttributes(array('email'=>strtolower($this->username), 'name'=>strtolower($this->username), 'condition'=>'or'));
    $record=User::model()->findByAttributes(array(), array(
	  'condition' => 'LOWER(email) =:username OR LOWER(name) =:username',
	  'params' => array(
	    ':username' => strtolower($this->username),
	  ),
	));

    if($record===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if( ! password_verify($this->password, $record->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
    else
    {
      $this->id=$record->id;
      $this->setState('roles', Lookup::item('user_type', $record->type_id));
      $this->setState('background_id', $record->background_id);
      $this->errorCode=self::ERROR_NONE;
    }
    return !$this->errorCode;
  }

  public function getId()
  {
    return $this->id;
  }


	public function loginWithoutPassword()
	{
    $record=User::model()->findByAttributes(array('email'=>$this->username));
    if($record===null)
       $this->errorCode=self::ERROR_USERNAME_INVALID;
    else
    {
      $this->id=$record->id;
      $this->setState('roles', Lookup::item('user_type', $record->type_id));
      $this->errorCode=self::ERROR_NONE;
    }
    return !$this->errorCode;
	}
}
/*
// Original function:
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
		$users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;
	}

	public function getIsAdmin()
	{
		return 1;
	}
}
*/