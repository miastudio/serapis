<?php
/* CheckAccess function
 * Allows to asign roles to groups
 */
class MiAWebUser extends CWebUser
{
//		// Don't use as it kills setState from UserIdentiy
// 		var $roles;
    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }
        $role = $this->getState("roles");
        if ($role === 'admin') {
            return true; // admin role has access to everything
        }
        // allow access if the operation request is the current user's role
        return ($operation === $role);
    }

		public function getRoles()
		{
			if($this->getState('roles'))
				return $this->getState('roles');
			else
				return null;
		}

    public function getBackground()
    {
		if($this->getState('background_id'))
			return $this->getState('background_id');
		else
			return null;
    }
}

?>