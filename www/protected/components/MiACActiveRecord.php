<?php
abstract class MiACActiveRecord extends CActiveRecord{

//ALTER TABLE `` ADD `createdBy_id` INT(11) UNSIGNED NULL DEFAULT NULL, ADD `creationDate` DATE NULL DEFAULT NULL , ADD `modifiedBy_id` INT(11) UNSIGNED NULL DEFAULT NULL , ADD `modificationDate` DATE NULL DEFAULT NULL,  ADD `deletedBy_id` INT(11) UNSIGNED NULL DEFAULT NULL, ADD `deletionDate` DATE NULL DEFAULT NULL;

	public function makeBadge($type, $label)
	{
		return Yii::app()->controller->widget(
	    'bootstrap.widgets.TbBadge',
	    array(
	      'type' => $type,
	      'label' => $label,
	    ),
	    true
		);
	}

	public function getCreatedBy()
	{
		return $this->createdBy_user->summary;
	}

	public function getModifiedBy()
	{
		return $this->modifiedBy_user->summary;
	}

	public function getDeletedBy()
	{
		return $this->deletedBy_user->summary;
	}

	public function markDeleted()
	{
		$this->deletionDate = date("Y-m-d H:i:s");
		if (php_sapi_name() == "cli")
			$this->deletedBy_id = 0;
		else
			$this->deletedBy_id = Yii::app()->user->id;
		$this->save(false);
	}

	public function markUndeleted()
	{
		$this->deletionDate = null;
		if (php_sapi_name() == "cli")
			$this->deletedBy_id = 0;
		else
			$this->deletedBy_id = Yii::app()->user->id;
		$this->save(false);
	}

	protected function beforeSave()
	{

		if($this->creationDate == "0000-00-00" || $this->creationDate == "0" || $this->creationDate == "")
			$this->creationDate = NULL;
		if($this->modificationDate == "0000-00-00" || $this->modificationDate == "0" || $this->modificationDate == "")
			$this->modificationDate = NULL;
		if($this->deletionDate == "0000-00-00" || $this->deletionDate == "0" || $this->deletionDate == "")
			$this->deletionDate = NULL;

		if($this->isNewRecord)
		{
			$this->creationDate = date("Y-m-d H:i:s");
			if (php_sapi_name() == "cli")
				$this->createdBy_id = 0;
			else
				$this->createdBy_id = Yii::app()->user->id;
		}
		else
		{
			$this->modificationDate = date("Y-m-d H:i:s");
			if (php_sapi_name() == "cli")
				$this->modifiedBy_id = 0;
			else
				$this->modifiedBy_id = Yii::app()->user->id;
		}
		return parent::beforeSave();
	}

	public function getNextId()
	{
    $record=$this->model()->find(array(
            'condition' => 'id>:current_id',
            'order' => 'id ASC',
            'limit' => 1,
            'params'=>array(':current_id'=>$this->id),
    ));
    if($record!==null)
      return $record->id;
    return null;
	}

	public function getPreviousId()
	{
	  $record=$this->model()->find(array(
	          'condition' => 'id<:current_id',
	          'order' => 'id DESC',
	          'limit' => 1,
	          'params'=>array(':current_id'=>$this->id),
	  ));
	  if($record!==null)
	      return $record->id;
	  return null;
	}

	abstract public function getSummary();

	abstract public function showBadges();

	public function displayContact($contact)
	{
		// email:
		if(strpos($contact, "@"))
		{
			return "<a href=\"javascript:sendMail('".strrev($contact)."')\">".str_replace(array("@","."), array("@<span class=\"privatise\">REMOVE</span>","<span class=\"privatise\">REMOVE</span>."), $contact)."</a>";
		}
		else
			return substr($contact, 0, strlen($contact)/2)."<span class=\"privatise\">-000-</span>".substr($contact, strlen($contact)/2);
	}
}