-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 08, 2017 at 02:42 PM
-- Server version: 5.5.57-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `serapis`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE IF NOT EXISTS `author` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nameFullerForm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lifetime` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` date DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `author_book`
--

CREATE TABLE IF NOT EXISTS `author_book` (
`id` int(11) unsigned NOT NULL COMMENT 'ID',
  `author_id` int(11) unsigned NOT NULL COMMENT 'Author',
  `book_id` int(11) unsigned NOT NULL COMMENT 'Book',
  `role_id` int(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Author''s role',
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` date DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
`id` int(11) unsigned NOT NULL COMMENT 'ID',
  `status_id` int(10) unsigned DEFAULT NULL,
  `sn` int(11) unsigned DEFAULT NULL COMMENT 'Barcode',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Title',
  `titleOriginal` text COLLATE utf8mb4_unicode_ci COMMENT 'Original Title',
  `titleRemainder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Remainder of Title',
  `titleParallel` text COLLATE utf8mb4_unicode_ci COMMENT 'Parallel Title',
  `partNumber` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Part ...',
  `partTotal` int(3) unsigned DEFAULT NULL COMMENT '... of',
  `statementOfResponsibility` text COLLATE utf8mb4_unicode_ci COMMENT 'Statement of Responsiblity',
  `languages` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ddc` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'DDC',
  `number` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Number',
  `ageGroup` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Age Group',
  `pubPublisher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Publisher',
  `pubPlace` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Publication Place',
  `pubYear` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Publication Year',
  `editionStatement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Edition Statement',
  `collation` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Collation',
  `size` int(4) unsigned DEFAULT NULL COMMENT 'Size',
  `binding_id` int(1) unsigned DEFAULT NULL COMMENT 'Binding',
  `ISBN` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ISBN',
  `ISSN` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ISSN',
  `seriesName` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Series Name',
  `seriesPartNumber` int(3) unsigned DEFAULT NULL COMMENT 'Series Part Number',
  `seriesPartName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Series Part Name',
  `notes` text COLLATE utf8mb4_unicode_ci COMMENT 'Notes',
  `checkoutCount` int(10) unsigned NOT NULL DEFAULT '0',
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` date DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_feature`
--

CREATE TABLE IF NOT EXISTS `book_feature` (
`id` int(11) unsigned NOT NULL,
  `book_id` int(11) unsigned NOT NULL,
  `feature_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_keyword`
--

CREATE TABLE IF NOT EXISTS `book_keyword` (
`id` int(11) unsigned NOT NULL,
  `book_id` int(11) unsigned NOT NULL,
  `keyword_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_topicalterm`
--

CREATE TABLE IF NOT EXISTS `book_topicalterm` (
`id` int(11) unsigned NOT NULL,
  `book_id` int(11) unsigned NOT NULL,
  `topicalterm_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE IF NOT EXISTS `checkout` (
`id` int(11) unsigned NOT NULL,
  `status_id` int(10) unsigned DEFAULT NULL,
  `book_id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `borrowDate` date NOT NULL,
  `dueDate` date NOT NULL,
  `returnDate` date NOT NULL,
  `reminderDate` date NOT NULL,
  `reminderCount` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `reminderStatus_id` int(10) unsigned DEFAULT NULL,
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` date DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contactdetail`
--

CREATE TABLE IF NOT EXISTS `contactdetail` (
`id` int(11) NOT NULL,
  `person_id` int(11) unsigned DEFAULT NULL,
  `contactType_id` int(11) unsigned DEFAULT NULL,
  `bAsynctoDelete` tinyint(1) DEFAULT NULL,
  `contact` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE IF NOT EXISTS `feature` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE IF NOT EXISTS `help` (
`id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` date DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `content`, `createdBy_id`, `creationDate`, `modifiedBy_id`, `modificationDate`, `deletedBy_id`, `deletionDate`) VALUES
(1, 'Go to help > update to modify this', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Go to help > update to modify this', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'No help created. Ask your admin to do so', NULL, NULL, NULL, NULL, NULL, NULL),
(4, '<p>No help created yet</p>\r\n', NULL, NULL, 1, '2016-12-05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keyword`
--

CREATE TABLE IF NOT EXISTS `keyword` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lookup`
--

CREATE TABLE IF NOT EXISTS `lookup` (
  `section` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookup`
--

INSERT INTO `lookup` (`section`, `code`, `description`) VALUES
('advanced_search_custom', '1', 'id'),
('advanced_search_custom', '10', 'partTotal'),
('advanced_search_custom', '11', 'statementOfResponsiblity'),
('advanced_search_custom', '12', 'ddc'),
('advanced_search_custom', '13', 'number'),
('advanced_search_custom', '14', 'ageGroup'),
('advanced_search_custom', '15', 'pubPublisher'),
('advanced_search_custom', '16', 'pubPlace'),
('advanced_search_custom', '17', 'pubYear'),
('advanced_search_custom', '18', 'editionStatement'),
('advanced_search_custom', '19', 'collation'),
('advanced_search_custom', '2', 'status_id'),
('advanced_search_custom', '20', 'size'),
('advanced_search_custom', '21', 'ISBN'),
('advanced_search_custom', '22', 'ISSN'),
('advanced_search_custom', '23', 'seriesName'),
('advanced_search_custom', '24', 'seriesPartNumber'),
('advanced_search_custom', '25', 'seriesPartName'),
('advanced_search_custom', '26', 'notes'),
('advanced_search_custom', '27', 'checkoutCount'),
('advanced_search_custom', '28', 'creationDate'),
('advanced_search_custom', '29', 'deletionDate'),
('advanced_search_custom', '3', 'sn'),
('advanced_search_custom', '4', 'title'),
('advanced_search_custom', '5', 'titleOriginal'),
('advanced_search_custom', '6', 'titleRemainder'),
('advanced_search_custom', '7', 'titleParallel'),
('advanced_search_custom', '8', 'search_author'),
('advanced_search_custom', '9', 'partNumber'),
('advanced_search_multiple', '1', 'title'),
('advanced_search_multiple', '10', 'notes'),
('advanced_search_multiple', '2', 'titleOriginal'),
('advanced_search_multiple', '3', 'titleRemainder'),
('advanced_search_multiple', '4', 'titleParallel'),
('advanced_search_multiple', '5', 'statementOfResponsibility'),
('advanced_search_multiple', '6', 'pubPublisher'),
('advanced_search_multiple', '7', 'pubPlace'),
('advanced_search_multiple', '8', 'editionStatement'),
('advanced_search_multiple', '9', 'seriesName'),
('author_role', '1', 'author'),
('author_role', '2', 'compiler'),
('author_role', '3', 'contributor'),
('author_role', '4', 'editor'),
('author_role', '5', 'illustrator'),
('author_role', '6', 'photographer'),
('author_role', '7', 'translator'),
('badge', 'author_role_author', 'success'),
('badge', 'author_role_compiler', 'info'),
('badge', 'author_role_contributor', 'warning'),
('badge', 'author_role_editor', 'inverse'),
('badge', 'author_role_illustrator', 'important'),
('badge', 'author_role_photographer', 'default'),
('badge', 'author_role_translator', 'warning'),
('badge', 'blabla', 'BliBli'),
('badge', 'book_binding_clamped', 'important'),
('badge', 'book_binding_hardcover', 'success'),
('badge', 'book_binding_paperback', 'info'),
('badge', 'book_binding_softcover', 'inverse'),
('badge', 'book_status_1-in', 'success'),
('badge', 'book_status_2-out', 'important'),
('badge', 'book_status_3-repair', 'warning'),
('badge', 'book_status_4-incoming', 'info'),
('badge', 'book_status_5-missing', 'inverse'),
('badge', 'checkout_status_in', 'success'),
('badge', 'checkout_status_long_overdue', 'important'),
('badge', 'checkout_status_out', 'inverse'),
('badge', 'checkout_status_overdue', 'warning'),
('badge', 'overdue_administratively_disabled', 'warning'),
('badge', 'overdue_long_overdue', 'important'),
('badge', 'overdue_no_overdue', 'success'),
('badge', 'person_status_aurovilian', 'success'),
('badge', 'person_status_aurovilian_child', 'important'),
('badge', 'person_status_friend_of_auroville', 'warning'),
('badge', 'person_status_guest', 'warning'),
('badge', 'person_status_new_comer', 'info'),
('badge', 'person_status_new_comer_child', 'warning'),
('badge', 'person_status_to_be_new_comer', 'warning'),
('badge', 'person_status_to_be_new_comer_child', 'warning'),
('badge', 'person_status_volunteer', 'important'),
('badge', 'person_status_worker', 'info'),
('badge', 'person_status_youth', 'inverse'),
('badge', 'user_type_admin', 'warning'),
('badge', 'user_type_client', 'success'),
('badge', 'user_type_disabled', 'default'),
('badge', 'user_type_masteradmin', 'important'),
('badge', 'user_type_staff', 'info'),
('book_status', '1', '1-In'),
('book_status', '2', '2-Out'),
('book_status', '3', '3-Repair'),
('book_status', '4', '4-Incoming'),
('book_status', '5', '5-Missing'),
('checkout_status', '1', 'In'),
('checkout_status', '2', 'Out'),
('checkout_status', '3', 'Overdue'),
('checkout_status', '4', 'Long Overdue'),
('community', '1', 'Abri'),
('community', '10', 'Ami'),
('community', '100', 'Mango Garden'),
('community', '101', 'Matrimandir'),
('community', '102', 'Meadow'),
('community', '103', 'Minati'),
('community', '104', 'Mir'),
('community', '105', 'Miracle'),
('community', '106', 'Mitra Guest House'),
('community', '107', 'Mm Nursery'),
('community', '108', 'Mudaliar Chavadi'),
('community', '109', 'Nandanam'),
('community', '11', 'Angiras Garden'),
('community', '110', 'New Creation'),
('community', '111', 'New Creation Field'),
('community', '112', 'New Lands'),
('community', '113', 'New Service Farm'),
('community', '114', 'Nilatangam'),
('community', '115', 'Nine Palms'),
('community', '116', 'Pazhamudircholai Farm'),
('community', '117', 'Petite Ferme'),
('community', '118', 'Pitchandikulam'),
('community', '119', 'Pony Farm'),
('community', '12', 'Annapurna'),
('community', '120', 'Prarthna'),
('community', '121', 'Prayatna'),
('community', '122', 'Prayogashala'),
('community', '123', 'Progress'),
('community', '124', 'Promesse'),
('community', '125', 'Protection'),
('community', '126', 'Pump House'),
('community', '127', 'Quiet'),
('community', '128', 'Ravena'),
('community', '129', 'Realization'),
('community', '13', 'Antakarana'),
('community', '130', 'Red Earth'),
('community', '131', 'Repos'),
('community', '132', 'Reve'),
('community', '133', 'Revelation'),
('community', '134', 'Ritam'),
('community', '135', 'Saaram'),
('community', '136', 'Sadhana Forest'),
('community', '137', 'Sailam'),
('community', '138', 'Samarpan'),
('community', '139', 'Samasti'),
('community', '14', 'Anusuya'),
('community', '140', 'Samriddhi'),
('community', '141', 'Sangamam'),
('community', '142', 'Sangha'),
('community', '143', 'Service Area'),
('community', '144', 'Shakti'),
('community', '145', 'Shakti Lokaa'),
('community', '146', 'Shanti'),
('community', '147', 'Sharnga'),
('community', '148', 'Siddhartha Farm'),
('community', '149', 'Siddhartha Forest'),
('community', '15', 'Aranya'),
('community', '150', 'Silence'),
('community', '151', 'Simplicity'),
('community', '152', 'Sincerity'),
('community', '153', 'Slancio'),
('community', '154', 'Solar Kitchen'),
('community', '155', 'Solitude Farm'),
('community', '156', 'Sri Ma'),
('community', '157', 'Success'),
('community', '158', 'Sukhavati'),
('community', '159', 'Surrender'),
('community', '16', 'Arati'),
('community', '160', 'Surya Nivas'),
('community', '161', 'Svaram'),
('community', '162', 'Sve Dame'),
('community', '163', 'Swayam'),
('community', '164', 'Tibetian Pavilion'),
('community', '165', 'Transformation'),
('community', '166', 'Transition School'),
('community', '167', 'Two Banyans'),
('community', '168', 'Udavi School'),
('community', '169', 'Udoombu'),
('community', '17', 'Arc En Ciel'),
('community', '170', 'Udyogam'),
('community', '171', 'Unity Pavilion'),
('community', '172', 'Utilite'),
('community', '173', 'Verite'),
('community', '174', 'Victory'),
('community', '175', 'Vikas'),
('community', '176', 'Waves'),
('community', '177', 'Windarra'),
('community', '178', 'Yantra'),
('community', '179', 'Youth Camp'),
('community', '18', 'Arka'),
('community', '180', 'Youth Centre'),
('community', '181', 'Recueillement'),
('community', '182', 'New Community'),
('community', '183', 'Administrative Area'),
('community', '184', 'Miramuki'),
('community', '185', 'Savitri Bhavan'),
('community', '186', 'International Zone'),
('community', '187', 'Visitors Centre'),
('community', '188', 'Inner Peace'),
('community', '189', 'Irumbai'),
('community', '19', 'Arya'),
('community', '190', 'Aditi'),
('community', '191', 'Sanjana'),
('community', '192', 'AV Foundation Staff Quarters'),
('community', '193', 'City Centre'),
('community', '2', 'Abri-forest'),
('community', '20', 'Aspiration'),
('community', '21', 'Aspiration Field'),
('community', '22', 'Atithi Griha'),
('community', '23', 'Aurelec'),
('community', '230', 'Maitreye II'),
('community', '24', 'Auro Farm'),
('community', '25', 'Auro Orchard'),
('community', '26', 'Auroannam'),
('community', '27', 'Aurobrindavan'),
('community', '28', 'Aurodam'),
('community', '29', 'Aurogreen'),
('community', '3', 'Acceptance'),
('community', '30', 'Auromode'),
('community', '31', 'Auromodele'),
('community', '32', 'Auroshilpam'),
('community', '33', 'Ayarpadi Farm'),
('community', '34', 'Baraka'),
('community', '35', 'Bharat Nivas'),
('community', '36', 'Bliss'),
('community', '37', 'Botanical Garden'),
('community', '38', 'Brihaspathi Farm'),
('community', '39', 'Buddha Garden'),
('community', '4', 'Adventure'),
('community', '40', 'Celebration'),
('community', '41', 'Centre Field'),
('community', '42', 'Centre Guest House'),
('community', '43', 'Certitude'),
('community', '44', 'Citadyn'),
('community', '45', 'Courage'),
('community', '46', 'Creativity'),
('community', '47', 'CSR'),
('community', '48', 'Dana'),
('community', '49', 'Darkali'),
('community', '5', 'Agni'),
('community', '50', 'Deepanam Staff Quart'),
('community', '51', 'Discipline'),
('community', '52', 'Djaima'),
('community', '53', 'Douceur'),
('community', '54', 'Edayanchavadi'),
('community', '55', 'Ekta'),
('community', '56', 'Equality'),
('community', '57', 'Espace'),
('community', '58', 'Eternity'),
('community', '59', 'Evergreen'),
('community', '6', 'Agni Jata'),
('community', '60', 'Existence'),
('community', '61', 'Felicity'),
('community', '62', 'Fertile'),
('community', '63', 'Fertile East'),
('community', '64', 'Fertile Field'),
('community', '65', 'Forecomers'),
('community', '66', 'Fraternity'),
('community', '67', 'Freedom'),
('community', '68', 'Future School'),
('community', '69', 'Gaia'),
('community', '7', 'Akashwa'),
('community', '70', 'Gaia''s Garden'),
('community', '71', 'Gokulam'),
('community', '72', 'Grace'),
('community', '73', 'Gratitude'),
('community', '74', 'Happiness'),
('community', '75', 'Harmony'),
('community', '76', 'Hc Staff Quarter'),
('community', '77', 'Herbal Forest'),
('community', '78', 'Hermitage'),
('community', '79', 'Hope'),
('community', '8', 'Alchemy'),
('community', '80', 'Horizon'),
('community', '81', 'Humility'),
('community', '82', 'Ilagnarkal'),
('community', '83', 'Inspiration'),
('community', '84', 'International House'),
('community', '85', 'Invocation'),
('community', '86', 'Isaiambalam'),
('community', '87', 'Jardin De Mere'),
('community', '88', 'Kailash'),
('community', '89', 'Kalabhumi'),
('community', '9', 'Allankuppam'),
('community', '90', 'Kindergarten'),
('community', '91', 'Kottakarai'),
('community', '92', 'Kuilapalayam'),
('community', '93', 'La Ferme'),
('community', '94', 'Lakshmipuram'),
('community', '95', 'Last School'),
('community', '96', 'Light'),
('community', '97', 'Luminosity'),
('community', '98', 'Madhuca'),
('community', '99', 'Maitreye'),
('contact_type', '1', 'telephone'),
('contact_type', '2', 'email'),
('email', 'from', 'notifications@miastudio.in'),
('email', 'from_name', 'Auroville Library'),
('email', 'from_name_short', 'Library'),
('email', 'password', 'zXr?mx;jn[dG'),
('email', 'smtphost', 'smtp.gmail.com'),
('email', 'url', 'http://serapis.local'),
('email', 'username', 'notifications@miastudio.in'),
('group', '1', 'aurovilian'),
('group', '2', 'guest'),
('group', '3', 'worker'),
('label', 'barcodefont', 'IDAutomationHC39M'),
('labelformat', '1', 'Spine Label'),
('labelformat', '2', 'Barcode Label'),
('language', '1', 'BEN'),
('language', '10', 'GUJ'),
('language', '11', 'HEB'),
('language', '12', 'HIN'),
('language', '13', 'ITA'),
('language', '14', 'JPN'),
('language', '15', 'KAN'),
('language', '16', 'KOR'),
('language', '17', 'LAT'),
('language', '18', 'MAL'),
('language', '19', 'MAY'),
('language', '2', 'BUL'),
('language', '20', 'MUL'),
('language', '21', 'NEW'),
('language', '22', 'NON'),
('language', '23', 'NOR'),
('language', '24', 'PAN'),
('language', '25', 'PER'),
('language', '26', 'PLI'),
('language', '27', 'POL'),
('language', '28', 'RUS'),
('language', '29', 'SPA'),
('language', '3', 'DUM'),
('language', '30', 'SWE'),
('language', '31', 'TAM'),
('language', '32', 'TEL'),
('language', '33', 'TIB'),
('language', '34', 'TML'),
('language', '35', 'WEL'),
('language', '4', 'DUT'),
('language', '5', 'ENG'),
('language', '6', 'FRE'),
('language', '7', 'GER'),
('language', '8', 'GRC'),
('language', '9', 'GRE'),
('lookup_type', 'advanced_search_custom', 'Advanced Search - Custom'),
('lookup_type', 'advanced_search_multiple', 'Advanced Search - Multiple'),
('lookup_type', 'author_role', 'Author Role'),
('lookup_type', 'author_type', 'Author Type'),
('lookup_type', 'badge', 'Badge'),
('lookup_type', 'book_status', 'Book Status'),
('lookup_type', 'checkout_status', 'Checkout Status'),
('lookup_type', 'community', 'Community'),
('lookup_type', 'contact_type', 'Contact Type'),
('lookup_type', 'email', 'Email'),
('lookup_type', 'group', 'Person Group'),
('lookup_type', 'label', 'Label'),
('lookup_type', 'labelformat', 'Label Format'),
('lookup_type', 'language', 'Language'),
('lookup_type', 'lookup_type', 'Lookup Labels'),
('lookup_type', 'merge_category', 'Merge Category'),
('lookup_type', 'overdue', 'Overdue Status'),
('lookup_type', 'person_status', 'Person status'),
('lookup_type', 'physicaldetail', 'Binding Type'),
('lookup_type', 'reminder', 'Reminder'),
('lookup_type', 'reminder_status', 'Reminder Status'),
('lookup_type', 'sparkpost', 'Sparkpost Email'),
('lookup_type', 'user_settings', 'User Settings'),
('lookup_type', 'user_type', 'User Type'),
('lookup_type', 'yesno', 'Yes-No'),
('merge_category', '1', 'Autor'),
('merge_category', '2', 'Keyword'),
('merge_category', '3', 'Topical Term'),
('overdue', '0', 'No Overdue'),
('overdue', '1', 'Long Overdue'),
('overdue', '2', 'Administratively Disabled'),
('person_status', '1', 'Aurovilian'),
('person_status', '2', 'New comer'),
('person_status', '3', 'Pre-Newcomer'),
('person_status', '4', 'Volunteer'),
('person_status', '5', 'Youth'),
('person_status', '6', 'Aurovilian Child'),
('person_status', '7', 'Guest'),
('person_status', '8', 'Worker'),
('physicaldetail', '1', 'hardcover'),
('physicaldetail', '2', 'softcover'),
('physicaldetail', '3', 'paperback'),
('physicaldetail', '4', 'clamped'),
('reminder', 'interval_1', '3'),
('reminder', 'interval_2', '10'),
('reminder', 'interval_3', '10'),
('reminder', 'last_processed', '2017-07-01'),
('reminder_status', '1', 'No Email'),
('reminder_status', '10', 'Not sending any more reminders'),
('reminder_status', '2', 'Waiting for 1st reminder'),
('reminder_status', '3', 'Ready for 1st reminder'),
('reminder_status', '4', 'Waiting for 2nd reminder'),
('reminder_status', '5', 'Ready for 2nd reminder'),
('reminder_status', '6', 'Waiting for 3rd reminder'),
('reminder_status', '7', 'Ready for 3rd reminder'),
('reminder_status', '8', 'Waiting for 4th reminder'),
('reminder_status', '9', 'Ready for 4th reminder'),
('sparkpost', 'apikey', 'ccf6303328c25c3520b6b94741bfe41860ee30c5'),
('sparkpost', 'from_email', 'avlib@auroville.org.in '),
('sparkpost', 'from_name', 'Auroville Library'),
('user_settings', 'badge_type_tags', 'info'),
('user_settings', 'ui_calendar_format', 'a:4:{s:6:"format";s:10:"yyyy-mm-dd";s:14:"todayHighlight";s:4:"true";s:9:"weekStart";s:1:"1";s:9:"autoclose";s:4:"true";}'),
('user_settings', 'ui_gridview_size', '25'),
('user_settings', 'ui_gridview_size_checkout', '15'),
('user_settings', 'ui_gridview_template', '{summary}\r\n{items}\r\n{pager}'),
('user_settings', 'ui_gridview_type', 'striped bordered condensed'),
('user_settings', 'ui_number_format', '##,##,##,##0'),
('user_type', '1', 'masteradmin'),
('user_type', '2', 'admin'),
('user_type', '3', 'staff'),
('user_type', '4', 'client'),
('yesno', '0', 'no'),
('yesno', '1', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `mailqueue`
--

CREATE TABLE IF NOT EXISTS `mailqueue` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `template_id` int(2) DEFAULT NULL,
  `mergevariables` text,
  `sendingDate` datetime DEFAULT NULL,
  `sendingresult` varchar(255) DEFAULT NULL,
  `sendingresult_id` int(1) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `createdBy_id` int(11) DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `modifiedBy_id` int(11) DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
`id` int(11) unsigned NOT NULL,
  `asynctoId` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `masterlistId` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aurovillename` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(96) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `community_id` int(11) unsigned DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `presence_id` int(11) DEFAULT NULL,
  `group_id` int(1) DEFAULT NULL,
  `workplace` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limitBooks` int(3) unsigned DEFAULT NULL,
  `limitDays` int(3) unsigned DEFAULT NULL,
  `limitExtensions` int(3) unsigned DEFAULT NULL,
  `overdue_id` int(1) unsigned NOT NULL DEFAULT '0',
  `notes` text COLLATE utf8mb4_unicode_ci,
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` datetime NOT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` datetime NOT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reminder`
--

CREATE TABLE IF NOT EXISTS `reminder` (
`id` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE IF NOT EXISTS `template` (
`id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `createdBy_id` int(11) unsigned DEFAULT NULL,
  `creationDate` date DEFAULT NULL,
  `modifiedBy_id` int(11) unsigned DEFAULT NULL,
  `modificationDate` date DEFAULT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `topicalterm`
--

CREATE TABLE IF NOT EXISTS `topicalterm` (
`id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) unsigned NOT NULL,
  `type_id` int(11) unsigned NOT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `template_id` int(11) unsigned DEFAULT NULL,
  `activationcode` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activationmail_count` int(2) DEFAULT NULL,
  `resetcode` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resetmail_count` int(2) DEFAULT NULL,
  `resetcodeDate` date DEFAULT NULL,
  `createdBy_id` int(10) unsigned NOT NULL,
  `creationDate` datetime NOT NULL,
  `modifiedBy_id` int(10) unsigned NOT NULL,
  `modificationDate` datetime NOT NULL,
  `deletedBy_id` int(11) unsigned DEFAULT NULL,
  `deletionDate` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `type_id`, `email`, `password`, `name`, `address`, `telephone`, `notes`, `template_id`, `activationcode`, `activationmail_count`, `resetcode`, `resetmail_count`, `resetcodeDate`, `createdBy_id`, `creationDate`, `modifiedBy_id`, `modificationDate`, `deletedBy_id`, `deletionDate`) VALUES
(0, 1, 'console@asp', '', 'console', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '2015-12-30 16:10:10', NULL, NULL),
(1, 1, 'coriolan@miastudio.in', '$2y$10$YOR4.H30IVbJXOJjZgA97elwmDNwu9yOU7a3xnkhtsw.9v/OWSH9K', 'Coriolan', NULL, NULL, NULL, NULL, NULL, 10, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 0, '2015-12-30 19:49:47', NULL, NULL),
(2, 2, 'coriolan@auroville.org.in', '$2y$10$YOR4.H30IVbJXOJjZgA97elwmDNwu9yOU7a3xnkhtsw.9v/OWSH9K', 'coriolan test', NULL, NULL, NULL, NULL, '7c5fbeb9ac7cc873de053bccec1aea48', 1, NULL, NULL, NULL, 1, '2016-05-11 12:43:40', 1, '2016-12-08 09:09:54', NULL, NULL),
(3, 1, 'avlib@auroville.org.in', '$2y$10$OcL1kyVd3h1xEiFROAfrN.LH6hKNHk2v6WSgATAjOjIuX1P6vhkMy', 'AVLib', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-12-13 12:44:55', 1, '2016-12-13 13:47:04', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author_book`
--
ALTER TABLE `author_book`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `serial_number` (`sn`), ADD KEY `autocomplete` (`pubPublisher`(191),`pubPlace`(191),`seriesName`), ADD KEY `deletionDate` (`deletionDate`), ADD KEY `title` (`title`(191));

--
-- Indexes for table `book_feature`
--
ALTER TABLE `book_feature`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_keyword`
--
ALTER TABLE `book_keyword`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_topicalterm`
--
ALTER TABLE `book_topicalterm`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactdetail`
--
ALTER TABLE `contactdetail`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `person_contact` (`person_id`,`contact`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keyword`
--
ALTER TABLE `keyword`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lookup`
--
ALTER TABLE `lookup`
 ADD PRIMARY KEY (`section`,`code`);

--
-- Indexes for table `mailqueue`
--
ALTER TABLE `mailqueue`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
 ADD PRIMARY KEY (`id`), ADD KEY `a` (`asynctoId`), ADD KEY `createdBy_id` (`createdBy_id`), ADD KEY `modifiedBy_id` (`modifiedBy_id`), ADD KEY `deletedBy_id` (`deletedBy_id`);

--
-- Indexes for table `reminder`
--
ALTER TABLE `reminder`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topicalterm`
--
ALTER TABLE `topicalterm`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `author_book`
--
ALTER TABLE `author_book`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `book_feature`
--
ALTER TABLE `book_feature`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `book_keyword`
--
ALTER TABLE `book_keyword`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `book_topicalterm`
--
ALTER TABLE `book_topicalterm`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contactdetail`
--
ALTER TABLE `contactdetail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `keyword`
--
ALTER TABLE `keyword`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mailqueue`
--
ALTER TABLE `mailqueue`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reminder`
--
ALTER TABLE `reminder`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `topicalterm`
--
ALTER TABLE `topicalterm`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
