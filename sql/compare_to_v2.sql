## check for number of keywords / features / topical terms

# compare keywords
select book_id, count(book_id) as cnt from avlib.books_keywords group by book_id having cnt > 1 ORDER BY `cnt` DESC;
select book_id, count(book_id) as cnt from serapis.book_keyword group by book_id having cnt > 1 ORDER BY `cnt` DESC;


# compare topical terms
select topicalterm_id, count(topicalterm_id) as cnt from avlib.books_topicalterms group by topicalterm_id having cnt > 1 ORDER BY `cnt` DESC;
select topicalterm_id, count(topicalterm_id) as cnt from serapis.book_topicalterm group by topicalterm_id having cnt > 1 ORDER BY `cnt` DESC;


# compare features - 66
select book_id, count(book_id) as cnt from avlib.books_features group by book_id having cnt > 1 ORDER BY `cnt` DESC;
select book_id, count(book_id) as cnt from serapis.book_feature group by book_id having cnt > 1 ORDER BY `cnt` DESC;
