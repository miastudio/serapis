# delete lookups when importing from lookup table
delete from serapis.lookup where section in ("group" , "language", "overdue", "physicaldetail", "author_role", "author_role");

#lookup groups
insert into `serapis`.`lookup`
(`section`, `code`, `description`)
select
"group", id, name
from avlib.groups;

#lookup language
insert into `serapis`.`lookup`
(`section`, `code`, `description`)
select
"language", id, name
from avlib.languages;

#lookup overdues
insert into `serapis`.`lookup`
(`section`, `code`, `description`)
select
"overdue", id, name
from avlib.overdues;

#lookup physicaldetails
insert into `serapis`.`lookup`
(`section`, `code`, `description`)
select
"physicaldetail", id, name
from avlib.physicaldetails;

#lookup physicaldetails
insert into `serapis`.`lookup`
(`section`, `code`, `description`)
select
"author_role", id, name
from avlib.roles;



#book
insert into serapis.book
(id, sn, title, titleOriginal, titleRemainder, titleParallel, partNumber, partTotal, statementOfResponsibility, ddc, number, pubPublisher, pubPlace, pubYear, editionStatement, collation, size, ISBN, ISSN, seriesName, seriesPartNumber, seriesPartName, notes, creationDate, modificationDate, deletionDate)
select
id, serial_number, title, original_title, remainder_of_title, parallel_title, part_x, part_of, statement_of_responsability, dewey_decimal_classification, book_number, publication_publisher, publication_place, publication_year, edition_statement, collation, size, ISBN, ISSN, series_name, series_part_no, series_part_name, notes, date_created, date_modified, date_removed
from avlib.books;

#book binding id
update serapis.book, avlib.books_physicaldetails
set serapis.book.binding_id = avlib.books_physicaldetails.physicaldetail_id
where serapis.book.id = avlib.books_physicaldetails.book_id;

#book language (long  !)
update serapis.book
set serapis.book.languages = (
select group_concat(languages.name)
from avlib.books_languages
left join avlib.languages on avlib.books_languages.language_id = avlib.languages.id
where avlib.books_languages.book_id = serapis.book.id
);


#author
insert into serapis.author
(id, title, name, nameFullerForm, lifetime, notes)
select
id, title, name, name_fuller_form, lifetime, notes
from avlib.authors;

#author_book link
insert into serapis.author_book(author_id, book_id, role_id)
select author_id, book_id, role_id
from avlib.authors_books;

#book_client link
insert into serapis.checkout
(book_id, person_id, borrowDate, dueDate, returnDate, reminderDate, reminderCount)
select book_id, client_id, date_borrow, date_due, date_return, date_reminder, reminder_level
from avlib.books_clients;

update serapis.checkout set status_id = 4 where reminderCount > 2;
update serapis.checkout set status_id = 3 where reminderCount > 0 and reminderCount <= 2;
update serapis.checkout set status_id = 2 where returnDate = "0000-00-00" and status_id IS NULL;
update serapis.checkout set status_id = 1 where returnDate != "0000-00-00";

# set all books as 'in'
update serapis.book
set status_id = 1;

# set borrowed books as 'out'
update serapis.book
left join serapis.checkout on serapis.checkout.book_id = serapis.book.id and serapis.checkout.returnDate = "0000-00-00"
set serapis.book.status_id = 2
where serapis.checkout.id IS NOT NULL;

# set checkoutcount in book
UPDATE serapis.book b,
(   SELECT book_id, count(id) as cnt
    FROM serapis.checkout
 group by book_id
) c
SET b.checkoutCount = c.cnt
WHERE b.id = c.book_id;

# default deletion (removal) date
UPDATE serapis.book
SET deletionDate = NULL
WHERE deletionDate = "0000-00-00";

# default creation date
UPDATE serapis.book
SET creationDate = NULL
WHERE creationDate = "0000-00-00";

# clients
insert into serapis.person
(`id`, `asynctoid`, `aurovillename`, `name`, `surname`, `category_id`, `address`, `workplace`, `limitBooks`, `limitDays`, `creationDate`, `deletionDate` )
select
`id`, `asynctoid`, `aurovillename`, `name`, `surname`, `group`, `address`, `workplace`, `number_of_books`, `number_of_days`, `date_registration`, `date_termination`
from avlib.clients;

# client group
update serapis.person, avlib.clients_groups
set serapis.person.status_id = avlib.clients_groups.group_id
where serapis.person.id = avlib.clients_groups.client_id;

update serapis.person
set serapis.person.status_id = 7
where serapis.person.status_id = 2;

update serapis.person
set serapis.person.status_id = 8
where serapis.person.status_id = 3;

# client address
update serapis.person
left join serapis.lookup on lookup.section = 'community' and lookup.description = person.address
set serapis.person.community_id = serapis.lookup.code;

update serapis.person
set serapis.person.address = null
where serapis.person.community_id is not null or serapis.person.address = '';

# client contacts
insert into `serapis`.`contactdetail`
(`person_id`,`contactType_id`, `contact`)
select
`avlib`.`clients`.`id`, "1", `avlib`.`clients`.`telephone`
from `avlib`.`clients`
where `avlib`.`clients`.`telephone` != '';

insert into `serapis`.`contactdetail`
(`person_id`,`contactType_id`, `contact`)
select
`avlib`.`clients`.`id`, "2", `avlib`.`clients`.`email`
from `avlib`.`clients`
where `avlib`.`clients`.`email` != '';

# remove spaces (in telephone numbers)
update serapis.contactdetail
set contact = REPLACE(contact, ' ', '');

# client overdue ID
update serapis.person, avlib.clients_overdues
set serapis.person.overdue_id = avlib.clients_overdues.overdue_id
where serapis.person.id = avlib.clients_overdues.client_id;

# client overdue ID (2)
update serapis.person
set serapis.person.overdue_id = 1
where serapis.person.overdue_id = 0;

# topical terms
insert into `serapis`.`topicalterm`
(`id`,`name`,`notes`)
select
`id`,`name`,`notes`
from avlib.topicalterms;

# book_topicalterm
insert into `serapis`.`book_topicalterm`
(`id`, `book_id`, `topicalterm_id`)
select 
`id`, `book_id`, `topicalterm_id`
from avlib.books_topicalterms;

# features
insert into `serapis`.`feature`
(`id`,`name`,`notes`)
select
`id`,`name`,`notes`
from avlib.features;

# book_topicalterm
insert into `serapis`.`book_feature`
(`id`, `book_id`, `feature_id`)
select 
`id`, `book_id`, `feature_id`
from avlib.books_features;

# keyword
insert into `serapis`.`keyword`
(`id`,`name`,`notes`)
select
`id`,`name`,`notes`
from `avlib`.`keywords`;

# book_keyword
insert into `serapis`.`book_keyword`
(`id`, `book_id`, `keyword_id`)
select 
`id`, `book_id`, `keyword_id`
from `avlib`.`books_keywords`;
